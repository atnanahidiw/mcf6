;(function ($, window, undefined) {
    'use strict';

    var $doc = $(document),
        Modernizr = window.Modernizr;

    $(document).ready(function() {
        $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
        $.fn.foundationButtons          ? $doc.foundationButtons() : null;
        $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
        $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
        $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
        $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
        $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
        $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
        $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
        $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
        $.fn.foundationClearing         ? $doc.foundationClearing() : null;

        $.fn.placeholder                ? $('input, textarea').placeholder() : null;
    });

    // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
    // $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
    // $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
    // $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
    // $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

    // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
    if (Modernizr.touch && !window.location.hash) {
        $(window).load(function () {
            setTimeout(function () {
                window.scrollTo(0, 1);
            }, 0);
        });
    }




})(jQuery, this);


/*---------------------------------
 Correct OS & Browser Check
 -----------------------------------*/

var ua = navigator.userAgent,
    checker = {
        os : {
            iphone : ua.match(/iPhone/),
            ipod : ua.match(/iPod/),
            ipad : ua.match(/iPad/),
            blackberry : ua.match(/BlackBerry/),
            android : ua.match(/(Android|Linux armv6l|Linux armv7l)/),
            linux : ua.match(/Linux/),
            win : ua.match(/Windows/),
            mac : ua.match(/Macintosh/)
        },
        ua : {
            ie : ua.match(/MSIE/),
            ie6 : ua.match(/MSIE 6.0/),
            ie7 : ua.match(/MSIE 7.0/),
            ie8 : ua.match(/MSIE 8.0/),
            ie9 : ua.match(/MSIE 9.0/),
            ie10 : ua.match(/MSIE 10.0/),
            opera : ua.match(/Opera/),
            firefox : ua.match(/Firefox/),
            chrome : ua.match(/Chrome/),
            safari : ua.match(/(Safari|BlackBerry)/)
        }
    };

jQuery(document).ready(function() {

    /* Mobile Devices Navigation Script */
    (function ($) {
        $('a.top-menu-button').bind('click', function () {
            if ($(this).hasClass('active')) {
                $('#top-menu').slideUp('fast');

                $(this).removeClass('active');
            } else {
                $('#top-menu').slideDown('fast');

                $(this).addClass('active');
            }

            return false;
        } );

        $(window).bind('resize', function () {
            $('a.top-menu-button').removeClass('active');
        } );

        $('#top-menu li.has-submenu>.menu-item-wrap').bind('click', function () {
            if (
                checker.os.iphone ||
                    checker.os.ipad ||
                    checker.os.ipod ||
                    checker.os.android ||
                    checker.os.blackberry
                ) {
                if ($(this).next().is('ul')) {
                    $(this).find('a').removeAttr('href');

                }
            }
            if ($('a.top-menu-button').is(':visible')) {
                if ($(this).next().is('ul')) {
                    if ($(this).next().is(':visible')) {
                        $(this).removeClass('drop_active');

                        if (
                            checker.os.iphone ||
                                checker.os.ipad ||
                                checker.os.ipod

                            ) {
                            $(this).next().css( {
                                display : 'none'
                            } );
                        } else {
                            $(this).next().slideUp('fast');
                        }

                        $(this).next().find('ul').css( {
                            display : 'none'
                        } );
                    } else {
                        $(this).parent().parent().find('a').removeClass('drop_active');

                        if (
                            checker.os.iphone ||
                                checker.os.ipad ||
                                checker.os.ipod
                            ) {
                            $(this).parent().parent().find('ul').css( {
                                display : 'none'
                            } );
                        } else {
                            $(this).parent().parent().find('ul').slideUp('fast');
                        }

                        $(this).addClass('drop_active');

                        if (
                            checker.os.iphone ||
                                checker.os.ipad ||
                                checker.os.ipod
                            ) {
                            $(this).next().css( {
                                display : 'block'
                            } );
                        } else {
                            $(this).next().slideDown('fast');
                        }
                    }

                }
            }
        } );
    } )(jQuery);

    /* Top Menu Toggle */
    (function ($) {
        $('#open-top-panel').bind('click', function () {
            if ($(this).hasClass('active')) {
                $('.top-panel-inner').slideUp('slow');

                $(this).removeClass('active');
            } else {
                $('.top-panel-inner').slideDown('slow');

                $(this).addClass('active');
            }

            return false;
        } );
    } )(jQuery);

    /*---------------------------------
     Widget icons
     -----------------------------------*/

    jQuery(document).ready(function() {

        var icon_name;
        var icon_element;

        jQuery(".widget-title").each(function () {


            icon_name = jQuery(this).css('content');

            if(jQuery.browser.msie)
            {
                icon_name = jQuery(this).css('ie');
            }

            icon_element = "<i class=" + icon_name + "></i>";
            icon_element = jQuery(icon_element).addClass('icon');


            if ( !(jQuery(this).parent('div').hasClass('widget_crum_latest_3_news'))) {
                jQuery(this).prepend(icon_element);
            }
        });

    });

    /*---------------------------------
     Lang drop-down
     -----------------------------------*/

    jQuery(".lang-sel").hover(function () {

        jQuery(this).addClass("hovered");

    }, function () {

        jQuery(this).removeClass("hovered");

    });

    /*---------------------------------
     Scroll To Top
     -----------------------------------*/

    jQuery(".backtotop").addClass("hidden");
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() === 0) {
            jQuery(".backtotop").addClass("hidden")
        } else {
            jQuery(".backtotop").removeClass("hidden")
        }
    });

    jQuery('.backtotop').click(function () {
        jQuery('body,html').animate({
            scrollTop:0
        }, 1200);
        return false;
    });


    (function ($) {
        $('#header #searchform .s-submit').bind('click', function () {
            if ($('#header #searchform').hasClass('active')) {
                $('#header #searchform').submit();
            } else {
                $('#header #searchform').addClass('active');
                $('#header .s-field').fadeIn('slow');

            }
        return false;
            
        } );

    } )(jQuery);

    /*---------------------------------
     Navigation dropdown
     -----------------------------------*/
    (function ($) {
        if ($(window).width() >= 768) {
            $(window).scroll(function () {
                var sT = $(window).scrollTop(),
                    hH = Number($('#header').height()),
                    pH = Number($('#top-panel').height());
                if (sT > hH+pH) {
                    $('.droped-navi').addClass('active');
                    $('.droped-navi').stop().animate( {
                        top : 0,
                        opacity : 1
                    }, 500);
                } else {
                    $('.droped-navi').removeClass('active');
                    $('.droped-navi').stop().animate( {
                        top : '-60px',
                        opacity : 0
                    }, 500);
                }
            } );
        }
    } )(jQuery);

    /*---------------------------------
     Nicescroll run
     -----------------------------------*/

    if (jQuery.fn.niceScroll) {
        jQuery('html').niceScroll({
            styler:'fb',
            cursorcolor:'#616b74',
            cursorborder:'0',
            zindex:9999
        });
    }





} );

jQuery("#loginform a.submit").click( function(){
    jQuery(this).parents("#loginform").submit();
    return false;
});


jQuery(document).ready(function() {
    jQuery("a[class^='prettyPhoto']").prettyPhoto();

    jQuery('.entry-content a').has('img').addClass('prettyPhoto');


    jQuery('.entry-content a img').click(function () {
        var desc = jQuery(this).attr('title');
        jQuery('.entry-content a').has('img').attr('title', desc);
    });


    jQuery("a[rel^='prettyPhoto']").prettyPhoto();
    jQuery("a.zoom-link").prettyPhoto();
    jQuery("a.thumbnail").prettyPhoto();
});