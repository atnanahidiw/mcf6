function initialize() {
    var myLatlng = new google.maps.LatLng(-6.888515, 107.608858);
    var mapOptions = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    var marker = new google.maps.Marker({
    position: myLatlng,
    title: 'Sekretariat MCF-MMC ITB 2014',
  });
    marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);