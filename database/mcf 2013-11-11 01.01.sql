-- MySQL dump 10.13  Distrib 5.1.36, for portbld-freebsd7.1 (i386)
--
-- Host: localhost    Database: mcf-mmc
-- ------------------------------------------------------
-- Server version	5.1.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `name` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES ('paperCode','P'),('paperCount','3'),('paperStarted','false'),('schoolCode','S'),('schoolCount','6'),('schoolStarted','false'),('seminarCode','X'),('seminarCount','14'),('seminarStarted','false'),('universityCode','U'),('universityCount','12'),('universityStarted','false');
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant`
--

DROP TABLE IF EXISTS `contestant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_username` varchar(10) COLLATE utf8_bin NOT NULL,
  `contestant_password` varchar(100) COLLATE utf8_bin NOT NULL,
  `contestant_type` int(11) NOT NULL,
  `contestant_status` int(11) NOT NULL DEFAULT '0',
  `contestant_expired_registration_date` date NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant`
--

LOCK TABLES `contestant` WRITE;
/*!40000 ALTER TABLE `contestant` DISABLE KEYS */;
INSERT INTO `contestant` VALUES (0,'admin','admin',0,1,'0000-00-00'),(1,'MCF001U','tes',1,0,'0000-00-00'),(2,'MCF001S','tes',2,0,'0000-00-00'),(3,'MCF001P','tes',3,0,'0000-00-00'),(4,'MCF001X','tes',4,0,'0000-00-00'),(5,'MCF002U','',2,0,'0000-00-00'),(6,'MCF003U','',2,0,'0000-00-00'),(7,'MCF004U','',2,0,'0000-00-00'),(8,'MCF005U','',2,0,'0000-00-00'),(9,'MCF006U','',2,0,'0000-00-00'),(10,'MCF007U','',2,0,'0000-00-00'),(11,'MCF010U','',2,0,'0000-00-00'),(12,'MCF002S','',1,0,'0000-00-00'),(13,'MCF003S','0f9a859b42aa',1,1,'0000-00-00'),(14,'MCF002P','',3,0,'0000-00-00'),(15,'MCF011U','',2,0,'0000-00-00'),(16,'MCF004S','ed33ed4d7e14',1,1,'0000-00-00'),(17,'MCF003P','',3,0,'0000-00-00'),(18,'MCF012U','',2,0,'0000-00-00'),(19,'MCF002X','',4,0,'0000-00-00'),(20,'MCF005S','',1,0,'0000-00-00'),(21,'MCF003X','',4,0,'0000-00-00'),(22,'MCF006S','',1,0,'0000-00-00'),(24,'MCF005X','',4,0,'0000-00-00'),(25,'MCF006X','',4,0,'0000-00-00'),(26,'MCF007X','',4,0,'0000-00-00'),(27,'MCF008X','',4,0,'0000-00-00'),(28,'MCF009X','',4,0,'0000-00-00'),(29,'MCF010X','',4,0,'0000-00-00'),(30,'MCF011X','',4,0,'0000-00-00'),(31,'MCF012X','',4,0,'0000-00-00'),(33,'MCF014X','214dad367069',4,1,'0000-00-00');
/*!40000 ALTER TABLE `contestant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_paper`
--

DROP TABLE IF EXISTS `contestant_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_paper` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  CONSTRAINT `contestant_paper_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_paper`
--

LOCK TABLES `contestant_paper` WRITE;
/*!40000 ALTER TABLE `contestant_paper` DISABLE KEYS */;
INSERT INTO `contestant_paper` VALUES (3,'MCF001P_photo.jpg','','','ab','ab','ab@ad.aa','1234567890','ab','','1234567890'),(14,'','','','Ya','Gitu','','6281804597377','','',''),(17,'','','','insan','insan','insan@rock.com','085697169191','insan','insan','085697169191');
/*!40000 ALTER TABLE `contestant_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_payment`
--

DROP TABLE IF EXISTS `contestant_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_payment` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(20) NOT NULL,
  KEY `contestant_payment_ibfk_1` (`contestant_id`),
  CONSTRAINT `contestant_payment_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_payment`
--

LOCK TABLES `contestant_payment` WRITE;
/*!40000 ALTER TABLE `contestant_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_problem`
--

DROP TABLE IF EXISTS `contestant_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_problem` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  KEY `contestant_problem_ibfk_1` (`contestant_id`),
  CONSTRAINT `contestant_problem_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_problem`
--

LOCK TABLES `contestant_problem` WRITE;
/*!40000 ALTER TABLE `contestant_problem` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_school`
--

DROP TABLE IF EXISTS `contestant_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_school` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`),
  CONSTRAINT `contestant_school_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_school`
--

LOCK TABLES `contestant_school` WRITE;
/*!40000 ALTER TABLE `contestant_school` DISABLE KEYS */;
INSERT INTO `contestant_school` VALUES (2,'a','a','','','','a','','','','','','a','','','','','','a','','','','','','','a','',''),(12,'DIta','dita.manurung@gmail.com','MCF002S_photo_1.jpg','MCF002S_id_1_f.jpg','MCF002S_id_1_b.jpg','Dita Ully','jl. parakan resik 33','08996911778','MCF002S_photo_2.jpg','MCF002S_id_2_f.jpg','MCF002S_id_2_b.jpg','Tiya Juniarti','jl.rajawali','085722443515','MCF002S_photo_3.jpg','MCF002S_id_3_f.jpg','MCF002S_id_3_b.jpg','Karina','Jl.kopo','08522231232','Edy soewono','jl.dagieaya','edysoewono@itb.ac.id','0813211567654','SMAN 11 BANDUNG','jl. kembar baru','022-5463128'),(13,'Panitia MCF','yozef.g.tjandra@students.itb.ac.id','','','','Yoyoyoyoyoyo','kebun bibit 3000','6281804597377','','','','zefzefzefzef','kebun kacang 1567','62818045974sd','','','','Giogiogiogio','kebun stroberi','6281804ssdvhjk','Limanta','Kebun binatang 3000','limanta@gmail.com','085639200439','Institut Toko Bagus','Jalan Ganesha 5000',''),(16,'Tes1','dav3leo@gmail.com','','','','Nicholas','','','','','','','','','','','','','','','','','','','SMAK Penabur HI','',''),(20,'asoy','siput_fudrin@yahoo.com','','','','itulahitu','','1234567890','','','','bapaknya','','1234567890','','','','aji','','1234567890','','','','','itb','','1234567890'),(22,'asd','rhezamichael@yahoo.com','','','','asd','asd','123123123','','','','asdasd','asd','123123123','','','','asdas','asdas','123123123','asdas','asdas','','12312','asd','asd','123123123');
/*!40000 ALTER TABLE `contestant_school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_seminar`
--

DROP TABLE IF EXISTS `contestant_seminar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_seminar` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_institution_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_institution_address` text COLLATE utf8_bin NOT NULL,
  `contestant_institution_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_seminar`
--

LOCK TABLES `contestant_seminar` WRITE;
/*!40000 ALTER TABLE `contestant_seminar` DISABLE KEYS */;
INSERT INTO `contestant_seminar` VALUES (4,'','','','a','a','a','a','a','',''),(19,'','','','sdvhjk','sdvsioldvo','yozef.g.tjandra@live.com','08356782349','sdvhjk','','08236983493'),(21,'','','','ga tau','','siput_fudrin@yahoo.com','1234567890','dimana','','1234567890'),(28,'MCF009X_photo.jpg','','','lksdasadaskl','','sada@dasd.sad','1234567890','djskdjskhfsjkdjk','','1234567890'),(33,'MCF014X_photo.jpg','','','asdfg','','atnanahidiw@gmail.com','1234567890','xdasda','','1234567890');
/*!40000 ALTER TABLE `contestant_seminar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_university`
--

DROP TABLE IF EXISTS `contestant_university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_university` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_university_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_university_address` text COLLATE utf8_bin NOT NULL,
  `contestant_university_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`),
  CONSTRAINT `contestant_university_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_university`
--

LOCK TABLES `contestant_university` WRITE;
/*!40000 ALTER TABLE `contestant_university` DISABLE KEYS */;
INSERT INTO `contestant_university` VALUES (1,'adadeh','adadeh','','','','adadeh','','','','','','adadeh','','','','','','adadeh','','','','','','','adadeh','',''),(10,'Mirza','atnanahidiw@gmail.com','MCF007U_photo_1.jpg','','','','','','','','','','','','','','','','','','','','','','','',''),(11,'ganteng','mcf.mmc.itb@gmail.com','MCF010U_photo_1.jpg','','','','','','','','','','','','','','','','','','','','','','','',''),(15,'','','','','','','','','','','','','','','','','','','','','','','','','','',''),(18,'yoyoyoi','yozef.g.tjandra@live.com','','','','lalala','lilili','08257892340','','','','lulululu','','08235678235','','','','hihihihi','','0812478935','','','','','shm,k','','0224696346');
/*!40000 ALTER TABLE `contestant_university` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-11  3:44:39
