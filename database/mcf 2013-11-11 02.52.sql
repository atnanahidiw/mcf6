-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 10 Nov 2013 pada 20.50
-- Versi Server: 5.6.11
-- Versi PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `mcf`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `configuration`
--

DROP TABLE IF EXISTS `configuration`;
CREATE TABLE IF NOT EXISTS `configuration` (
  `name` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `configuration`
--

INSERT INTO `configuration` (`name`, `value`) VALUES
('paperCode', 'P'),
('paperCount', '0'),
('paperStarted', 'false'),
('schoolCode', 'S'),
('schoolCount', '0'),
('schoolStarted', 'false'),
('seminarCode', 'X'),
('seminarCount', '0'),
('seminarStarted', 'false'),
('universityCode', 'U'),
('universityCount', '0'),
('universityStarted', 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant`
--

DROP TABLE IF EXISTS `contestant`;
CREATE TABLE IF NOT EXISTS `contestant` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_username` varchar(10) COLLATE utf8_bin NOT NULL,
  `contestant_password` varchar(100) COLLATE utf8_bin NOT NULL,
  `contestant_type` int(11) NOT NULL,
  `contestant_status` int(11) NOT NULL DEFAULT '0',
  `contestant_expired_registration_date` date NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=44 ;

--
-- Dumping data untuk tabel `contestant`
--

INSERT INTO `contestant` (`contestant_id`, `contestant_username`, `contestant_password`, `contestant_type`, `contestant_status`, `contestant_expired_registration_date`) VALUES
(0, 'admin', '3145a5774b821fe25e4bdaab4c22499f', 0, 2, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_paper`
--

DROP TABLE IF EXISTS `contestant_paper`;
CREATE TABLE IF NOT EXISTS `contestant_paper` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_payment`
--

DROP TABLE IF EXISTS `contestant_payment`;
CREATE TABLE IF NOT EXISTS `contestant_payment` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(20) NOT NULL,
  KEY `contestant_payment_ibfk_1` (`contestant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_problem`
--

DROP TABLE IF EXISTS `contestant_problem`;
CREATE TABLE IF NOT EXISTS `contestant_problem` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  KEY `contestant_problem_ibfk_1` (`contestant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_school`
--

DROP TABLE IF EXISTS `contestant_school`;
CREATE TABLE IF NOT EXISTS `contestant_school` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_seminar`
--

DROP TABLE IF EXISTS `contestant_seminar`;
CREATE TABLE IF NOT EXISTS `contestant_seminar` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_institution_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_institution_address` text COLLATE utf8_bin NOT NULL,
  `contestant_institution_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_university`
--

DROP TABLE IF EXISTS `contestant_university`;
CREATE TABLE IF NOT EXISTS `contestant_university` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_university_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_university_address` text COLLATE utf8_bin NOT NULL,
  `contestant_university_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=44 ;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `contestant_paper`
--
ALTER TABLE `contestant_paper`
  ADD CONSTRAINT `contestant_paper_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_payment`
--
ALTER TABLE `contestant_payment`
  ADD CONSTRAINT `contestant_payment_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_problem`
--
ALTER TABLE `contestant_problem`
  ADD CONSTRAINT `contestant_problem_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_school`
--
ALTER TABLE `contestant_school`
  ADD CONSTRAINT `contestant_school_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_seminar`
--
ALTER TABLE `contestant_seminar`
  ADD CONSTRAINT `contestant_seminar_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_university`
--
ALTER TABLE `contestant_university`
  ADD CONSTRAINT `contestant_university_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
