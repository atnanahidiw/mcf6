-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: mcf
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `name` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES ('paperCode','P'),('paperCount','0'),('paperStarted','false'),('schoolCode','S'),('schoolCount','0'),('schoolStarted','false'),('seminarCode','X'),('seminarCount','0'),('seminarStarted','false'),('universityCode','U'),('universityCount','0'),('universityStarted','true');
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant`
--

DROP TABLE IF EXISTS `contestant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_username` varchar(10) COLLATE utf8_bin NOT NULL,
  `contestant_password` varchar(100) COLLATE utf8_bin NOT NULL,
  `contestant_type` int(11) NOT NULL,
  `contestant_status` int(11) NOT NULL DEFAULT '0',
  `contestant_expired_registration_date` date NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant`
--

LOCK TABLES `contestant` WRITE;
/*!40000 ALTER TABLE `contestant` DISABLE KEYS */;
INSERT INTO `contestant` VALUES (0,'admin','3145a5774b821fe25e4bdaab4c22499f',0,2,'0000-00-00'),(1,'tes001','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(2,'tes002','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(3,'tes003','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(4,'tes004','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(5,'tes005','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(6,'tes006','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(7,'tes007','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(8,'tes008','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(9,'tes009','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(10,'tes010','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(11,'tes011','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(12,'tes012','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(13,'tes013','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(14,'tes014','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(15,'tes015','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(16,'tes016','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(17,'tes017','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(18,'tes018','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(19,'tes019','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(20,'tes020','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(21,'tes021','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(22,'tes022','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(23,'tes023','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(24,'tes024','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(25,'tes025','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(26,'tes026','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(27,'tes027','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(28,'tes028','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(29,'tes029','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(30,'tes030','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(31,'tes031','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(32,'tes032','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(33,'tes033','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(34,'tes034','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(35,'tes035','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(36,'tes036','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(37,'tes037','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(38,'tes038','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(39,'tes039','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(40,'tes040','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(41,'tes041','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(42,'tes042','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(43,'tes043','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(44,'tes044','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(45,'tes045','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(46,'tes046','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(47,'tes047','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(48,'tes048','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(49,'tes049','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(50,'tes050','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(51,'tes051','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(52,'tes052','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(53,'tes053','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(54,'tes054','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(55,'tes055','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(56,'tes056','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(57,'tes057','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(58,'tes058','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(59,'tes059','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(60,'tes060','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(61,'tes061','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(62,'tes062','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(63,'tes063','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(64,'tes064','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(65,'tes065','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(66,'tes066','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(67,'tes067','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(68,'tes068','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(69,'tes069','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(70,'tes070','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(71,'tes071','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(72,'tes072','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(73,'tes073','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(74,'tes074','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(75,'tes075','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(76,'tes076','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(77,'tes077','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(78,'tes078','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(79,'tes079','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(80,'tes080','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(81,'tes081','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(82,'tes082','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(83,'tes083','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(84,'tes084','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(85,'tes085','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(86,'tes086','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(87,'tes087','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(88,'tes088','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(89,'tes089','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(90,'tes090','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(91,'tes091','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(92,'tes092','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(93,'tes093','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(94,'tes094','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(95,'tes095','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(96,'tes096','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(97,'tes097','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(98,'tes098','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(99,'tes099','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00'),(100,'tes100','eff42b9e770e8fd0484e5d13229aa279',-1,2,'0000-00-00');
/*!40000 ALTER TABLE `contestant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_paper`
--

DROP TABLE IF EXISTS `contestant_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_paper` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  CONSTRAINT `contestant_paper_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_paper`
--

LOCK TABLES `contestant_paper` WRITE;
/*!40000 ALTER TABLE `contestant_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_problem`
--

DROP TABLE IF EXISTS `contestant_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_problem` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  KEY `contestant_problem_ibfk_1` (`contestant_id`),
  CONSTRAINT `contestant_problem_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_problem`
--

LOCK TABLES `contestant_problem` WRITE;
/*!40000 ALTER TABLE `contestant_problem` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_school`
--

DROP TABLE IF EXISTS `contestant_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_school` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_nik` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`),
  CONSTRAINT `contestant_school_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_school`
--

LOCK TABLES `contestant_school` WRITE;
/*!40000 ALTER TABLE `contestant_school` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_seminar`
--

DROP TABLE IF EXISTS `contestant_seminar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_seminar` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_institution_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_institution_address` text COLLATE utf8_bin NOT NULL,
  `contestant_institution_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  CONSTRAINT `contestant_seminar_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_seminar`
--

LOCK TABLES `contestant_seminar` WRITE;
/*!40000 ALTER TABLE `contestant_seminar` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_seminar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_university`
--

DROP TABLE IF EXISTS `contestant_university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_university` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_nik` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_university_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_university_address` text COLLATE utf8_bin NOT NULL,
  `contestant_university_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_motivation_letter` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`),
  CONSTRAINT `contestant_university_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_university`
--

LOCK TABLES `contestant_university` WRITE;
/*!40000 ALTER TABLE `contestant_university` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_university_topic`
--

DROP TABLE IF EXISTS `contestant_university_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_university_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_title` varchar(100) NOT NULL,
  `topic_quota` int(11) NOT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_university_topic`
--

LOCK TABLES `contestant_university_topic` WRITE;
/*!40000 ALTER TABLE `contestant_university_topic` DISABLE KEYS */;
INSERT INTO `contestant_university_topic` VALUES (1,'Topik Dummy 1',10),(2,'Topik Dummy 2',10),(3,'Topik Dummy 3',10);
/*!40000 ALTER TABLE `contestant_university_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestant_university_topic_choice`
--

DROP TABLE IF EXISTS `contestant_university_topic_choice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestant_university_topic_choice` (
  `topic_id` int(11) NOT NULL,
  `contestant_id` int(11) NOT NULL,
  KEY `topic_id` (`topic_id`),
  KEY `contestant_id` (`contestant_id`),
  CONSTRAINT `contestant_university_topic_choice_ibfk_2` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `contestant_university_topic_choice_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `contestant_university_topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestant_university_topic_choice`
--

LOCK TABLES `contestant_university_topic_choice` WRITE;
/*!40000 ALTER TABLE `contestant_university_topic_choice` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestant_university_topic_choice` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-19  1:12:11
