-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 10 Jun 2013 pada 13.37
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `mcf`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `name` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `configuration`
--

INSERT INTO `configuration` (`name`, `value`) VALUES
('paperCode', 'P'),
('paperCount', '5'),
('schoolCode', 'S'),
('schoolCount', '1'),
('universityCode', 'U'),
('universityCount', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant`
--

CREATE TABLE IF NOT EXISTS `contestant` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_username` varchar(10) COLLATE utf8_bin NOT NULL,
  `contestant_password` varchar(100) COLLATE utf8_bin NOT NULL,
  `contestant_type` int(11) NOT NULL,
  `contestant_paid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=331 ;

--
-- Dumping data untuk tabel `contestant`
--

INSERT INTO `contestant` (`contestant_id`, `contestant_username`, `contestant_password`, `contestant_type`, `contestant_paid`) VALUES
(327, 'MCF003P', '', 0, 0),
(328, 'MCF004P', '', 0, 0),
(329, 'MCF005P', '', 0, 0),
(330, 'MCF001S', '', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_paper`
--

CREATE TABLE IF NOT EXISTS `contestant_paper` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=330 ;

--
-- Dumping data untuk tabel `contestant_paper`
--

INSERT INTO `contestant_paper` (`contestant_id`, `contestant_name`, `contestant_phone`, `contestant_address`, `contestant_email`, `contestant_school_name`) VALUES
(328, '1234', '1234', 0x31323331, '1231', '12312'),
(329, '1234', '1234', 0x31323331, '1231', '12312');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_school`
--

CREATE TABLE IF NOT EXISTS `contestant_school` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=331 ;

--
-- Dumping data untuk tabel `contestant_school`
--

INSERT INTO `contestant_school` (`contestant_id`, `contestant_team_name`, `contestant_phone`, `contestant_email`, `contestant_first_name`, `contestant_second_name`, `contestant_third_name`, `contestant_school_name`) VALUES
(330, 'salmdlsa;', 'l;cmscl;sdm;', ';lsdm;ldsm;', 'l;dsmfl;sdm;', 'l;sdmf;lsm;', 'l;sdfml;sdmflsd', ';lsdmfl;ds');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_university`
--

CREATE TABLE IF NOT EXISTS `contestant_university` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_university_name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `contestant_paper`
--
ALTER TABLE `contestant_paper`
  ADD CONSTRAINT `contestant_paper_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_school`
--
ALTER TABLE `contestant_school`
  ADD CONSTRAINT `contestant_school_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_university`
--
ALTER TABLE `contestant_university`
  ADD CONSTRAINT `contestant_university_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
