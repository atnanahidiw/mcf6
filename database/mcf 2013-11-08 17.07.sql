-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 08 Nov 2013 pada 11.07
-- Versi Server: 5.6.11
-- Versi PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `mcf`
--
CREATE DATABASE IF NOT EXISTS `mcf` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mcf`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `name` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `configuration`
--

INSERT INTO `configuration` (`name`, `value`) VALUES
('paperCode', 'P'),
('paperCount', '2'),
('paperStarted', 'false'),
('schoolCode', 'S'),
('schoolCount', '24'),
('schoolStarted', 'false'),
('seminarCode', 'X'),
('seminarCount', '3'),
('seminarStarted', 'false'),
('universityCode', 'U'),
('universityCount', '15'),
('universityStarted', 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant`
--

CREATE TABLE IF NOT EXISTS `contestant` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_username` varchar(10) COLLATE utf8_bin NOT NULL,
  `contestant_password` varchar(100) COLLATE utf8_bin NOT NULL,
  `contestant_type` int(11) NOT NULL,
  `contestant_status` int(11) NOT NULL DEFAULT '0',
  `contestant_expired_registration_date` date NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=44 ;

--
-- Dumping data untuk tabel `contestant`
--

INSERT INTO `contestant` (`contestant_id`, `contestant_username`, `contestant_password`, `contestant_type`, `contestant_status`, `contestant_expired_registration_date`) VALUES
(0, 'admin', 'admin', 0, 1, '0000-00-00'),
(1, 'MCF001U', 'tes', 2, 0, '0000-00-00'),
(2, 'MCF001S', '0f78ee68ed09', 1, 1, '0000-00-00'),
(3, 'MCF001P', 'tes', 3, 0, '0000-00-00'),
(4, 'MCF001X', 'tes', 4, 0, '0000-00-00'),
(11, 'MCF003U', '', 1, 0, '0000-00-00'),
(12, 'MCF004U', '', 1, 0, '0000-00-00'),
(13, 'MCF005U', '', 1, 0, '0000-00-00'),
(14, 'MCF006U', '', 1, 0, '0000-00-00'),
(15, 'MCF007U', '', 1, 0, '0000-00-00'),
(16, 'MCF009U', '', 1, 0, '0000-00-00'),
(17, 'MCF010U', '', 2, 0, '0000-00-00'),
(18, 'MCF011U', '', 2, 0, '0000-00-00'),
(20, 'MCF005S', '', 1, 0, '0000-00-00'),
(21, 'MCF002P', 'd149f04d4c58', 3, 1, '0000-00-00'),
(22, 'MCF002X', '', 4, 0, '0000-00-00'),
(23, 'MCF003X', '', 4, 0, '0000-00-00'),
(25, 'MCF009S', 'de41ca213c12', 1, 2, '0000-00-00'),
(26, 'MCF010S', '', 1, 0, '0000-00-00'),
(27, 'MCF011S', '', 1, 0, '0000-00-00'),
(28, 'MCF012S', '', 1, 0, '0000-00-00'),
(29, 'MCF013S', '', 1, 0, '0000-00-00'),
(30, 'MCF014S', '', 1, 0, '0000-00-00'),
(31, 'MCF015S', '', 1, 0, '0000-00-00'),
(32, 'MCF016S', '', 1, 0, '0000-00-00'),
(33, 'MCF017S', '', 1, 0, '0000-00-00'),
(34, 'MCF018S', '', 1, 0, '0000-00-00'),
(38, 'MCF022S', '', 1, 0, '0000-00-00'),
(39, 'MCF023S', '', 1, 0, '0000-00-00'),
(40, 'MCF024S', '', 1, 0, '0000-00-00'),
(43, 'MCF015U', '6ff65cfae5e8', 2, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_paper`
--

CREATE TABLE IF NOT EXISTS `contestant_paper` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=22 ;

--
-- Dumping data untuk tabel `contestant_paper`
--

INSERT INTO `contestant_paper` (`contestant_id`, `contestant_photo`, `contestant_id_f`, `contestant_id_b`, `contestant_name`, `contestant_address`, `contestant_email`, `contestant_phone`, `contestant_school_name`, `contestant_school_address`, `contestant_school_phone`) VALUES
(3, 'MCF001P_photo.jpg', '', '', 'a', 'a', 'aaa@aaa.aaa', '1234567890', 'a', '', '1234567890'),
(21, '', '', '', 'Mirza Widihananta', '', 'atnanahidiw@gmail.com', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_payment`
--

CREATE TABLE IF NOT EXISTS `contestant_payment` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(20) NOT NULL,
  KEY `contestant_payment_ibfk_1` (`contestant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_problem`
--

CREATE TABLE IF NOT EXISTS `contestant_problem` (
  `contestant_id` int(11) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  KEY `contestant_problem_ibfk_1` (`contestant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_school`
--

CREATE TABLE IF NOT EXISTS `contestant_school` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_school_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_school_address` text COLLATE utf8_bin NOT NULL,
  `contestant_school_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=41 ;

--
-- Dumping data untuk tabel `contestant_school`
--

INSERT INTO `contestant_school` (`contestant_id`, `contestant_team_name`, `contestant_email`, `contestant_first_photo`, `contestant_first_id_f`, `contestant_first_id_b`, `contestant_first_name`, `contestant_first_address`, `contestant_first_phone`, `contestant_second_photo`, `contestant_second_id_f`, `contestant_second_id_b`, `contestant_second_name`, `contestant_second_address`, `contestant_second_phone`, `contestant_third_photo`, `contestant_third_id_f`, `contestant_third_id_b`, `contestant_third_name`, `contestant_third_address`, `contestant_third_phone`, `contestant_supervisor_name`, `contestant_supervisor_address`, `contestant_supervisor_email`, `contestant_supervisor_phone`, `contestant_school_name`, `contestant_school_address`, `contestant_school_phone`) VALUES
(2, 'a', 'a', '', '', '', 'a', '', '', '', '', '', 'a', '', '', '', '', '', 'a', '', '', '', '', '', '', 'a', '', ''),
(25, 'mirza', 'atnanahidiw@gmail.com', 'MCF009S_photo_1.png', 'MCF009S_id_1_f.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(26, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(31, 'lsad;lasd;a', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(32, ';''da;''sds;''a', 'aaa@dlsada.aaa', 'MCF016S_photo_1.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(33, 'dklsajda', 'a@sada.asa', 'MCF017S_photo_1.docx', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(34, 'ld;kasl;a', 'aa@asa.asa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(38, 'ljdklsajlksa', 'asd@sas.asa', 'MCF022S_photo_1.jpg', '', '', 'kdjsaljlak', '', 'jdaksjal', '', '', '', 'lfkk;ls', '', 'fjsdkjfklsdjkl', '', '', '', 'fjksejflka`', '', 'jdajldsal', 'djsklajkdsa', '', '', '', 'hfdshflsjl', '', 'vkldskfjskl'),
(39, 'sdjfklsjfkljdsklfjsl', 'atnana@sdjkak.asd', 'MCF023S_photo_1.jpg', '', '', 'kdsfjklsjkfds.sdadsa', '', 'skdjsklajlsjdlk', '', '', '', 'cjdklxvkdhvkjhdfkj', '', 'sdhjskahdkjashk', '', '', '', 'djsakldjaslkjdlksa', '', 'djskhsahkj', '', '', '', '', 'sdfhasdlas', '', 'ldksal;kd;lask;'),
(40, 'dslajdla', 'aaa@aaa.aaa', '', '', '', 'lc;ksal;dksal;', '', 'sdadsa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_seminar`
--

CREATE TABLE IF NOT EXISTS `contestant_seminar` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_address` text COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_institution_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_institution_address` text COLLATE utf8_bin NOT NULL,
  `contestant_institution_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=24 ;

--
-- Dumping data untuk tabel `contestant_seminar`
--

INSERT INTO `contestant_seminar` (`contestant_id`, `contestant_photo`, `contestant_id_f`, `contestant_id_b`, `contestant_name`, `contestant_address`, `contestant_email`, `contestant_phone`, `contestant_institution_name`, `contestant_institution_address`, `contestant_institution_phone`) VALUES
(4, '', '', '', 'a', 'a', 'a', 'a', 'a', '', ''),
(23, '', '', '', 'AAAA', '', 'atnanahidiw@gmail.com', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contestant_university`
--

CREATE TABLE IF NOT EXISTS `contestant_university` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `contestant_team_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_first_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_first_address` text COLLATE utf8_bin NOT NULL,
  `contestant_first_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_second_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_second_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_second_address` text COLLATE utf8_bin NOT NULL,
  `contestant_second_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_third_photo` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_f` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_id_b` varchar(20) COLLATE utf8_bin NOT NULL,
  `contestant_third_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_third_address` text COLLATE utf8_bin NOT NULL,
  `contestant_third_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_address` text COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_email` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_supervisor_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  `contestant_university_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `contestant_university_address` text COLLATE utf8_bin NOT NULL,
  `contestant_university_phone` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`contestant_id`),
  UNIQUE KEY `contestant_team_name` (`contestant_team_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=44 ;

--
-- Dumping data untuk tabel `contestant_university`
--

INSERT INTO `contestant_university` (`contestant_id`, `contestant_team_name`, `contestant_email`, `contestant_first_photo`, `contestant_first_id_f`, `contestant_first_id_b`, `contestant_first_name`, `contestant_first_address`, `contestant_first_phone`, `contestant_second_photo`, `contestant_second_id_f`, `contestant_second_id_b`, `contestant_second_name`, `contestant_second_address`, `contestant_second_phone`, `contestant_third_photo`, `contestant_third_id_f`, `contestant_third_id_b`, `contestant_third_name`, `contestant_third_address`, `contestant_third_phone`, `contestant_supervisor_name`, `contestant_supervisor_address`, `contestant_supervisor_email`, `contestant_supervisor_phone`, `contestant_university_name`, `contestant_university_address`, `contestant_university_phone`) VALUES
(1, 'adadeh', 'adadeh', '', '', '', 'adadeh', '', '', '', '', '', 'adadeh', '', '', '', '', '', 'adadeh', '', '', '', '', '', '', 'adadeh', '', ''),
(43, 'mirza', 'atnanahidiw@gmail.com', '', '', '', 'hjhjkhkj', '', '1234567890', '', '', '', 'kljkljlk', '', '1234567890', '', '', '', 'kl;k;lk;l', '', '1234567890', '', '', '', '', ''';l;''l'';l''l''', '', '1234567890');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `contestant_paper`
--
ALTER TABLE `contestant_paper`
  ADD CONSTRAINT `contestant_paper_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_payment`
--
ALTER TABLE `contestant_payment`
  ADD CONSTRAINT `contestant_payment_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_problem`
--
ALTER TABLE `contestant_problem`
  ADD CONSTRAINT `contestant_problem_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_school`
--
ALTER TABLE `contestant_school`
  ADD CONSTRAINT `contestant_school_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `contestant_university`
--
ALTER TABLE `contestant_university`
  ADD CONSTRAINT `contestant_university_ibfk_1` FOREIGN KEY (`contestant_id`) REFERENCES `contestant` (`contestant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
