<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Cache-control" content="no-cache" />
	<meta http-equiv="Cache-control" content="must-revalidate" />
	<meta name="language" content="en" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/modif.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/contestant.css" />
	<?php Yii::app()->bootstrap->register(); ?>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-fileupload.min.js"></script>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
			'items'=>array(
                array('label'=>'Logout', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php switch(Yii::app()->user->type){
		case 1: echo '<h1>Modeling Universitas</h1>'; break;
		case 2: echo '<h1>Modeling SMA</h1>'; break;
		case 3: echo '<h1>Karya Ilmiah</h1>'; break;
		default: throw new CHttpException(404,'The requested page does not exist.');
		}
	?>

	<?php
	$items = array(
			array('label' => 'Halaman Utama', 'url' => Yii::app()->createUrl($this->id.'/index'), 'active' => ($this->action->id == 'index') ? true : false),
			array('label' => 'Data Diri', 'url' => Yii::app()->createUrl($this->id.'/profile'), 'active' => ($this->action->id == 'profile') ? true : false),
			);
	if((Yii::app()->user->paid == 0) && !Configuration::isContestStarted($this->id))
		array_push(
			$items, array('label' => 'Bukti Pembayaran', 'url' => Yii::app()->createUrl($this->id.'/payment'), 'active' => ($this->action->id == 'payment') ? true : false)
		);
	if( (Yii::app()->user->paid == 1) && Configuration::isContestStarted($this->id) ) 
		array_push(
			$items, array('label' => 'Soal', 'url' => Yii::app()->createUrl($this->id.'/problem'), 'active' => ($this->action->id == 'problem') ? true : false)
		);
	
	$this->widget('bootstrap.widgets.TbMenu', array(
		'type'=>'tabs',
		'items'=>$items,
	)); ?>
	
	<?php echo $content; ?>

</div><!-- page -->

</body>
</html>
