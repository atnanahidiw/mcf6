<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
//$this->breadcrumbs=array(
//	'Login',
//);
?>

<!-- <h1>Login</h1> -->
<div>
	<div><?php echo CHtml::link("Modeling Universitas",array('/university')); ?></div>
	<div><?php echo CHtml::link("Modeling SMA",array('/school')); ?></div>
	<div><?php echo CHtml::link("Karya Tulis",array('/paper')); ?></div>
</div>
<h3><br></h3>

<!-- <div class="form"> -->
<?php

if(Yii::app()->user->isGuest){
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<!-- 	<p class="note">Fields with <span class="required">*</span> are required.</p> -->
	<div class="control-group">
		<?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model,'username',array('size'=>10,'maxlength'=>10)); ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		</div>
	</div>
	<div class="rememberMe control-group" style="display:inline;">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Login',
        )); ?>
	</div>
<?php
$this->endWidget();
} else{
?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'link',
            'type'=>'primary',
            'label'=>'Logout',
			'url'=>CHtml::normalizeUrl(array('/site/logout')),
        )); ?>
	</div>
<?php } ?>
</div><!-- form -->
