<?php

class UniversalContestantController extends Controller
{
    /**
     * @var array model type code converter
     */
    private $model_type=array(
        'school'     => 1,
        'university' => 2,
        'paper'      => 3,        
        'seminar'    => 4,        
    );


    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/main2';
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('pendaftaran', 'index'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('profil', 'masalah', 'motivation_letter', 'topik', 'pengumpulan', 'index'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin', 'repaid', 'paid', 'complete', 'start', 'stop', 'delete', 'view', 'index'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * used on all process in the child class
     * @param string $model_type the contest type
     */
    public function allRegistration($model_type)
    {
        $model = null;
        $file_count = 0;
        $this->layout = '//layouts/main';

        $config        =    new Configuration();
        $contestant    =    new Contestant;
        switch($model_type){
            case 'university' : $model = new University;  $file_count = 9;  break;
            case 'school'     : $model = new School;      $file_count = 9;  break;
            case 'paper'      : $model = new Paper;       $file_count = 3;  break;
            case 'seminar'    : $model = new Seminar;     $file_count = 3;  break;
        }

        if (Configuration::isContestStarted($model_type)) { 
        
            if(isset($_POST[ucfirst($model_type)]))
            {
                $contestant->fill();
                $model->fill();
                $model->attributes=$_POST[ucfirst($model_type)];
                if($model->validate()){
                    $contestant->contestant_username                  = $config->createID($model_type);
                    $contestant->contestant_type                      = $this->model_type[$model_type];
                    $contestant->save();
                    $model->contestant_id=$contestant->contestant_id;
                    $foldername=$contestant->contestant_username;
                    if($model_type != 'seminar'){
                        if($file_count == 3){
                            for($i=0;$i<3;++$i){
                                if(!empty($_FILES['file-'.$i]['name'])){
                                    $temp = explode('.', $_FILES['file-'.$i]['name']);
                                    $filename = '.'.end($temp);
                                    switch($i){
                                        case 0 : $filename=$foldername.'_photo'.$filename; $model->contestant_photo=$filename;  break;
                                        case 1 : $filename=$foldername.'_id_f'.$filename;  $model->contestant_id_f=$filename;   break;
                                        case 2 : $filename=$foldername.'_id_b'.$filename;  $model->contestant_id_b=$filename;   break;
                                    }
                                    move_uploaded_file($_FILES['file-'.$i]['tmp_name'], 'upload/'.$filename);
                                    copy('upload/'.$filename, 'backup/'.$filename);
                                }
                            }
                        }
                        else{
                            for($i=0;$i<9;++$i){
                                if(!empty($_FILES['file-'.$i]['name'])){
                                    $temp = explode('.', $_FILES['file-'.$i]['name']);
                                    $filename = '.'.end($temp);
                                    switch($i){
                                        case 0 : $filename=$foldername.'_photo_1'.$filename; $model->contestant_first_photo=$filename;  break;
                                        case 1 : $filename=$foldername.'_id_1_f'.$filename;  $model->contestant_first_id_f=$filename;   break;
                                        case 2 : $filename=$foldername.'_id_1_b'.$filename;  $model->contestant_first_id_b=$filename;   break;
                                        case 3 : $filename=$foldername.'_photo_2'.$filename; $model->contestant_second_photo=$filename; break;
                                        case 4 : $filename=$foldername.'_id_2_f'.$filename;  $model->contestant_second_id_f=$filename;  break;
                                        case 5 : $filename=$foldername.'_id_2_b'.$filename;  $model->contestant_second_id_b=$filename;  break;
                                        case 6 : $filename=$foldername.'_photo_3'.$filename; $model->contestant_third_photo=$filename;  break;
                                        case 7 : $filename=$foldername.'_id_3_f'.$filename;  $model->contestant_third_id_f=$filename;   break;
                                        case 8 : $filename=$foldername.'_id_3_b'.$filename;  $model->contestant_third_id_b=$filename;   break;
                                    }
                                    move_uploaded_file($_FILES['file-'.$i]['tmp_name'], 'upload/'.$filename);
                                    copy('upload/'.$filename, 'backup/'.$filename);
                                }
                            }
                        }
                    }
                }
                if($model->save()){
                    Yii::app()->user->setFlash('success', 'Selamat Pendaftaran Anda Berhasil.<br>Silahkan cek inbox untuk melihat email konfirmasi.<br>(Jika tidak ada di inbox coba cek di folder spam)');
                    $this->sendRegisteredEmail($model);
                }
            }

        } else {
            switch($model_type){
                case 'university' : $message = 'Mohon maaf, pendaftaran saat ini sudah ditutup.<br/>Jika anda memiliki pertanyaan lebih lanjut, silahkan hubungi panitia melalui <a href="'.Yii::app()->createUrl('kontak').'">kontak</a> yang tersedia.<br><br>Terima kasih,<br>Panitia MCF-MMC ITB 2014';  break;
                case 'school'     : $message = 'Mohon maaf, pendaftaran saat ini sudah ditutup.<br/>Jika anda memiliki pertanyaan lebih lanjut, silahkan hubungi panitia melalui <a href="'.Yii::app()->createUrl('kontak').'">kontak</a> yang tersedia.<br><br>Terima kasih,<br>Panitia MCF-MMC ITB 2014';  break;
                case 'paper'      : $message = 'Mohon maaf, pendaftaran saat ini sudah ditutup.<br/>Jika anda memiliki pertanyaan lebih lanjut, silahkan hubungi panitia melalui <a href="'.Yii::app()->createUrl('kontak').'">kontak</a> yang tersedia.<br><br>Terima kasih,<br>Panitia MCF-MMC ITB 2014';  break;
                case 'seminar'    : $message = 'Mohon maaf, pendaftaran saat ini sudah ditutup.<br/>Jika anda memiliki pertanyaan lebih lanjut, silahkan hubungi panitia melalui <a href="'.Yii::app()->createUrl('kontak').'">kontak</a> yang tersedia.<br><br>Terima kasih,<br>Panitia MCF-MMC ITB 2014';  break;
            }
            Yii::app()->user->setFlash('error', $message);
        }

        $this->render('registration',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * used after load model on the child class
     * @param this $model the model to be deleted
     */
    public function allDelete($model)
    {
        Contestant::model()->findByPk($model->contestant_id)->delete();
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
//    public function actionIndex()
//    {
//        $dataProvider=new CActiveDataProvider('University');
//        $this->render('index',array(
//            'dataProvider'=>$dataProvider,
//        ));
//    }
    
    /**
     * Show Contest Description
     * used after load model on the child class
     * @param string $model_type the contest type
     * @param this $model the model to be shown
     */
    public function afterIndex($model_type, $model = null)
    {
        if($model != null)
        {            
            $this->render('index',array(
                'model'=>$model,
            ));
        }
        else{
            $this->layout = '//layouts/main';
            $this->render('description', array(
                'model_type' => $model_type,
            ));
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
//    public function actionView($id)
//    {
//        $this->render('view',array(
//            'model'=>$this->loadModel($id),
//        ));
//    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
//    public function actionUpdate($id)
//    {
//        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

//        if(isset($_POST['University']))
//        {
//            $model->attributes=$_POST['University'];
//            if($model->save())
//                $this->redirect(array('view','id'=>$model->contestant_id));
//        }
//
//        $this->render('update',array(
//            'model'=>$model,
//        ));
//    }

    /**
     * Show Contestant profile
     * used after load model on the child class
     * @param string $model_type the contest type
     * @param this $model the model to be shown
     */
    public function afterProfile($model_type, $model = null)
    {
        if((Yii::app()->user->id != 0) && (Yii::app()->user->status == 2)) throw new CHttpException(404,'The requested page does not exist.');
        else{
            if($this->model_type[$model_type]<3) $file_count=9; else $file_count=3;

            if(((!Yii::app()->user->isGuest) && (Yii::app()->user->type==$this->model_type[$model_type])) || (Yii::app()->user->id == 0))
            {
                if(isset($_POST[ucfirst($model_type)]))
                {
                    $foldername=Contestant::model()->findByPk($model->contestant_id)->contestant_username;
                    if($file_count == 3){
                        for($i=0;$i<3;++$i){
                            if(!empty($_FILES['file-'.$i]['name'])){
                                $temp = explode('.', $_FILES['file-'.$i]['name']);
                                $filename = '.'.end($temp);
                                switch($i){
                                    case 0 : $filename=$foldername.'_photo'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_photo=$filename;
                                             break;
                                    case 1 : $filename=$foldername.'_id_f'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_id_f=$filename;
                                             break;
                                    case 2 : $filename=$foldername.'_id_b'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_id_b=$filename;
                                             break;
                                }
                                move_uploaded_file($_FILES['file-'.$i]['tmp_name'], 'upload/'.$filename);
                                if((strlen($filename)!=0) && (file_exists('backup/'.$filename))) unlink('backup/'.$filename);
                                copy('upload/'.$filename, 'backup/'.$filename);
                            }
                        }
                    }
                    else{
                        for($i=0;$i<9;++$i){
                            if(!empty($_FILES['file-'.$i]['name'])){
                                $temp = explode('.', $_FILES['file-'.$i]['name']);
                                $filename = '.'.end($temp);
                                switch($i){
                                    case 0 : $filename=$foldername.'_photo_1'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_first_photo=$filename;
                                             break;
                                    case 1 : $filename=$foldername.'_id_1_f'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_first_id_f=$filename;
                                             break;
                                    case 2 : $filename=$foldername.'_id_1_b'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_first_id_b=$filename;
                                             break;
                                    case 3 : $filename=$foldername.'_photo_2'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_second_photo=$filename;
                                             break;
                                    case 4 : $filename=$foldername.'_id_2_f'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_second_id_f=$filename;
                                             break;
                                    case 5 : $filename=$foldername.'_id_2_b'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_second_id_b=$filename;
                                             break;
                                    case 6 : $filename=$foldername.'_photo_3'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_third_photo=$filename;
                                             break;
                                    case 7 : $filename=$foldername.'_id_3_f'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_third_id_f=$filename;
                                             break;
                                    case 8 : $filename=$foldername.'_id_3_b'.$filename;
                                             if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                                             $model->contestant_third_id_b=$filename;
                                             break;
                                }
                                move_uploaded_file($_FILES['file-'.$i]['tmp_name'], 'upload/'.$filename);
                                if((strlen($filename)!=0) && (file_exists('backup/'.$filename))) unlink('backup/'.$filename);
                                copy('upload/'.$filename, 'backup/'.$filename);
                            }
                        }
                    }
                    $model->attributes=$_POST[ucfirst($model_type)];
                    $model->save();
                }

                $this->render('profile',array(
                    'model'=>$model,
                ));
            }
            else throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    /**
     * Show Problem upload page
     * used on all process in the child class
     * @param string $model_type the contest type
     */
    public function allProblem($model_type)
    {
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==$this->model_type[$model_type]))
        {
            $error = '';
            $filename = '';
            $file = Problem::model()->findByAttributes(array('contestant_id' => Yii::app()->user->id));
            
            if(!empty($_FILES)){
                $foldername = Contestant::model()->findByPk(Yii::app()->user->id)->contestant_username;
                
                $allowedExts = array('pdf');
                $temp = explode('.', $_FILES['file']['name']);
                $extension = end($temp);

                if ($_FILES['file']['size'] > 10*1024*1024){
                    $error = 'Ukuran berkas terlalu besar';
                }
                else if (($_FILES['file']['type'] == 'application/pdf')
                && in_array($extension, $allowedExts))
                {
                    if ($_FILES['file']['error'] > 0) foreach($_FILES['file']['error'] as $temp_error) $error .= $temp_error . '<br>';
                }
                else $error = 'Berkas tidak valid';

                if($error !== '') Yii::app()->user->setFlash('error', $error);
                else{
                    if($file == null){
                        $file = new Problem();
                        $file->contestant_id = Yii::app()->user->id;
                        $file->file_name = $foldername.'/'.$_FILES['file']['name'];
                        $file->save();
                        mkdir(__DIR__.'/../../upload/'.$foldername, 0666);
                    }
                    else{
                        Problem::model()->updateAll(array('file_name' => $foldername.'/'.$_FILES['file']['name']), 'contestant_id = '.Yii::app()->user->id);
                        $file = Problem::model()->findByAttributes(array('contestant_id' => Yii::app()->user->id));
                    }
                    move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$foldername.'/'.$_FILES['file']['name']);
                    Yii::app()->user->setFlash('success', 'Berkas berhasil diunggah');
                }
            }

            $this->render('problem', array(
                'problem'=>'',
                'filename'=>'',
                'submit'=>$file,
            ));        
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }

    /**
     * Manages all models
     * used on all process in the child class
     * @param string $model_type the contest type
     */
    public function allAdmin($model_type)
    {
        $this->layout = 'main3';
  
        $criteria = array(
            'with' => array('contestant'),
            'order' => 'contestant.contestant_status',
        );
        switch($model_type){
            case 'university' : $model = University::model()->findAll($criteria);  break;
            case 'school'     : $model = School::model()->findAll($criteria);      break;
            case 'paper'      : $model = Paper::model()->findAll($criteria);       break;
            case 'seminar'    : $model = Seminar::model()->findAll($criteria);     break;
        }

        //$model->unsetAttributes();  // clear any default values
        //if(isset($_GET[ucfirst($model_type)]))
        //    $model->attributes=$_GET[ucfirst($model_type)];
        
        $this->render('admin',array(
            'model'=>$model,
            'model_type' => $model_type,
        ));
    }

    /**
     * Verify particular model.
     * If verification is successful, the browser will be redirected to the 'admin' page.
     * @param this $id the model id to be verified
     * @param this $type the verification type (0 => paid verification; 1 => complete data verification)
     */
    public function allVerify($id, $type)
    {
        $contestant = Contestant::model()->findByPk($id);
        if(($contestant->contestant_status == 0) && ($type == 0)){
            $password = substr(md5(uniqid()), 20, 14);
            $user = new UserIdentity($contestant->contestant_username, '');
            $contestant->contestant_password = $user->encrypt($password);
            switch(date('N', strtotime('now'))){
                case 6  : $expired = date("Y-m-d", strtotime('+6 day'));  break;
                case 7  : $expired = date("Y-m-d", strtotime('+5 day'));  break;
                default : $expired = date("Y-m-d", strtotime('+1 week'));
            }
            switch($contestant->contestant_type){
                case 1  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['schoolEndRegistration']));  break;
                case 2  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['universityEndRegistration']));  break;
                case 3  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['universityEndRegistration']));  break;
                case 4  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['universityEndRegistration']));  break;
                default : $end_registration = $expired;
            }
            if($expired > $end_registration) $expired = $end_registration;
            $contestant->contestant_expired_registration_date = $expired;
            $contestant->contestant_status = 1;
            if($contestant->save()) $this->sendVerifiedEmail($contestant, $password);
        } else if(($contestant->contestant_status == 1) && ($type == 1)){
        //    $this->sendParticipantCard($contestant);
            $contestant->contestant_status = 2;
            if($contestant->save()) $this->sendCompletedEmail($contestant);
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Retry to verify particular model.
     * If verification is successful, the browser will be redirected to the 'admin' page.
     * @param this $id the model id to be verified
     * @param this $type the verification type (0 => paid verification; 1 => complete data verification)
     */
    public function retryVerify($id, $type)
    {
        $contestant = Contestant::model()->findByPk($id);
        if($type == 0){
            $password = substr(md5(uniqid()), 20, 14);
            $user = new UserIdentity($contestant->contestant_username, '');
            $contestant->contestant_password = $user->encrypt($password);
            switch(date('N', strtotime('now'))){
                case 6  : $expired = date("Y-m-d", strtotime('+6 day'));  break;
                case 7  : $expired = date("Y-m-d", strtotime('+5 day'));  break;
                default : $expired = date("Y-m-d", strtotime('+1 week'));
            }
            switch($contestant->contestant_type){
                case 1  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['schoolEndRegistration']));  break;
                case 2  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['universityEndRegistration']));  break;
                case 3  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['universityEndRegistration']));  break;
                case 4  : $end_registration = date("Y-m-d", strtotime(Yii::app()->params['universityEndRegistration']));  break;
                default : $end_registration = $expired;
            }
            if($expired > $end_registration) $expired = $end_registration;
            $contestant->contestant_expired_registration_date = $expired;
            $contestant->contestant_status = 1;
            if($contestant->save()) $this->sendVerifiedEmail($contestant, $password);
        } else if($type == 1){
        //    $this->sendParticipantCard($contestant);
            $contestant->contestant_status = 2;
            if($contestant->save()) $this->sendCompletedEmail($contestant);
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Send registered confirmation email 
     * @param $model that will be sent
     */
    protected function sendRegisteredEmail($model)
    {
        $contestant = Contestant::model()->findByPk($model->contestant_id);

        switch($contestant->contestant_type){
            case 1 : $competition = 'Kompetisi Pemodelan Siswa';      $amount=150000; break;
            case 2 : $competition = 'Kompetisi Pemodelan Mahasiswa';  $amount=300000; break;
            case 3 : $competition = 'Kontes Menulis Esai';            $amount=50000;  break;
            case 4 : $competition = 'Seminar Nasional Matematika';
                     ($model->contestant_institution_type == 0)?$amount=35000:$amount=20000;
                     break;
        }
        //send it via email
        $recipient  = ($contestant->contestant_type<3) ? 'Tim '.$model->contestant_team_name : $model->contestant_name;
        $subject    = 'Konfirmasi Pendaftaran '.$competition.' MCF-MMC ITB 2014';
        $reg_nmbr   = $contestant->contestant_type.substr($contestant->contestant_username, 3, -1);
        $cost       = ($amount+(int)$reg_nmbr) / 1000;

        $body = 'Halo '.$recipient.',<br><br>';
        $body .= 'Terima kasih telah mendaftar di '.$competition.' MCF-MMC ITB 2014.<br>';
        $body .= 'Nomor Pendaftaran Anda adalah: <strong>'.$reg_nmbr.'</strong>.<br><br><br>';
        $body .= 'Mohon untuk segera melakukan pelunasan pembayaran sebesar <strong>Rp '.sprintf('%.3f', $cost).',00 </strong> dengan melakukan transfer ke salah satu nomor rekening di bawah ini:<br><br>';
        $body .= '<div style="margin-left:20px;"><strong>BNI</strong><br>0303961960<br>Kantor Cabang Perintis Kemerdekaan Bandung<br>a.n. Resty Deniawanty</div><br>';
        $body .= '<div style="margin-left:20px;"><strong>BCA</strong><br>0153025298<br>Kantor Cabang Utama Gladag Surakarta<br>a.n. Yozef Giovanni Tjandra</div><br>';
        $body .= '<div style="margin-left:20px;"><strong>Bank Mandiri</strong><br>1380009729554<br>Kantor Cabang Gladag Surakarta<br>a.n. Yozef Giovanni Tjandra</div><br>';
        $body .= 'Setelah selesai melakukan pembayaran, <strong>mohon untuk melakukan konfirmasi pembayaran</strong> dengan mengirimkan SMS ke 085624788307 (Resty) dengan format SMS:<br><br>';
        $body .= '<div style="margin-left:20px;">< Nama '.(($contestant->contestant_type<3) ? 'Tim' : 'Peserta').' ><br>< No Registrasi ><br>< Acara Yang Diikuti ><br>< Total Transfer ><br>< Transfer Ke Bank ></div><br>';
        $body .= 'Besar uang yang ditransfer harus tepat sebesar yang dicantumkan pada email ini. <strong>Kesalahan besar uang yang ditransfer dapat mengakibatkan tidak diprosesnya pendaftaran peserta</strong>. Penyelesaian masalah pembayaran dapat dilakukan dengan mengirimkan SMS/WA ke nomor kontak 085624788307 (Resty).<br><br>';
        $body .= 'Harap peserta menyimpan bukti transfer. Jika sewaktu-waktu terjadi masalah pembayaran, panitia tidak akan memproses masalah pembayaran peserta tanpa bukti transfer.<br><br>';
        $body .= 'Terima kasih atas perhatiannya.<br><br><br><br>';
        $body .= "Salam,<br>";
        $body .= "Panitia MCF-MMC ITB 2014";

        //Yii::log('error','paper',$subject.$body.$headers);

        $contestant->sendEmail($subject, $recipient, $model->contestant_email, $body);
    }

    /**
     * Send verified confirmation email 
     * @param $contestant that will be sent
     * @param $password that was generated
     */
    protected function sendVerifiedEmail($contestant, $password)
    {
        switch($contestant->contestant_type){
            case 1 : $model = School::model()->findByPk($contestant->contestant_id);      $competition = "Kompetisi Pemodelan Siswa";      $deadline = "";  break;
            case 2 : $model = University::model()->findByPk($contestant->contestant_id);  $competition = "Kompetisi Pemodelan Mahasiswa";  $deadline = " atau tanggal 4 Desember 2013";  break;
            case 3 : $model = Paper::model()->findByPk($contestant->contestant_id);       $competition = "Kontes Menulis Esai";            $deadline = "";  break;
            case 4 : $model = Seminar::model()->findByPk($contestant->contestant_id);     $competition = "Seminar Nasional Matematika";    $deadline = "";  break;
        }
        //send it via email
        $recipient  = ($contestant->contestant_type<3) ? 'Tim '.$model->contestant_team_name : $model->contestant_name;
        $subject    = 'Konfirmasi Pembayaran '.$competition.' MCF-MMC ITB 2014';
        
        $body = 'Halo '.$recipient.',<br><br>';
        $body .= 'Melalui email ini kami beritahukan bahwa pembayaran biaya pendaftaran untuk '.$competition.' MCF-MMC ITB 2014 sudah kami terima.<br><br>';
        $body .= 'Berikut akun yang dapat Anda gunakan untuk masuk ke laman peserta:<br>';
        $body .= 'Username : '.$contestant->contestant_username.'<br>';
        $body .= 'Password : '.$password.'<br><br>';
        $body .= 'Mohon untuk segera melengkapi profil dan juga kelengkapan administrasi Anda pada web <a href="www.math.itb.ac.id/mcf-mmc">MCF - MMC ITB 2014</a>. Jika peserta tidak melengkapi setiap persyaratan administrasi maka peserta akan dianggap mengundurkan diri dan uang pendaftaran tidak akan dikembalikan.<br><br><br><br>';
        $body .= 'Salam,<br>';
        $body .= 'Panitia MCF-MMC ITB 2014';

        //Yii::log('error','paper',$subject.$body.$headers);

        $contestant->sendEmail($subject, $recipient, $model->contestant_email, $body);
    }

    /**
     * Send completed confirmation email 
     * @param $contestant that will be sent
     */
    protected function sendCompletedEmail($contestant)
    {
        switch($contestant->contestant_type){
            case 1 : $model = School::model()->findByPk($contestant->contestant_id);      $competition = "Kompetisi Pemodelan Siswa";      break;
            case 2 : $model = University::model()->findByPk($contestant->contestant_id);  $competition = "Kompetisi Pemodelan Mahasiswa";  break;
            case 3 : $model = Paper::model()->findByPk($contestant->contestant_id);       $competition = "Kontes Menulis Esai";            break;
            case 4 : $model = Seminar::model()->findByPk($contestant->contestant_id);     $competition = "Seminar Nasional Matematika";    break;
        }
        //send it via email
        $recipient  = ($contestant->contestant_type<3) ? 'Tim '.$model->contestant_team_name : $model->contestant_name;
        $prefix     = ($contestant->contestant_type<3) ? 'Tim Anda' : 'Anda';
        $subject    = 'Konfirmasi Kelengkapan Data Peserta '.$competition.' MCF-MMC ITB 2014';

        $body = 'Halo '.$recipient.',<br><br>';
        $body .= 'Selamat! '.$prefix.' sudah resmi terdaftar sebagai peserta '.$competition.' MCF-MMC ITB 2014.<br>';
        switch($contestant->contestant_type){
            case 1 : $body .= 'Terima kasih atas partisipasinya, sampai berjumpa di babak penyisihan pada tanggal '.Yii::app()->params['school']['preliminary'].'!<br>';
                     $body .= 'Jangan lupa untuk meluangkan waktu membaca Peraturan dan Teknis kompetisi kami di <a href="http://www.math.itb.ac.id/mcf-mmc/berkas/KPMS%202014.pdf">KPMS 2014</a><br><br><br><br>';
                     break;
            case 2 : $body .= 'Terima kasih atas partisipasinya, sampai berjumpa di periode pemodelan pada tanggal '.Yii::app()->params['university']['semifinal']['begin'].' - '.Yii::app()->params['university']['semifinal']['end'].'!<br>';
                     $body .= 'Jangan lupa untuk meluangkan waktu membaca Peraturan dan Teknis kompetisi kami di <a href="http://www.math.itb.ac.id/mcf-mmc/berkas/KPMM%202014.pdf">KPMM 2014</a><br><br><br><br>';
                     break;
            case 3 : $body .= 'Terima kasih atas partisipasinya, sampai berjumpa di waktu penerimaan berkas esai adalah tanggal '.Yii::app()->params['paper']['semifinal']['begin'].' - '.Yii::app()->params['paper']['semifinal']['end'].'!<br>';
                     $body .= 'Jangan lupa untuk meluangkan waktu membaca Peraturan dan Teknis kompetisi kami di <a href="http://www.math.itb.ac.id/mcf-mmc/berkas/KMES%202014.pdf">KMES 2014</a><br><br><br><br>';
                     break;
            case 4 : $body .= 'Terima kasih atas partisipasinya, sampai berjumpa di seminar pada tanggal '.Yii::app()->params['seminar']['time'].'!';
                     break;
        }
        $body .= "Salam,<br>";
        $body .= "Panitia MCF-MMC ITB 2014";

        //Yii::log('error','paper',$subject.$body.$headers);

        $contestant->sendEmail($subject, $recipient, $model->contestant_email, $body);
    }

    /**
     * Send participant card via email 
     * @param $contestant that will be sent
     */
    protected function sendParticipantCard($contestant)
    {
        $model = null;
        switch($contestant->contestant_type){
            case 1 : $model = School::model()->findByPk($contestant->contestant_id);      $competition = "Kompetisi Pemodelan Siswa";      break;
            case 2 : $model = University::model()->findByPk($contestant->contestant_id);  $competition = "Kompetisi Pemodelan Mahasiswa";  break;
            case 3 : $model = Paper::model()->findByPk($contestant->contestant_id);       $competition = "Kontes Menulis Esai";            break;
            case 4 : $model = Seminar::model()->findByPk($contestant->contestant_id);     $competition = "Seminar Nasional Matematika";    break;
        }
        //send it via email
        $recipient  = ($contestant->contestant_type<3) ? 'Tim '.$model->contestant_team_name : $model->contestant_name;
        $subject    = 'Kartu peserta '.$competition.' MCF-MMC ITB 2014';
        
        $body = 'Halo '.$recipient.',<br><br>';
        $body .= 'Melalui email ini kami lampirkan kartu peserta untuk '.$competition.' MCF-MMC ITB 2014.<br><br>';
        $body .= 'Mohon simpan kartu ini baik-baik, karena akan digunakan di kompetisi nanti.<br><br><br><br>';
        $body .= 'Salam,<br>';
        $body .= 'Panitia MCF-MMC ITB 2014';

        //generate participant card
        $pdf = new PdfGenerator(PdfGenerator::TYPE_PARTICIPANT_CARD);
        
        //check contestant type
        switch ($contestant->contestant_type) {
            case 2:
                //university

                if (empty($model)) {
                    throw new CHttpException(404,'Sertfikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $education_name = $model->contestant_university_name;

                    //user 1
                    if (!empty($model->contestant_first_name)) {
                        $pdf->addCard($model->contestant_first_name, Yii::app()->user->id, $education_name);
                    }
    
                    //user 2
                    if (!empty($model->contestant_second_name)) {
                        $pdf->addCard($model->contestant_second_name, Yii::app()->user->id, $education_name);
                    }
    
                    //user 3
                    if (!empty($model->contestant_third_name)) {
                        $pdf->addCard($model->contestant_third_name, Yii::app()->user->id, $education_name);
                    }
                }
            break;
        }

        //generate random name
        $filename = (rand()%1000)."-".date("d-m-Y").".pdf"; //error_log("filename: ".$filename);

        $pdf->Output($filename, 'F');
        //$pdf->Output();
        
        //send certificate
        $contestant->sendEmail($subject, $recipient, $model->contestant_email, $body, array('path' => $filename, 'name' => 'certificate.pdf'));
    
        //delete temporary file
        unlink($filename);
    }

    /**
     * Activate contest
     * @param string $model_type the contest type
     */
    public function regButton($model_type, $action)
    {
        $config_model = Configuration::model()->findByAttributes(array('name' => $model_type.'Started'));
        if($action == 'start') $config_model->value = 'true'; else $config_model->value = 'false';
        $config_model->save();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
}