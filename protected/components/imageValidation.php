<?php

class imageValidation extends CValidator
{
    public $maxwidth = 1024;
    public $maxheight = 1024;
    public $maxFileSize = 1048576; //1MB
    //public $valid_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP);
    public $valid_types = array('image/gif', 'image/jpeg', 'image/png', 'image/bmp');

    protected function validateAttribute($object,$attribute)
    //public function imageValidation($attribute, $params = array('maxwidth' => 1024, 'maxheight' => 1024))
    {
        $img = null;
        error_log(serialize($_FILES));
        switch ($attribute) {
            // individual contest
            case 'contestant_photo': $img = $_FILES['file-0']; break;
            case 'contestant_id_f': $img = $_FILES['file-1']; break;
            case 'contestant_id_b': $img = $_FILES['file-2']; break;
            // team contest
            case 'contestant_first_photo': $img = $_FILES['file-0']; break;
            case 'contestant_first_id_f': $img = $_FILES['file-1']; break;
            case 'contestant_first_id_b': $img = $_FILES['file-2']; break;
            case 'contestant_second_photo': $img = $_FILES['file-3']; break;
            case 'contestant_second_id_f': $img = $_FILES['file-4']; break;
            case 'contestant_second_id_b': $img = $_FILES['file-5']; break;
            case 'contestant_third_photo': $img = $_FILES['file-6']; break;
            case 'contestant_third_id_f': $img = $_FILES['file-7']; break;
            case 'contestant_third_id_b': $img = $_FILES['file-8']; break;
        }

        /*$image_info = getimagesize($img);
        $imageWidth = $image_info[0];
        $imageHeight = $image_info[1];*/

        if (!empty($img['name'])) {
            $imageType = $img['type'];
            $imageSize = $img['size'];

            //file type validation
            if (!in_array($imageType, $this->valid_types))
            {
                $this->addError($object, $attribute, 'This file type is not allowed. Please only use .jpg, .png, .gif, or .bmp files');
            }

            //image size validation
            /*if($imageWidth < $this->maxwidth || $imageHeight < $this->maxheight )
            {
                $this->addError($object,$attribute, 'your password is not strong enough!');
            }*/

            //file size validation
            if ($imageSize > $this->maxFileSize)
            {
                $this->addError($object, $attribute, 'Ukuran {attribute} terlalu besar (maksimal 1MB)');
            }
        }

        return 0;
    }
}