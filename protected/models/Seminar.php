<?php

/**
 * This is the model class for table "contestant_seminar".
 *
 * The followings are the available columns in table 'contestant_seminar':
 * @property integer $contestant_id
 * @property string $contestant_id_number
 * @property string $contestant_name
 * @property string $contestant_address
 * @property string $contestant_email
 * @property string $contestant_phone
 * @property string $contestant_institution_name
 * @property string $contestant_institution_address
 * @property string $contestant_institution_phone
 * @property string $contestant_institution_type
 *
 * The followings are the available model relations:
 * @property Contestant $contestant
 */
class Seminar extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Seminar the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contestant_seminar';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contestant_email', 'unique', 'message'=>'{attribute} sudah terdaftar'),
            array('contestant_id_number, contestant_name, contestant_phone, contestant_email, contestant_institution_name, contestant_institution_type', 'required', 'message'=>'{attribute} tidak boleh kosong'),
            array('contestant_name, contestant_email, contestant_institution_name', 'length', 'max'=>50),
            array('contestant_phone', 'length', 'max'=>30),
            array('contestant_id, contestant_photo, contestant_id_f, contestant_id_b, contestant_name, contestant_address, contestant_email, contestant_phone, contestant_institution_name, contestant_institution_address, contestant_institution_phone', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('contestant_id, contestant_photo, contestant_id_f, contestant_id_b, contestant_name, contestant_address, contestant_email, contestant_phone, contestant_institution_name, contestant_institution_address, contestant_institution_phone', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contestant' => array(self::BELONGS_TO, 'Contestant', 'contestant_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'contestant_id_number' => 'Nomor Kartu Identitas',
            'contestant_name' => 'Nama',
            'contestant_address' => 'Alamat',
            'contestant_email' => 'Email',
            'contestant_phone' => 'Nomor Kontak',
            'contestant_photo' => 'Pasfoto Peserta',
            'contestant_id_f' => 'Foto Kartu Identitas Peserta (depan)',
            'contestant_id_b' => 'Foto Kartu Identitas Peserta (belakang)',
            'contestant_institution_name' => 'Nama Institusi',
            'contestant_institution_address' => 'Alamat Institusi',
            'contestant_institution_phone' => 'Nomor Kontak Institusi',
            'contestant_institution_type' => 'Tipe Institusi',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('contestant_id',$this->contestant_id);
        $criteria->compare('contestant_name',$this->contestant_name,true);
        $criteria->compare('contestant_phone',$this->contestant_phone,true);
        $criteria->compare('contestant_address',$this->contestant_address,true);
        $criteria->compare('contestant_email',$this->contestant_email,true);
        $criteria->compare('contestant_institution_name',$this->contestant_institution_name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * fill the default value of Seminar
     */
    public function fill()
    {
        $this->contestant_id_number           = '';
        $this->contestant_name                = '';
        $this->contestant_address             = '';
        $this->contestant_email               = '';
        $this->contestant_phone               = '';
        $this->contestant_institution_name    = '';
        $this->contestant_institution_address = '';
        $this->contestant_institution_phone   = '';
        $this->contestant_institution_type    = 0;
    }
}