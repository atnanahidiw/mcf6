<?php

/**
 * This is the model class for table "contestant_school".
 *
 * The followings are the available columns in table 'contestant_school':
 * @property integer $contestant_id
 * @property string $contestant_team_name
 * @property string $contestant_email
 * @property string $contestant_first_photo
 * @property string $contestant_first_id_f
 * @property string $contestant_first_id_b
 * @property string $contestant_first_name
 * @property string $contestant_first_address
 * @property string $contestant_first_phone
 * @property string $contestant_second_photo
 * @property string $contestant_second_id_f
 * @property string $contestant_second_id_b
 * @property string $contestant_second_name
 * @property string $contestant_second_address
 * @property string $contestant_second_phone
 * @property string $contestant_third_photo
 * @property string $contestant_third_id_f
 * @property string $contestant_third_id_b
 * @property string $contestant_third_name
 * @property string $contestant_third_address
 * @property string $contestant_third_phone
 * @property string $contestant_supervisor_name
 * @property string $contestant_supervisor_nik
 * @property string $contestant_supervisor_address
 * @property string $contestant_supervisor_email
 * @property string $contestant_supervisor_phone
 * @property string $contestant_school_name
 * @property string $contestant_school_address
 * @property string $contestant_school_phone
 * @property integer $province
 *
 *
 * The followings are the available model relations:
 * @property Contestant $contestant
 */
class School extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return School the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contestant_school';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contestant_email', 'unique', 'message'=>'{attribute} sudah terdaftar'),
            array('contestant_team_name, contestant_school_phone, contestant_first_phone, contestant_email, contestant_first_name, contestant_school_name, province', 'required', 'message'=>'{attribute} tidak boleh kosong'),
            array('contestant_team_name, contestant_email, contestant_first_name, contestant_second_name, contestant_third_name, contestant_school_name', 'length', 'max'=>50),
            array('contestant_id, contestant_team_name, contestant_email, contestant_first_photo, contestant_first_id_f, contestant_first_id_b, contestant_first_name, contestant_first_address, contestant_first_phone, contestant_second_photo, contestant_second_id_f, contestant_second_id_b, contestant_second_name, contestant_second_address, contestant_second_phone, contestant_third_photo, contestant_third_id_f, contestant_third_id_b, contestant_third_name, contestant_third_address, contestant_third_phone, contestant_supervisor_name, contestant_supervisor_nik, contestant_supervisor_address, contestant_supervisor_email, contestant_supervisor_phone, contestant_school_name, contestant_school_address, contestant_school_phone, province', 'safe'),
            //type validation
            array('contestant_first_phone, contestant_second_phone, contestant_third_phone, contestant_supervisor_phone, contestant_school_phone', 'length', 'max'=>15),
            array('contestant_email, contestant_supervisor_email','email'),
            array('contestant_first_phone, contestant_second_phone, contestant_third_phone, contestant_supervisor_phone, contestant_school_phone','numerical'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('contestant_id, contestant_team_name, contestant_email, contestant_first_photo, contestant_first_id_f, contestant_first_id_b, contestant_first_name, contestant_first_address, contestant_first_phone, contestant_second_photo, contestant_second_id_f, contestant_second_id_b, contestant_second_name, contestant_second_address, contestant_second_phone, contestant_third_photo, contestant_third_id_f, contestant_third_id_b, contestant_third_name, contestant_third_address, contestant_third_phone, contestant_supervisor_name, contestant_supervisor_nik, contestant_supervisor_address, contestant_supervisor_email, contestant_supervisor_phone, contestant_school_name, contestant_school_address, contestant_school_phone', 'safe', 'on'=>'search'),
            //array('contestant_first_photo, contestant_first_id_f, contestant_first_id_b, contestant_second_photo, contestant_second_id_f, contestant_second_id_b, contestant_third_photo, contestant_third_id_f, contestant_third_id_b', 'application.components.imageValidation'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contestant' => array(self::BELONGS_TO, 'Contestant', 'contestant_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'contestant_team_name' => 'Nama Tim',
            'contestant_email' => 'Email',
            'contestant_first_name' => 'Nama Anggota #1',
            'contestant_first_address' => 'Alamat Anggota #1',
            'contestant_first_phone' => 'Nomor Kontak Anggota #1',
            'contestant_first_photo' => 'Pasfoto Peserta #1',
            'contestant_first_id_f' => 'Foto Kartu Identitas Peserta #1 (depan)',
            'contestant_first_id_b' => 'Foto Kartu Identitas Peserta #1 (belakang)',
            'contestant_second_name' => 'Nama Anggota #2',
            'contestant_second_address' => 'Alamat Anggota #2',
            'contestant_second_phone' => 'Nomor Kontak Anggota #2',
            'contestant_second_photo' => 'Pasfoto Peserta #2',
            'contestant_second_id_f' => 'Foto Kartu Identitas Peserta #2 (depan)',
            'contestant_second_id_b' => 'Foto Kartu Identitas Peserta #2 (belakang)',
            'contestant_third_name' => 'Nama Anggota #3',
            'contestant_third_address' => 'Alamat Anggota #3',
            'contestant_third_phone' => 'Nomor Kontak Anggota #3',
            'contestant_third_photo' => 'Pasfoto Peserta #3',
            'contestant_third_id_f' => 'Foto Kartu Identitas Peserta #3 (depan)',
            'contestant_third_id_b' => 'Foto Kartu Identitas Peserta #3 (belakang)',
            'contestant_supervisor_name' => 'Nama Supervisor',
            'contestant_supervisor_nik' => 'NIK Supervisor',
            'contestant_supervisor_address' => 'Alamat Supervisor',
            'contestant_supervisor_email' => 'Email Supervisor',
            'contestant_supervisor_phone' => 'Nomor Kontak Supervisor',
            'contestant_school_name' => 'Nama Sekolah',
            'contestant_school_address' => 'Alamat Sekolah',
            'contestant_school_phone' => 'Nomor Kontak Sekolah',
            'province' => 'Provinsi Asal Sekolah',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('contestant_id',$this->contestant_id);
        $criteria->compare('contestant_team_name',$this->contestant_team_name,true);
        $criteria->compare('contestant_phone',$this->contestant_phone,true);
        $criteria->compare('contestant_email',$this->contestant_email,true);
        $criteria->compare('contestant_first_name',$this->contestant_first_name,true);
        $criteria->compare('contestant_second_name',$this->contestant_second_name,true);
        $criteria->compare('contestant_third_name',$this->contestant_third_name,true);
        $criteria->compare('contestant_school_name',$this->contestant_school_name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * fill the default value of School
     */
    public function fill()
    {
        $this->contestant_team_name          = '';
        $this->contestant_email              = '';
        $this->contestant_first_name         = '';
        $this->contestant_first_address      = '';
        $this->contestant_first_phone        = '';
        $this->contestant_first_photo        = '';
        $this->contestant_first_id_f         = '';
        $this->contestant_first_id_b         = '';
        $this->contestant_second_name        = '';
        $this->contestant_second_address     = '';
        $this->contestant_second_phone       = '';
        $this->contestant_second_photo       = '';
        $this->contestant_second_id_f        = '';
        $this->contestant_second_id_b        = '';
        $this->contestant_third_name         = '';
        $this->contestant_third_address      = '';
        $this->contestant_third_phone        = '';
        $this->contestant_third_photo        = '';
        $this->contestant_third_id_f         = '';
        $this->contestant_third_id_b         = '';
        $this->contestant_supervisor_name    = '';
        $this->contestant_supervisor_nik     = '';
        $this->contestant_supervisor_address = '';
        $this->contestant_supervisor_email   = '';
        $this->contestant_supervisor_phone   = '';
        $this->contestant_school_name        = '';
        $this->contestant_school_address     = '';
        $this->contestant_school_phone       = '';
        $this->province                      = 0;
    }

    /**
     * Return an array contain the cde table for province's codes
     * @return array "province code table"
     */
    public static function get_all_province_code()
    {
        return array(
            1 => 'Aceh',
            2 => 'Sumatera Utara',
            3 => 'Sumatera Barat',
            4 => 'Riau',
            5 => 'Kepulauan Riau',
            6 => 'Jambi',
            7 => 'Sumatera Selatan',
            8 => 'Kepulauan Bangka Belitung',
            9 => 'Bengkulu',
            10 => 'Lampung',
            11 => 'DKI Jakarta',
            12 => 'Jawa Barat',
            13 => 'Banten',
            14 => 'Jawa Tengah',
            15 => 'DI Yogyakarta',
            16 => 'Jawa Timur',
            17 => 'Bali',
            18 => 'Nusa Tenggara Barat',
            19 => 'Nusa Tenggara Timur',
            20 => 'Kalimantan Barat',
            21 => 'Kalimantan Tengah',
            22 => 'Kalimantan Selatan',
            23 => 'Kalimantan Timur, Kalimantan Utara',
            24 => 'Sulawesi Utara',
            25 => 'Gorontalo',
            26 => 'Sulawesi Tengah',
            27 => 'Sulawesi Selatan',
            28 => 'Sulawesi Barat',
            29 => 'Sulawesi Tenggara',
            30 => 'Maluku',
            31 => 'Maluku Utara',
            32 => 'Papua',
            33 => 'Papua Barat',
        );
    }
}