<?php

/**
 * Contain all method related to pdf file generation
 * @author Abdurrahman Shofy Adianto <azophy@gmail.com>
 */
class PdfGenerator
{
    const TYPE_CERTIFICATE = 1;
    const TYPE_PARTICIPANT_CARD = 2;
    const NEW_CERTIFICATE = 3;

    private $_pdf;

    /**
     * Constructor
     * Create new FPDF object
     */
    public function __construct($type = 1)
    {
        switch ($type) {
            case ($this::TYPE_CERTIFICATE):
                Yii::import('application.vendors.fpdf.*');
                require('fpdf.php');

                $this->_pdf = new FPDF('L','pt','A4');
            break;

            case ($this::TYPE_PARTICIPANT_CARD):
                Yii::import('application.vendors.fpdf.*');
                require('fpdf.php');

                $this->_pdf = new FPDF('L','pt',array(1600,1200));
            break;

            case ($this::NEW_CERTIFICATE):
                Yii::import('application.vendors.fpdf.*');
                require('fpdf.php');

                $this->_pdf = new FPDF('L','pt',array(825,635));
            break;
        }
    }

    /**
     * Add new certificate page to the current FPDF object
     * @param string $contestant_name    The contestant's real name
     * @param string $education_name        The contestant education institution origin
     * @return void
    **/
    public function addCertificate($contestant_name, $education_name, $type = 0, $participation = false)
    {
        if ($type == 0) {
            $this->_pdf->AddPage();
            $this->_pdf->Image($background_image_path,0,0);
            $this->_pdf->SetFont('Arial','B',16);
            $this->_pdf->SetXY(0,325);
            $this->_pdf->Cell(800,10,$contestant_name,0,0,'C');
            $this->_pdf->SetXY(0,425);
            $this->_pdf->Cell(800,10,$education_name,0,0,'C');    
        } else {
            $background_image_path = __DIR__."/../../images/template_background_certificate.png";
            switch ($type) {
                case 1: //school
                $background_image_path = __DIR__."/../../images/KPMS2.jpg";
                break;
                case 2: //university
                $background_image_path = __DIR__."/../../images/KPMM2.jpg";
                break;
                case 3: //paper
                $background_image_path = __DIR__."/../../images/KMES2.jpg";
                break;
                case 4: //seminar
                break;
            }
            $this->_pdf->AddFont('Lucida','','lucida_handwriting_italic.php');
            $font_name = 'Lucida';
            $font_weight = '';

            $this->_pdf->AddPage();
            $this->_pdf->Image($background_image_path,0,0);
            $this->_pdf->SetFont($font_name,$font_weight,24);
            $this->_pdf->SetXY(10,250);
            $this->_pdf->Cell(800,10,$contestant_name,0,0,'C');
            $this->_pdf->SetFont($font_name,$font_weight,18);
            $this->_pdf->SetXY(10,275);
            $this->_pdf->Cell(800,10,$education_name,0,0,'C');
            if ($participation) {
                $this->_pdf->SetFont($font_name,$font_weight,18);
                $this->_pdf->SetXY(10,330);
                $this->_pdf->Cell(800,10,$participation,0,0,'C');
            }
        }
    }

    /**
     * Add new participant card page to the current FPDF object
     * @param string $contestant_name    The contestant's real name
     * @param string $education_name     The contestant education institution origin
     * @return void
    **/
    public function addCard($contestant_name, $contestant_number, $education_name)
    {
        $background_image_path = __DIR__."/../../images/template_background_participant_card.jpg";
        $this->_pdf->AddPage();
        $this->_pdf->Image($background_image_path,0,0);
        $this->_pdf->SetFont('Arial','B',60);
        $this->_pdf->SetXY(360,600);
        $this->_pdf->Cell(800,10,$contestant_name,0,0,'C');
        $this->_pdf->SetXY(360,700);
        $this->_pdf->Cell(800,10,$contestant_number,0,0,'C');
        $this->_pdf->SetXY(360,800);
        $this->_pdf->Cell(800,10,$education_name,0,0,'C');
    }

    /**
     * Wrapper for FPDF's "Output" method
     * @param string $filename 
     * @param string $mode
     */
    public function Output($filename = null, $mode = null)
    {
        $this->_pdf->Output($filename, $mode);
    }
}