<?php

/**
 * This is the model class for table "contestant_paper".
 *
 * The followings are the available columns in table 'contestant_paper':
 * @property integer $contestant_id
 * @property string $contestant_name
 * @property string $contestant_address
 * @property string $contestant_email
 * @property string $contestant_phone
 * @property string $contestant_photo
 * @property string $contestant_id_f
 * @property string $contestant_id_b
 * @property string $contestant_school_name
 * @property string $contestant_school_address
 * @property string $contestant_school_phone
 *
 * The followings are the available model relations:
 * @property Contestant $contestant
 */
class Paper extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Paper the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contestant_paper';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contestant_email', 'unique', 'message'=>'{attribute} sudah terdaftar'),
            array('contestant_name, contestant_phone, contestant_email, contestant_school_name, contestant_school_phone', 'required', 'message'=>'{attribute} tidak boleh kosong'),
            array('contestant_name, contestant_email, contestant_school_name', 'length', 'max'=>50),
            array('contestant_phone, contestant_school_phone', 'length', 'max'=>15),
            array('contestant_id, contestant_photo, contestant_id_f, contestant_id_b, contestant_name, contestant_address, contestant_email, contestant_phone, contestant_school_name, contestant_school_address, contestant_school_phone', 'safe'),
            //type validation
            array('contestant_email','email'),
            array('contestant_phone, contestant_school_phone','numerical'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('contestant_id, contestant_photo, contestant_id_f, contestant_id_b, contestant_name, contestant_address, contestant_email, contestant_phone, contestant_school_name, contestant_school_address, contestant_school_phone', 'safe', 'on'=>'search'),
            //array('contestant_photo, contestant_id_f, contestant_id_b', 'application.components.imageValidation'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contestant' => array(self::BELONGS_TO, 'Contestant', 'contestant_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'contestant_name' => 'Nama',
            'contestant_address' => 'Alamat',
            'contestant_email' => 'Email',
            'contestant_phone' => 'Nomor Kontak',
            'contestant_photo' => 'Pasfoto Peserta',
            'contestant_id_f' => 'Foto Kartu Identitas Peserta (depan)',
            'contestant_id_b' => 'Foto Kartu Identitas Peserta (belakang)',
            'contestant_school_name' => 'Nama Sekolah',
            'contestant_school_address' => 'Alamat Sekolah',
            'contestant_school_phone' => 'Nomor Kontak Sekolah',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('contestant_id',$this->contestant_id);
        $criteria->compare('contestant_name',$this->contestant_name,true);
        $criteria->compare('contestant_phone',$this->contestant_phone,true);
        $criteria->compare('contestant_address',$this->contestant_address,true);
        $criteria->compare('contestant_email',$this->contestant_email,true);
        $criteria->compare('contestant_school_name',$this->contestant_school_name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * fill the default value of Paper
     */
    public function fill()
    {
        $this->contestant_name           = '';
        $this->contestant_address        = '';
        $this->contestant_email          = '';
        $this->contestant_phone          = '';
        $this->contestant_photo          = '';
        $this->contestant_id_f           = '';
        $this->contestant_id_b           = '';
        $this->contestant_school_name    = '';
        $this->contestant_school_address = '';
        $this->contestant_school_phone   = '';
    }
}