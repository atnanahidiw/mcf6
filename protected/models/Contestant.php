<?php

/**
 * This is the model class for table "contestant".
 *
 * The followings are the available columns in table 'contestant':
 * @property integer $contestant_id
 * @property string $contestant_username
 * @property string $contestant_password
 * @property integer $contestant_type
 * @property integer $contestant_status
 * @property integer $contestant_expired_registration_date
 *
 * The followings are the available model relations:
 * @property Paper $paper
 * @property School $school
 * @property University $university
 */
class Contestant extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Contestant the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contestant';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contestant_type, contestant_status', 'numerical', 'integerOnly'=>true),
            array('contestant_username', 'length', 'max'=>10),
            array('contestant_password', 'length', 'max'=>100),
            array('contestant_id, contestant_username, contestant_password, contestant_type, contestant_status, contestant_expired_registration_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('contestant_id, contestant_username, contestant_password, contestant_type, contestant_status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'paper' => array(self::HAS_ONE, 'Paper', 'contestant_id'),
            'school' => array(self::HAS_ONE, 'School', 'contestant_id'),
            'university' => array(self::HAS_ONE, 'University', 'contestant_id'),
            'payment' => array(self::HAS_ONE, 'Payment', 'contestant_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'contestant_id' => 'Contestant',
            'contestant_username' => 'Username',
            'contestant_password' => 'Password',
            'contestant_type' => 'Contestant Type',
            'contestant_status' => 'Contestant Status',
            'contestant_expired_registration_date' => 'Contestant Expired Registration Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('contestant_id',$this->contestant_id);
        $criteria->compare('contestant_username',$this->contestant_username,true);
        $criteria->compare('contestant_password',$this->contestant_password,true);
        $criteria->compare('contestant_type',$this->contestant_type);
        $criteria->compare('contestant_status',$this->contestant_status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Custom action done before save. Here, we'll generate credentials and ecrypt it.
     * @return boolean true if nothing happen
     */
    public function beforeSave() {
        //$this->generateCredentials();

        // encrypt password when its the first time the user is editing
        //if ($this->getScenario() == 'register') {
        //    $this->password = $this->encrypt($this->password);
        //}

        return(true);
    }

    /*
     * Send an email to users
     * Uses PHPMailer class
     */
    public function sendEmail($subject, $recipient_name, $recipient_address, $body, $attachment = null) {
        
        Yii::import('application.vendors.phpmailer.*');
        require_once('class.phpmailer.php');
        //include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
        
        $mail             = new PHPMailer();
        
        $mail->IsSMTP();                                          // telling the class to use SMTP
        //$mail->SMTPDebug  = 2;                                  // enables SMTP debug information (for testing)
                                                                  // 1 = errors and messages
                                                                  // 2 = messages only
        $mail->SMTPAuth   = true;                                 // enable SMTP authentication
        $mail->SMTPSecure = "tls";                                // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";                     // sets GMAIL as the SMTP server
        $mail->Port       = 587;                                  // set the SMTP port for the GMAIL server
        $mail->Username   = Yii::app()->params['adminEmail'];      // GMAIL username
        $mail->Password   = Yii::app()->params['adminEmailPass'];  // GMAIL password
        
        $mail->SetFrom(Yii::app()->params['adminEmail'], Yii::app()->params['adminEmailName']);
        
        $mail->AddReplyTo(Yii::app()->params['adminEmail'], Yii::app()->params['adminEmailName']);
        
        $mail->Subject    = $subject;
        
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        
        $mail->MsgHTML($body);

        if (!empty($attachment)) {
            $mail->addAttachment($attachment['path'], $attachment['name']);
        }
        
        $mail->AddAddress($recipient_address, $recipient_name);
        
        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        }/* else {
          echo "Message sent!";
        }
*/    }

    /**
     * checker untuk pendaftaran universitas
     * @author Abduurahman Shofy Adianto <azophy@gmail.com>
     */
    public static function checkUniversityQuota()
    {
        if (Contestant::model()->countByAttributes(array('contestant_type' => '2', 'contestant_status' => '2')) >= Yii::app()->params['universityMaxContestantNumber']) {
            //otomatod disable pendaftaran universitas
            $config_universitas = Configuration::model()->findByAttributes(array('name' => 'universityStarted'));
            $config_universitas->value = 'false';
            $config_universitas->save();
        }
    }

    /**
     * fill the default value of Contestant
     */
    public function fill()
    {
        $this->contestant_username                  = '';
        $this->contestant_password                  = '';
        $this->contestant_type                      = -1;
        $this->contestant_expired_registration_date = '0000-00-00';
    }
}
