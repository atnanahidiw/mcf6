<?php

class FeedbackController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionMakian()
    {
        if($_POST != null){
            Yii::import('application.vendors.phpmailer.*');
            require_once('class.phpmailer.php');
            //include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

            $mail             = new PHPMailer();

            $mail->IsSMTP();                                          // telling the class to use SMTP
            //$mail->SMTPDebug  = 2;                                  // enables SMTP debug information (for testing)
                                                                      // 1 = errors and messages
                                                                      // 2 = messages only
            $mail->SMTPAuth   = true;                                 // enable SMTP authentication
            $mail->SMTPSecure = "tls";                                // sets the prefix to the servier
            $mail->Host       = "smtp.gmail.com";                     // sets GMAIL as the SMTP server
            $mail->Port       = 587;                                  // set the SMTP port for the GMAIL server
            $mail->Username   = Yii::app()->params['adminEmail'];      // GMAIL username
            $mail->Password   = Yii::app()->params['adminEmailPass'];  // GMAIL password

            $mail->SetFrom($_POST['email'], $_POST['name']);

            $mail->AddReplyTo($_POST['email'], $_POST['name']);

            $mail->Subject    = "Webnya Cupu Sekali";

            $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

            $mail->MsgHTML('Kawan '.$_POST['name'].' ('.$_POST['email'].') ga suka ama webnya, dia bilang:<br>'.$_POST['saran']);

            $mail->AddAddress(Yii::app()->params['adminEmail'], 'Tim Web yang ganteng-ganteng');

            if(!$mail->Send()) {
              echo "Mailer Error: " . $mail->ErrorInfo;
            }

            Yii::app()->user->setFlash('success', '<i>Feedback</i> Anda sudah kami terima.<br> Terima kasih atas partisipasinya ^^<br><br><br><br>Kecup Sayang,<br>Tim Web MCF-MMC ITB 2014');
        }
	$this->redirect(Yii::app()->baseUrl.'/feedback');
    }
}
