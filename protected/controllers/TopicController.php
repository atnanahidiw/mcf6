<?php

class TopicController extends Controller
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('choose', 'index'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('edit', 'choose', 'delete', 'index'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Edit the topic
     */
    public function actionEdit()
    {
        $model = Topic::model()->findByPk($_GET['id']);
        if(isset($_POST['Topic']))
        {
            $model->attributes = $_POST['Topic'];
            if($model->validate()) $model->update();
        }
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('home/admin'));
    }

    /**
     * Choose the topic
     */
    public function actionChoose()
    {
        $model = new TopicChoice();
        if(isset($_POST['choice']))
        {
            $choosen = TopicChoice::model()->countByAttributes(array('topic_id' => $_POST['choice']));
            /*$quota = Topic::model()->find(array(
                'condition'=>'topic_id=:id',
                'params'=>array(':id'=>$_POST['choice']),
            ))->topic_quota;*/
            $quota = $choosen + 1;
            if($quota > $choosen){
                $model->topic_id = $_POST['choice'];
                if(isset($_POST['username'])){
                    $temp = Contestant::model()->find(array(
                        'condition'=>'contestant_username=:username',
                        'params'=>array(':username'=>$_POST['username']),
                    ));
                    if($temp != null) $model->contestant_id = $temp->contestant_id;
                    else $this->redirect(array('home/admin'));
                }
                else $model->contestant_id = $_POST['id'];
                $exist_model = TopicChoice::model()->find(array(
                    'condition'=>'contestant_id=:contestant_id',
                    'params'=>array(':contestant_id'=>$model->contestant_id,),
                ));
                if($exist_model!=null) $this->redirect(array('home/admin?choosen=true'));
                if(($exist_model==null) && $model->validate()) $model->save();
            }
        }
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('home/admin'));
    }

    /**
     * Delete TopicChoice
     */
    public function actionDelete()
    {
        TopicChoice::model()->deleteAll( 'contestant_id=:id', array(':id'=>$_GET['id']) );
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('home/admin'));
    }

    /*    public function actionDummy()
    {
        for($i=1;$i<101;++$i){
            $model = new Contestant();
            $model->contestant_id = $i;
            $ID = $i;
            while(strlen($ID)<3) $ID='0'.$ID;
            $model->contestant_username = 'tes'.$ID;
            $user = new UserIdentity('admin', '');
            $model->contestant_password = $user->encrypt('tes');
            $model->contestant_type = -1;
            $model->contestant_status = 2;
            $model->save();
        }
        echo 'Success';
    }
*/
}