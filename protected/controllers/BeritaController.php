<?php

class BeritaController extends Controller
{
    /**
     * Index
     */
    public function actionIndex()
    {
        $university = University::model()->findAll(array(
            'with' => array('contestant'),
            'condition' => 'contestant.contestant_status = 2 OR contestant.contestant_status = 1',
			'select' => 'contestant_team_name, contestant_university_name',
            'order' => 'contestant_university_name, contestant.contestant_id',
		));
		$this->render('index', array(
			'university_contestant' => $university,
		));
    }

}
