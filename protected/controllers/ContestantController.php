<?php

class ContestantController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','certificate','card','sendcertificate'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Contestant;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Contestant']))
        {
            $model->attributes=$_POST['Contestant'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->contestant_id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Contestant']))
        {
            $model->attributes=$_POST['Contestant'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->contestant_id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Contestant');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Contestant('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Contestant']))
            $model->attributes=$_GET['Contestant'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contestant the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Contestant::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Contestant $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='contestant-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionCertificate()
    {
        $pdf = new PdfGenerator(PdfGenerator::NEW_CERTIFICATE);
        $finalis = array("MCF027U", "MCF036U", "MCF043U", "MCF087U", "MCF040U", "MCF083U");
        $honorable = array("MCF017U", "MCF019U", "MCF029U", "MCF038U", "MCF057U", "MCF086U", "MCF088U", "MCF014U", "MCF015U", "MCF016U",
                           "MCF020U", "MCF032U", "MCF053U", "MCF090U", "MCF021U", "MCF044U");

        if (isset($_GET['type']) && isset($_GET['id']) && Yii::app()->user->getName()=='admin') { error_log(Yii::app()->user->getName());
            $type   = $_GET['type'];
            $id     = $_GET['id'];
        } else {
            $type   = Yii::app()->user->type;
            $id     = Yii::app()->user->id;
        }
        
        $contestant = Contestant::model()->findByAttributes(array('contestant_id'=>$id));
        //check contestant type
        switch ($type) {
            case 1:
                //school
                $model = School::model()->findByAttributes(array('contestant_id'=>$id));

                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $participation = "Semifinalis";
                    $education_name = $model->contestant_school_name;

                    //user 1
                    if (!empty($model->contestant_first_name)) {
                        $pdf->addCertificate($model->contestant_first_name, $education_name, $type, $participation);
                    }
    
                    //user 2
                    if (!empty($model->contestant_second_name)) {
                        $pdf->addCertificate($model->contestant_second_name, $education_name, $type, $participation);
                    }
    
                    //user 3
                    if (!empty($model->contestant_third_name)) {
                        $pdf->addCertificate($model->contestant_third_name, $education_name, $type, $participation);
                    }

                    //supervisor
                    if (!empty($model->contestant_supervisor_name)) {
                        $pdf->addCertificate($model->contestant_supervisor_name, $education_name, $type, "Supervisor");
                    }
                }
            break;
            case 2:
                //university
                $model = University::model()->findByAttributes(array('contestant_id'=>$id));

                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $participation = "Peserta";
                    if(in_array($contestant->contestant_username, $finalis)) $participation = "Finalis";
                    else if(in_array($contestant->contestant_username, $honorable)) $participation = "Honorable Mention";
                    $education_name = $model->contestant_university_name;

                    //user 1
                    if (!empty($model->contestant_first_name)) {
                        $pdf->addCertificate($model->contestant_first_name, $education_name, $type, $participation);
                    }
    
                    //user 2
                    if (!empty($model->contestant_second_name)) {
                        $pdf->addCertificate($model->contestant_second_name, $education_name, $type, $participation);
                    }
    
                    //user 3
                    if (!empty($model->contestant_third_name)) {
                        $pdf->addCertificate($model->contestant_third_name, $education_name, $type, $participation);
                    }

                    //supervisor
                    if (!empty($model->contestant_supervisor_name)) {
                        $pdf->addCertificate($model->contestant_supervisor_name, $education_name, $type, "Supervisor");
                    }
                }
            break;
            case 3:
                //paper
                $model = Paper::model()->findByAttributes(array('contestant_id'=>$id));
                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $pdf->addCertificate($model->contestant_name, $model->contestant_school_name, $type);
                }
            break;
        }

        $pdf->Output($contestant->contestant_username.'.pdf', 'D');
    }
    
    public function actionCard()
    {
        $pdf = new PdfGenerator(PdfGenerator::TYPE_PARTICIPANT_CARD);
        
        //check contestant type
        switch (Yii::app()->user->type) {
            case 2:
                //university
                $model = University::model()->findByAttributes(array('contestant_id'=>Yii::app()->user->id));

                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $education_name = $model->contestant_university_name;

                    //user 1
                    if (!empty($model->contestant_first_name)) {
                        $pdf->addCard($model->contestant_first_name, Yii::app()->user->id, $education_name);
                    }
    
                    //user 2
                    if (!empty($model->contestant_second_name)) {
                        $pdf->addCard($model->contestant_second_name, Yii::app()->user->id, $education_name);
                    }
    
                    //user 3
                    if (!empty($model->contestant_third_name)) {
                        $pdf->addCard($model->contestant_third_name, Yii::app()->user->id, $education_name);
                    }
                }
            break;
        }

        $pdf->Output();
    }
    
    public function actionSendcertificate()
    {
        if (empty($_GET['user_id'])) {
            throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak developer website MCF-MMC');
        }

        $contestant_id = $_GET['user_id'];
        $contestant = Contestant::model()->findByAttributes(array('contestant_id'=>$contestant_id));

        if (empty($contestant)) {
            throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak developer website MCF-MMC');
        }

        $model = null;

        //generate certificate
        $pdf = new PdfGenerator(PdfGenerator::TYPE_CERTIFICATE);
            
        //check contestant type
        switch ($contestant->contestant_type) {
            case 1:
                //school
                $model = School::model()->findByAttributes(array('contestant_id'=>$contestant_id));

                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $education_name = $model->contestant_school_name;

                    //user 1
                    if (!empty($model->contestant_first_name)) {
                        $pdf->addCertificate($model->contestant_first_name, $education_name, Yii::app()->user->type);
                    }
    
                    //user 2
                    if (!empty($model->contestant_second_name)) {
                        $pdf->addCertificate($model->contestant_second_name, $education_name, Yii::app()->user->type);
                    }
    
                    //user 3
                    if (!empty($model->contestant_third_name)) {
                        $pdf->addCertificate($model->contestant_third_name, $education_name, Yii::app()->user->type);
                    }
                }
            break;
            case 2:
                //university
                $model = University::model()->findByAttributes(array('contestant_id'=>$contestant_id));

                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $education_name = $model->contestant_university_name;

                    //user 1
                    if (!empty($model->contestant_first_name)) {
                        $pdf->addCertificate($model->contestant_first_name, $education_name, Yii::app()->user->type);
                    }
    
                    //user 2
                    if (!empty($model->contestant_second_name)) {
                        $pdf->addCertificate($model->contestant_second_name, $education_name, Yii::app()->user->type);
                    }
    
                    //user 3
                    if (!empty($model->contestant_third_name)) {
                        $pdf->addCertificate($model->contestant_third_name, $education_name, Yii::app()->user->type);
                    }
                }
            break;
            case 3:
                //paper
                $model = Paper::model()->findByAttributes(array('contestant_id'=>$contestant_id));
                if (empty($model)) {
                    throw new CHttpException(404,'Sertifikat yang diminta belum tersedia. Silahkan hubungi pihak panitia MCF-MMC');
                } else {
                    $pdf->addCertificate($model->contestant_third_name, $model->contestant_school_name, Yii::app()->user->type);
                }
            break;
        }
            
        //generate random name
        $filename = (rand()%1000)."-".date("d-m-Y").".pdf"; //error_log("filename: ".$filename);

        $pdf->Output($filename, 'F');
        //$pdf->Output();
        
        //send certificate
        $body = "Your certificate of MCF6 has been attached to this email";
        $subject    = "Mathematical Challenge Festival VI Participant's Certificate";
        $contestant_email = $model->contestant_email;

        $contestant->sendEmail($subject, "MCF VI Contestant", $contestant_email, $body, array('path' => $filename, 'name' => 'certificate.pdf'));
    
        //delete temporary file
        unlink($filename);
    }
}
