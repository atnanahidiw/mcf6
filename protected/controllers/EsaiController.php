<?php

class EsaiController extends UniversalContestantController
{
    /**
     * Creates a new model.
     */
    public function actionPendaftaran()
    {
        parent::allRegistration('paper');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        parent::allDelete($this->loadModel($id));
    }

    /**
     * Show Paper Contestant Profile from Admin
     */
    public function actionView($id)
    {
        $this->layout = '//layouts/main3';
        parent::afterProfile('paper', $this->loadModel($id));
    }

    /**
     * Show Paper Contest Description
     */
    public function actionIndex()
    {
        if((Yii::app()->user->isGuest) || (Yii::app()->user->type != 3)) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterIndex('paper', $model);
    }

    /**
     * Show Paper Contestant Profile
     */
    public function actionProfil()
    {
        if(Yii::app()->user->isGuest) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterProfile('paper', $model);
    }

    /**
     * Show Paper Contestant Payment upload page
     */
    public function actionPayment()
    {
        parent::allPayment('paper');
    }

    /**
     * Show Paper Contestant Problem upload page
     */
    public function actionMasalah()
    {
        parent::allProblem('paper');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        parent::allAdmin('paper');
    }

    /**
     * Verify Paper Contestant Payment
     */
    public function actionPaid($id)
    {
        parent::allVerify($id, 0);
    }

    /**
     * Retry Verify University Modeling Contestant Payment
     */
    public function actionRepaid($id)
    {
        if(isset($_GET['mirzaganteng']) && ($_GET['mirzaganteng'] == 'true')) parent::retryVerify($id, 0);
        else echo '<div>Katakan Mirza Ganteng 3x, lalu klik OK!</div><div><button>OK</button>';
    }

    /**
     * Verify Paper Contestant Complete Data
     */
    public function actionComplete($id)
    {
        parent::allVerify($id, 1);
    }

    /**
     * Show University Modeling Contestant Problem upload page
     */
    public function actionPengumpulan()
    {
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==3))
        {
            $model = Problem::model()->findByAttributes(array('contestant_id' => Yii::app()->user->id));
            if($model == null){
                $model                = new Problem();
                $model->contestant_id = Yii::app()->user->id;
            }

            if(!empty($_FILES) && (
            ($_FILES['file']['type'] == 'application/pdf') ||
            ($_FILES['file']['type'] == 'application/x-pdf') ||
            ($_FILES['file']['type'] == 'application/msword') ||
            ($_FILES['file']['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            )){
                $filename = $_FILES['file']['name'];
                if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$filename);
                if((strlen($filename)!=0) && (file_exists('backup/'.$filename))) unlink('backup/'.$filename);
                copy('upload/'.$filename, 'backup/'.$filename);
                $model->file_name = $filename;
                if($model->save()) Yii::app()->user->setFlash('success', 'Berkas berhasil diunggah');
            }

            $this->render('submit', array(
                'model'  => $model,
            ));
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return paper the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Paper::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param paper $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='paper-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Activate registration
     */
    public function actionStart()
    {
        parent::regButton('paper', 'start');
    }

    /**
     * Disable registration
     */
    public function actionStop()
    {
        parent::regButton('paper', 'stop');
    }
}
