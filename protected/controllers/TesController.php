<?php

class TesController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/main4', meaning
     * using two-column layout. See 'protected/views/layouts/main4.php'.
     */
    public $layout='//layouts/main4';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('topik', 'penyisihan', 'index'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Index
     */
    public function actionIndex()
    {
        if(Yii::app()->user->id > 50) $this->render('preliminary1');
        else $this->render('index');
    }

    public function actionPenyisihan()
    {
        if($_POST != null) {
            $this->render('preliminary2', array(
                'start' => 11,
            ));
        }
        else {
            $this->render('preliminary2', array(
                'start' => 1,
            ));
        }
    }

    public function actionTopik()
    {
        $choice = TopicChoice::model()->find(array(
            'condition'=>'contestant_id=:id',
            'params'=>array(':id'=>Yii::app()->user->id),
        ));
        $criteria = array(
            'condition' => 'topic_id > 3 AND topic_id < 6',
        );
        $topics = Topic::model()->findAll($criteria);

        $this->render('topic', array(
            'topics'=>$topics,
            'choice'=>$choice,
        ));
    }

    /**
     * Upload Motivation Letter
     */
    public function actionMotivation_letter()
    {
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==-1))
        {
            if(!empty($_FILES)){
                Yii::app()->user->setFlash('warning', 'Laman ini hanya untuk uji coba. Berkas yang Anda unggah tidak akan disimpan');
            }
            $this->render('letter');
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }

    /**
     * Show University Modeling Contestant Problem upload page
     */
    public function actionMasalah()
    {
        $model = Topic::model()->findAll();
        $this->render('problem', array(
            'model'=>$model,
        ));
    }

    public function actionPengumpulan()
    {
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==-1))
        {
            $topic_choice = TopicChoice::model()->findByAttributes(array('contestant_id'=>Yii::app()->user->id));
            if($topic_choice == null) $topic = new Topic();
            else $topic = Topic::model()->findByPk($topic_choice->topic_id);

            //$filename = Contestant::model()->findByPk(Yii::app()->user->id)->contestant_username.'_'.$topic_choice->topic_id.'_Laporan.pdf';
            $filename = "";
            $model = Problem::model()->findByAttributes(array('contestant_id' => Yii::app()->user->id));
            if($model == null){
                $model                = new Problem();
                $model->contestant_id = Yii::app()->user->id;
            }

            if(!empty($_FILES)){
                if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                else $model->file_name = $filename;
                move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$filename);
                if((strlen($filename)!=0) && (file_exists('backup/'.$filename))) unlink('backup/'.$filename);
                copy('upload/'.$filename, 'backup/'.$filename);
                if($model->save()) Yii::app()->user->setFlash('success', 'Berkas berhasil diunggah dengan nama '.$filename);
            }

            $this->render('submit', array(
                'problem' => $topic,
                'model'  => $model,
            ));
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }
}