<?php

class HomeController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('login', 'logout', 'index', 'sync', 'error'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('index', 'error'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin', 'index', 'error'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
//    public function actionIndex()
//    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
//        $this->render('index');
//    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model));
    }

    /**
     * Displays the login page
     */
    public function actionIndex()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                switch(Yii::app()->user->type)
                {
                    case -1 : $this->redirect(array("/tes"));                  break;
                    case 0  : $this->redirect(array("home/admin"));            break;
                    case 1  : $this->redirect(array("/pemodelan_siswa"));      break;
                    case 2  : $this->redirect(array("/pemodelan_mahasiswa"));  break;
                    case 3  : $this->redirect(array("/esai"));                 break;
                    case 4  : $this->redirect(array("/seminar_nasional"));     break;
                    default : $this->redirect(array("/"));
                }
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    /**
     * Show admin homepage.
     */
    public function actionAdmin()
    {
        $this->layout = 'main3';

        $criteria = array(
            'condition' => 'topic_id > 3 AND topic_id < 6',
        );
        $topic_school = Topic::model()->findAll($criteria);

        $topic_university = array();
        $criteria = array(
            'condition' => 'topic_id < 4',
        );
        $topics = Topic::model()->findAll($criteria);
        foreach($topics as $topic)
            array_push($topic_university, array(
                'topic' => $topic,
                'amount' => TopicChoice::model()->countByAttributes(array('topic_id' => $topic->topic_id)),
            ));

        $choosens_school = array();
        $criteria = array(
            'condition' => 'topic_id > 3 AND topic_id < 6',
            'order'     => 'contestant_id',
        );
        $topic_choice = TopicChoice::model()->findAll($criteria);
        foreach($topic_choice as $tc){
            $contestant = University::model()->findByPk($tc->contestant_id);
            $contestant = ($contestant == null) ? $contestant = Contestant::model()->findByPk($tc->contestant_id)->contestant_username : 'Tim ' . $contestant->contestant_team_name;
            array_push($choosens_school, array(
                'id' => $tc->contestant_id,
                'contestant' => $contestant,
                'topic' => Topic::model()->findByPk($tc->topic_id)->topic_title,
            ));
        }

        $choosens_university = array();
        $criteria = array(
            'condition' => 'topic_id < 4',
            'order'     => 'contestant_id',
        );
        $topic_choice = TopicChoice::model()->findAll($criteria);
        foreach($topic_choice as $tc){
            $contestant = University::model()->findByPk($tc->contestant_id);
            $contestant = ($contestant == null) ? $contestant = Contestant::model()->findByPk($tc->contestant_id)->contestant_username : 'Tim ' . $contestant->contestant_team_name;
            array_push($choosens_university, array(
                'id' => $tc->contestant_id,
                'contestant' => $contestant,
                'topic' => Topic::model()->findByPk($tc->topic_id)->topic_title,
            ));
        }

        $this->render('admin',array(
            'topic_school'        => $topic_school,
            'topic_university'    => $topic_university,
            'choosens_school'     => $choosens_school,
            'choosens_university' => $choosens_university,
        ));
    }

    /**
     * Generate School Contestant data for sync
     */
    public function actionSync()
    {
        $criteria = array(
            'condition' => 'contestant_type = 1 AND contestant_status = 2',
        );
        $model = Contestant::model()->findAll($criteria);
        $this->renderPartial('sync',array(
            'model'=>$model
        ));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}