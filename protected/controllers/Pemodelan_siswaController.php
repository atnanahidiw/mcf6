<?php

class Pemodelan_siswaController extends UniversalContestantController
{
    /**
     * Creates a new model.
     */
    public function actionPendaftaran()
    {
        parent::allRegistration('school');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        parent::allDelete($this->loadModel($id));
    }

    /**
     * Show School Modeling Contestant Profile from Admin
     */
    public function actionView($id)
    {
        $this->layout = '//layouts/main3';
        parent::afterProfile('school', $this->loadModel($id));
    }

    /**
     * Show School Modeling Contest Description
     */
    public function actionIndex()
    {
        if((Yii::app()->user->isGuest) || (Yii::app()->user->type != 1)) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterIndex('school', $model);
    }

    /**
     * Show School Modeling Contestant Profile
     */
    public function actionProfil()
    {
        if(Yii::app()->user->isGuest) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterProfile('school', $model);
    }

    /**
     * Show School Modeling Contestant Payment upload page
     */
    public function actionPayment()
    {
        parent::allPayment('school');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        parent::allAdmin('school');
    }

    /**
     * Verify School Modeling Contestant Payment
     */
    public function actionPaid($id)
    {
        parent::allVerify($id, 0);
    }

    /**
     * Retry Verify University Modeling Contestant Payment
     */
    public function actionRepaid($id)
    {
        if(isset($_GET['mirzaganteng']) && ($_GET['mirzaganteng'] == 'true')) parent::retryVerify($id, 0);
        else echo '<div>Katakan Mirza Ganteng 3x, lalu klik OK!</div><div><button>OK</button>';
    }

    /**
     * Verify School Modeling Contestant Complete Data
     */
    public function actionComplete($id)
    {
        parent::allVerify($id, 1);
    }

    /**
     * Choose Topic
     */
    public function actionTopik()
    {
        $criteria = array(
            'condition' => 'topic_id > 3 AND topic_id < 6',
        );
        $model = Topic::model()->findAll($criteria);
        $choice = TopicChoice::model()->find(array(
            'condition'=>'contestant_id=:id',
            'params'=>array(':id'=>Yii::app()->user->id),
        ));
        $this->render('topic',array(
            'model'=>$model,
            'choice'=>$choice,
        ));
    }

    /**
     * Show University Modeling Contestant Problem description page
     */
    public function actionMasalah()
    {
        $criteria = array(
            'condition' => 'topic_id > 3 AND topic_id < 6',
        );
        $model = Topic::model()->findAll($criteria);
        $this->render('problem', array(
            'model'=>$model,
        ));
    }

    /**
     * Show University Modeling Contestant Problem upload page
     */
    public function actionPengumpulan()
    {
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==1))
        {
            $topic_choice = TopicChoice::model()->findByAttributes(array('contestant_id'=>Yii::app()->user->id));
            if($topic_choice == null) $topic = new Topic();
            else $topic = Topic::model()->findByPk($topic_choice->topic_id);

            $model = Problem::model()->findByAttributes(array('contestant_id' => Yii::app()->user->id));
            if($model == null){
                $model                = new Problem();
                $model->contestant_id = Yii::app()->user->id;
            }

            if(!empty($_FILES) && (
            ($_FILES['file']['type'] == 'application/pdf') ||
            ($_FILES['file']['type'] == 'application/x-pdf')
            )){
                if(time() >= strtotime('19 January 2014 00:00:01 GMT+7')){
                    Yii::app()->user->setFlash('error', 'Maaf batas waktu sudah terlewati, berkas Anda tidak kami terima');
                }
                else{
                    $filename = $_FILES['file']['name'];
                    if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                    move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$filename);
                    if((strlen($filename)!=0) && (file_exists('backup/'.$filename))) unlink('backup/'.$filename);
                    copy('upload/'.$filename, 'backup/'.$filename);
                    $model->file_name = $filename;
                    if($model->save()) Yii::app()->user->setFlash('success', 'Berkas berhasil diunggah');
                }
            }

            $this->render('submit', array(
                'problem' => $topic,
                'model'  => $model,
            ));
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return School the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=School::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param School $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='school-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Activate registration
     */
    public function actionStart()
    {
        parent::regButton('school', 'start');
    }

    /**
     * Disable registration
     */
    public function actionStop()
    {
        parent::regButton('school', 'stop');
    }
}
