<?php

class Pemodelan_mahasiswaController extends UniversalContestantController
{
    /**
     * Creates a new model.
     */
    public function actionPendaftaran()
    {
        Contestant::checkUniversityQuota();
        parent::allRegistration('university');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        parent::allDelete($this->loadModel($id));
    }

    /**
     * Show University Modeling Contestant Profile from Admin
     */
    public function actionView($id)
    {
        $this->layout = '//layouts/main3';
        parent::afterProfile('university', $this->loadModel($id));
    }

    /**
     * Show University Modeling Contest Description
     */
    public function actionIndex()
    {
        Contestant::checkUniversityQuota();
        
        if((Yii::app()->user->isGuest) || (Yii::app()->user->type != 2)) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterIndex('university', $model);
    }

    /**
     * Show University Modeling Contestant Profile
     */
    public function actionProfil()
    {
        if(Yii::app()->user->isGuest) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterProfile('university', $model);
    }

    /**
     * Show University Modeling Contestant Payment upload page
     */
    public function actionPayment()
    {
        parent::allPayment('university');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        parent::allAdmin('university');
    }

    /**
     * Verify University Modeling Contestant Payment
     */
    public function actionPaid($id)
    {
        parent::allVerify($id, 0);
    }

    /**
     * Retry Verify University Modeling Contestant Payment
     */
    public function actionRepaid($id)
    {
        if(isset($_GET['mirzaganteng']) && ($_GET['mirzaganteng'] == 'true')) parent::retryVerify($id, 0);
        else echo '<div>Katakan Mirza Ganteng 3x, lalu klik OK!</div><div><button>OK</button>';
    }

    /**
     * Verify University Modeling Contestant Complete Data
     */
    public function actionComplete($id)
    {
        parent::allVerify($id, 1);
    }

    /**
     * Upload Motivation Letter
     */
    public function actionMotivation_letter()
    {
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==2))
        {
            $filename = Contestant::model()->findByPk(Yii::app()->user->id)->contestant_username.'_Letter.pdf';
            $model = $this->loadModel(Yii::app()->user->id);
            
            if(!empty($_FILES)){
                if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                else $model->contestant_motivation_letter = $filename;
                move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$filename);
                if($model->save()) Yii::app()->user->setFlash('success', 'Berkas berhasil diunggah dengan nama '.$filename);
                
            }

            $this->render('letter',array(
            'model'=>$model,
        ));
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }

    /**
     * Choose Topic
     */
    public function actionTopik()
    {
        $model = array();
        $choice = TopicChoice::model()->find(array(
            'condition'=>'contestant_id=:id',
            'params'=>array(':id'=>Yii::app()->user->id),
        ));
        $topics = Topic::model()->findAll();
        foreach($topics as $topic)
            array_push($model, array(
                'topic' => $topic,
                'amount' => TopicChoice::model()->countByAttributes(array('topic_id' => $topic->topic_id)),
            ));

        $this->render('topic',array(
            'model'=>$model,
            'choice'=>$choice,
        ));
    }

    /**
     * Show University Modeling Contestant Problem description page
     */
    public function actionMasalah()
    {
        $model = Topic::model()->findAll();
        $this->render('problem', array(
            'model'=>$model,
        ));
    }

    /**
     * Show University Modeling Contestant Problem upload page
     */
    public function actionPengumpulan()
    {
        throw new CHttpException(404,'The requested page does not exist.');
        if( (!Yii::app()->user->isGuest) && (Yii::app()->user->type==2))
        {
            $topic_choice = TopicChoice::model()->findByAttributes(array('contestant_id'=>Yii::app()->user->id));
            if($topic_choice == null) $topic = new Topic();
            else $topic = Topic::model()->findByPk($topic_choice->topic_id);

            $filename = Contestant::model()->findByPk(Yii::app()->user->id)->contestant_username.'_'.$topic_choice->topic_id.'_Laporan.pdf';
            $model = Problem::model()->findByAttributes(array('contestant_id' => Yii::app()->user->id));
            if($model == null){
                $model                = new Problem();
                $model->contestant_id = Yii::app()->user->id;
            }
            
            if(!empty($_FILES)){
                if((strlen($filename)!=0) && (file_exists('upload/'.$filename))) unlink('upload/'.$filename);
                else $model->file_name = $filename;
                move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$filename);
                if((strlen($filename)!=0) && (file_exists('backup/'.$filename))) unlink('backup/'.$filename);
                copy('upload/'.$filename, 'backup/'.$filename);
                if($model->save()) Yii::app()->user->setFlash('success', 'Berkas berhasil diunggah dengan nama '.$filename);
            }

            $this->render('submit', array(
                'problem' => $topic,
                'model'  => $model,
            ));
        }
        else throw new CHttpException(404,'The requested page does not exist.');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return University the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=University::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param University $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='university-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Activate registration
     */
    public function actionStart()
    {
        parent::regButton('university', 'start');
    }

    /**
     * Disable registration
     */
    public function actionStop()
    {
        parent::regButton('university', 'stop');
    }
}
