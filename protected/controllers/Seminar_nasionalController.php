<?php

class Seminar_nasionalController extends UniversalContestantController
{
    /**
     * Creates a new model.
     */
    public function actionPendaftaran()
    {
        parent::allRegistration('seminar');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        parent::allDelete($this->loadModel($id));
    }

    /**
     * Show Seminar Nasional Participant Profile from Admin
     */
    public function actionView($id)
    {
        $this->layout = '//layouts/main3';

        $this->render('profile',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Show Seminar Nasional Description
     */
    public function actionIndex()
    {
        if((Yii::app()->user->isGuest) || (Yii::app()->user->type != 4)) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterIndex('seminar', $model);
    }

    /**
     * Show Seminar Nasional Participant Profile
     */
    public function actionProfil()
    {
        if(Yii::app()->user->isGuest) $model = null;
        else $model = $this->loadModel(Yii::app()->user->id);
        parent::afterProfile('seminar', $model);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        parent::allAdmin('seminar');
    }

    /**
     * Verify Seminar Nasional Contestant Payment
     */
    public function actionPaid($id)
    {
        parent::allVerify($id, 0);
    }

    /**
     * Verify Seminar Nasional Contestant Complete Data
     */
    public function actionComplete($id)
    {
        parent::allVerify($id, 1);
    }

   /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return seminar_nasional the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Seminar::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param seminar_nasional $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='seminar_nasional-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Activate registration
     */
    public function actionStart()
    {
        parent::regButton('seminar', 'start');
    }

    /**
     * Disable registration
     */
    public function actionStop()
    {
        parent::regButton('seminar', 'stop');
    }
}