<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// Define a path alias for the Bootstrap extension as it's used internally.
// In this example we assume that you unzipped the extension under protected/extensions.

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'MCF - MMC ITB 2014',
    'defaultController' => 'home',
    
    // preloading 'log' component
    'preload'=>array('log'),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
    ),

    //'theme'=>'bootstrap', // requires you to copy the theme under your themes directory
    
    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'mirzaganteng',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
    ),

    // application components
    'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),
        // uncomment the following to enable URLs in path-format
        
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
        
        'db'=>array(
            'connectionString' => 'mysql:host=127.0.0.1;dbname=mcf',
            'emulatePrepare' => true,
            'username' => 'mcf',
            'password' => 'enam',
            'charset' => 'utf8',
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'home/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'mcf.mmc.itb@gmail.com',
        'adminEmailName'=>'Panitia MCF-MMC ITB 2014',
        'adminEmailPass'=>'indonesiabermatematika',
        'schoolEndRegistration' => '01/05/2014',
        'universityMaxContestantNumber' => 60,
        'universityEndRegistration' => '12/04/2013',
        'school' => array(
            'registration' => array(
                'begin' => '11 Desember 2013',
                'end' => '10 Januari 2014',
            ),
            'preliminary' => '12 Januari 2014',
            'semifinal' => array(
                'begin' => '18 Januari 2014',
                'end' => '19 Januari 2014',
            ),
            'announcement' => '27 Januari 2014',
            'final' => '14 Februari 2014',
        ),
        'university' => array(
            'registration' => array(
                'begin' => '11 November 2013',
                'end' => '3 Desember 2013',
            ),
            'semifinal' => array(
                'begin' => '5 Desember 2013',
                'end' => '9 Desember 2013',
            ),
            'announcement' => '27 Januari 2014',
            'final' => '14 Februari 2014',
        ),
        'paper' => array(
            'registration' => array(
                'begin' => '9 Desember 2013',
                'end' => '12 Januari 2014',
            ),
            'semifinal' => array(
                'begin' => '9 Desember 2013',
                'end' => '12 Januari 2014',
            ),
            'announcement' => '27 Januari 2014',
        ),
        'seminar' => array(
            'time' => '15 Februari 2014',
        ),
    ),
);