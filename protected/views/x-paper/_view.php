<?php
/* @var $this PaperController */
/* @var $data Paper */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->contestant_id), array('view', 'id'=>$data->contestant_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_phone')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_address')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_email')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_school_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_school_name); ?>
	<br />


</div>