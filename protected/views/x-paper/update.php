<?php
/* @var $this PaperController */
/* @var $model Paper */

$this->breadcrumbs=array(
	'Papers'=>array('index'),
	$model->contestant_id=>array('view','id'=>$model->contestant_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Paper', 'url'=>array('index')),
	array('label'=>'Create Paper', 'url'=>array('create')),
	array('label'=>'View Paper', 'url'=>array('view', 'id'=>$model->contestant_id)),
	array('label'=>'Manage Paper', 'url'=>array('admin')),
);
?>

<h1>Update Paper <?php echo $model->contestant_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>