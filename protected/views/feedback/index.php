<?php
/* @var $this CacianController */
?>

<?php if (Yii::app()->user->hasFlash('success')) : ?>
	<div class="alert alert-success" style="width: 1100px; position: relative;	bottom: 25px; left: 75px; display: inline-block;"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php else : ?>
<form class="form-horizontal" action="<?php echo $this->id; ?>/makian" method="post" role="form">
	<div class="form-group">
		<label class="control-label col-md-2" for="name">Nama Lengkap</label>
		<div class="col-md-4">
			<input size="50" maxlength="50" name="name" class="form-control" type="text">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="email">Email</label>
		<div class="col-md-4">
			<input size="50" maxlength="50" name="email" class="form-control" type="email">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="saran">Saran</label>
		<div class="col-md-4">
			<textarea name="saran" class="form-control" style="width: 600px; height: 200px;"></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			<button type="submit" class="btn btn-default">Kirim</button>
		</div>
	</div>
</form>
<?php endif; ?>