<?php
/* @var $this SeminarController */
//require $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseUrl.'/protected/views/universal/index.php';
$name = (Yii::app()->user->type > 2) ? $model->contestant_name : 'Tim '.$model->contestant_team_name;
?>
	<ul class="contestant-announcement">
		<h4>Selamat Datang <?php echo $name; ?></h4>
		<?php if(Yii::app()->user->status == 2): ?>
		<li>
			Selamat! Anda sudah resmi terdaftar sebagai peserta Seminar Nasional Matematika MCF - MMC ITB 2014!
			Terima kasih atas partisipasinya <!--, sampai berjumpa di periode pemodelan pada tanggal <?php //echo Yii::app()->params['university']['semifinal']['begin'].' - '.Yii::app()->params['university']['semifinal']['end']; ?>! -->
		</li>
		<?php else: ?>
		<li>
			Silahkan melengkapi <a href="<?php echo Yii::app()->controller->createUrl('profil'); ?>">Data Profil</a> dengan menekan menu yang telah disediakan.
		</li>
		<?php endif; ?>
	</ul>