<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<style type="text/css">
.seminar-person{
width: 195px;
display: inline-block;
}
.seminar-employment{
display: inline;
}
.seminar-type{
width: 225px;
display: inline-block;
}
.seminar-price{
display: inline;
}
.htm > div,
.htm > p
{
margin-bottom: 5px;
}
</style>
<div class="desc-content">
	<div style="width: 100%; display: inline-block; margin-bottom: 40px;">
		<div class="reg-btn-wrapper">
			<a href="<?php echo Yii::app()->createUrl('/seminar_nasional/pendaftaran'); ?>" class="btn btn-warning btn-lg btn-block<?php echo Configuration::isContestStarted($model_type)?'':' disabled'; ?>">DAFTAR</a>
		</div>
	</div>

	<h3 class="desc-header" style="margin-top: 0px !important;">Deskripsi</h3>
	<p>Matematika merupakan salah satu ilmu dasar yang memegang peranan penting dalam pengembangan berbagai disiplin ilmu lain. Begitu luasnya peran matematika dalam kehidupan, namun hanya sebagian kecil masyarakat Indonesia yang menyadari hal tersebut. Tidak sedikit pula mahasiswa matematika yang masih ragu-ragu dengan bekal ilmu yang mereka untuk bisa menjajaki dunia keprofesian di bidang industri dan penelitian. Begitu sedikitnya kabar mengenai track record matematikawan yang sukses menerapkan ilmu matematika dalam pembangunan Indonesia mengakibatkan turunnya minat mahasiswa untuk berkecimpung di dunia kerja yang bersenjatakan ilmu matematika.</p>
	<p>Melalui Seminar Nasional MCF – MMC ITB 2014, kami hendak menyampaikan bagaimana seorang matematikawan dapat sangat berpengaruh dalam pembangunan nasional, secara terkhusus melalui pemodelan matematika yang diterapkan dalam dunia industri dan riset. Dalam seminar ini, kami menyajikan pembicara-pembicara dengan pengalaman kerja yang sangat erat dengan penggunaan matematika yang sangat canggih. Dari sisi akademisi, Prof. Edy Soewono, guru besar Matematika Industri dan Keuangan (MIK) ITB akan menyampaikan seperti apakah pemodelan matematika itu dan seberapa kuat kah pemodelan matematika menyelesaikan berbagai permasalahan yang sangat kompleks. Dari sisi praktisi, terdapat 3 alumni Matematika dengan latar belakang profesi di industri maskapai penerbangan, industri keuangan, dan badan riset meteorologi yang akan membagikan pengalaman bermatematika mereka dengan bidang non matematika yang mereka geluti saat ini, yang juga mengantar mereka sampai kepada profesi mereka saat ini yang tentunya sangat berpengaruh dalam pembangunan nasional Negeri Indonesia.</p>

	<h3 class="desc-header">Tema</h3>
	<h2><i>“Exploring Mathematical Modeling in Industry and Research”</i></h2>

	<h3 class="desc-header">Pembicara</h3>
	<ol>
		<li><div class="seminar-person">Prof. Edy Soewono</div><div class="seminar-employment">(Guru Besar Matematika Industri dan Keuangan ITB)</div></li>
		<li><div class="seminar-person">Dr. Ayu Putri W</div><div class="seminar-employment">(Operation Planner PT Garuda Indonesia)</div></li>
		<li><div class="seminar-person">Dr. Ardhasena Sopaheluwakan</div><div class="seminar-employment">(Badan Meteorologi dan Geofisika Indonesia)</div></li>
		<li><div class="seminar-person">Dr. Riyanto Ahmadi</div><div class="seminar-employment">(Persatuan Aktuaris Indonesia)</div></li>
	</ol>

	<h3 class="desc-header">Waktu dan Tempat Pelaksanaan</h3>
	<p><?php echo Yii::app()->params['seminar']['time']; ?></p>
	<p>
	Galeri Campus Center Timur ITB<br>
	Jalan Ganesha 5<br>
	Bandung
	</p>

	<h3 class="desc-header">HTM</h3>
	<div class="htm">
		<div><div class="seminar-type">Mahasiswa ITB</div><div class="seminar-price">Rp 20.000,00</div></div>
		<div><div class="seminar-type">Umum</div><div class="seminar-price">Rp 35.000,00</div></div>
		<p style="margin-top: 15px;">Pendaftaran online dibuka s.d. 14 Februari 2014.</p>
		<p>Peserta seminar mahasiswa ITB akan diverifikasi statusnya saat registrasi seminar di Hari - H. Harap membawa KTM Anda saat Hari - H seminar.</p>
	</div>

	<h3 class="desc-header">Fasilitas</h3>
	<ul style="margin-left: 20px;">
		<li>Seminar Kit</li>
		<li>Sertifikat</li>
		<li>Snack</li>
	</ul>
</div>