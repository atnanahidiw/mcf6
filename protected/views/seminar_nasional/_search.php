<?php
/* @var $this PaperController */
/* @var $model Paper */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'contestant_id'); ?>
		<?php echo $form->textField($model,'contestant_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_name'); ?>
		<?php echo $form->textField($model,'contestant_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_phone'); ?>
		<?php echo $form->textField($model,'contestant_phone',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_address'); ?>
		<?php echo $form->textArea($model,'contestant_address',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_email'); ?>
		<?php echo $form->textField($model,'contestant_email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_school_name'); ?>
		<?php echo $form->textField($model,'contestant_school_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->