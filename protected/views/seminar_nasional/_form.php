<?php
/* @var $this SeminarController */
/* @var $model Seminar */

$new   = $model->isNewRecord;
$admin = (!$new && Yii::app()->user->type==0);
?>

<div class="form-error-message alert alert-danger"></div>

<form enctype="multipart/form-data" class="form-horizontal" action="<?php echo $this->action->id; ?>" method="post" role="form" style="margin-bottom: 50px;">
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Pribadi</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_name">Nomor Kartu Identitas*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Seminar[contestant_id_number]" id="Seminar_contestant_id_number" class="form-control" type="text" value="<?php echo $new ? '' : $model->contestant_id_number; ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_name">Nama Lengkap*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Seminar[contestant_name]" id="Seminar_contestant_name" class="form-control" type="text" value="<?php echo $new ? '' : $model->contestant_name; ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Seminar[contestant_address]" id="Seminar_contestant_address" class="form-control"><?php echo $new ? '' : $model->contestant_address; ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_email">Email*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Seminar[contestant_email]" id="Seminar_contestant_email" class="form-control" type="email" value="<?php echo $new ? '' : $model->contestant_email; ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_phone">Telepon / HP*</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="Seminar[contestant_phone]" id="Seminar_contestant_phone" class="form-control" type="text" value="<?php echo $new ? '' : $model->contestant_phone; ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_institution_phone">Mahasiswa ITB?*</label>
		<div class="col-md-2">
			<label class="radio-inline">
				<input type="radio" name="Seminar[contestant_institution_type]" value=1 <?php echo ($model->contestant_institution_type == 1)? 'checked' : ''; ?>> Ya
			</label>
			<label class="radio-inline">
				<input type="radio" name="Seminar[contestant_institution_type]" value=0 <?php echo ($new || ($model->contestant_institution_type == 0))? 'checked' : ''; ?>> Tidak
			</label>
		</div>
	</div>
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Institusi</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_institution_name">Nama Institusi*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Seminar[contestant_institution_name]" id="Seminar_contestant_institution_name" class="form-control" type="text" value="<?php echo $new ? '' : $model->contestant_institution_name; ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_institution_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Seminar[contestant_institution_address]" id="Seminar_contestant_institution_address" class="form-control"><?php echo $new ? '' : $model->contestant_institution_address; ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Seminar_contestant_institution_phone">Telepon / Faksimile*</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="Seminar[contestant_institution_phone]" id="Seminar_contestant_institution_phone" class="form-control" type="text" value="<?php echo $new ? '' : $model->contestant_institution_phone; ?>">
		</div>
	</div>
	<?php if(!$admin): ?>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			<button type="submit" class="btn btn-default"><?php echo $new ? 'Daftar' : 'Simpan'; ?></button>
		</div>
	</div>
	<?php endif; ?>
</form>

<script language="javascript" type="text/javascript">
var photo_size = 1024 * 1024;
var number =new RegExp('^\[0-9]{9,15}$');
jQuery('button[type=submit]').click(function(){
	var error = '';
	jQuery('.has-error').removeClass('has-error');
	jQuery('.form-error-message').hide();
    if(jQuery('#Seminar_contestant_id_number').val() == '' ){
		error = 'Nomor Kartu Identitas perlu diisi';
		jQuery('#Seminar_contestant_id_number').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Seminar_contestant_name').val() == '' ){
		error = 'Nama Lengkap perlu diisi';
		jQuery('#Seminar_contestant_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Seminar_contestant_phone').val() == '' ){
		error = 'Telepon / HP perlu diisi';
		jQuery('#Seminar_contestant_phone').parent().parent().addClass('has-error');
	}
    else if(jQuery('#Seminar_contestant_email').val() == '' ){
		error = 'Email perlu diisi';
		jQuery('#Seminar_contestant_email').parent().parent().addClass('has-error');
	}
	else if(!number.test(jQuery('#Seminar_contestant_phone').val())){
		error = 'Format Telepon / HP tidak benar';
		jQuery('#Seminar_contestant_phone').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Seminar_contestant_institution_name').val() == '' ){
		error = 'Nama Sekolah perlu diisi';
		jQuery('#Seminar_contestant_institution_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Seminar_contestant_institution_phone').val() == '' ){
		error = 'Telepon / Faksimile Sekolah perlu diisi';
		jQuery('#Seminar_contestant_institution_phone').parent().parent().addClass('has-error');
	}
	else if(!number.test(jQuery('#Seminar_contestant_institution_phone').val())){
		error = 'Format Telepon / Faksimile Sekolah tidak benar';
		jQuery('#Seminar_contestant_institution_phone').parent().parent().addClass('has-error');
	}
	if(error != ''){
		jQuery('.form-error-message').html(error);
		jQuery('.form-error-message').css('display','inline-block');
		window.scrollTo(0,0);
		return false;
	}
});
</script>