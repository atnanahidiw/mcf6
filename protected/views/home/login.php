<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/countdown.js" type="text/javascript"></script>
<div style="width: 450px; position: relative; float: right; top: 20px; right: 25px;">
<!--<div class="linecon-star" style="margin: 0 0 50px -5px; color: #000; font-size: 19px; font-weight: 600;">Launching Web</div>-->
<div class="countdown0">
	<div class="linecon-star" style="margin: 0 0 40px -5px; color: #000; font-size: 24px; font-weight: 900;">Final MCF-MMC ITB 2014</div>
	<script type="application/javascript">
	var myCountdown0 = new Countdown({
		time: <?php echo strtotime('2/14/2014 +7') - strtotime('now') + 86400; ?>,
		width:300,
		height:60,
		inline: true,
		rangeHi:"day",
		style:"flip"	// <- no comma on last item!
		});
	</script>
</div>
<div class="countdown1" style="display: none;">
	<div class="linecon-star" style="margin: 0 0 44px -5px; color: #000; font-size: 22px; font-weight: 900;">Seminar Nasional Matematika ITB 2014</div>
<!--	<div style="margin: 0 0 37px 30px; color: #000; font-size: 24px; font-weight: 600;">ITB 2014</div> -->
	<script type="application/javascript">
	var myCountdown1 = new Countdown({
		time: <?php echo strtotime('2/15/2014 +7') - strtotime('now') + 86400; ?>,
		width:300,
		height:60,
		inline: true,
		rangeHi:"day",
		style:"flip"	// <- no comma on last item!
		});
	</script>
</div>
<div><h1 class="linecon-doc" style="margin: 45px 0 10px;">Berita Terbaru</h1></div>
<div class="hot-news">
	<div class="clearfif cl">
		<header>
                <div class="news-header"><i>Pengunguman Finalis Kompetisi Pemodelan Matematika Siswa</i></div>
			<time class="news-time">
				<div class="day">30</div>
				<div class="month">Jan</div>
			</time>
		</header>
		<div class="news-content" style="margin-top: 10px;">
				<p>Halo peserta Kompetisi Pemodelan Matematika Siswa!</p>
				<p>Melalui berita ini, kami sampaikan daftar tim yang lolos ke babak final KPMS MCF – MMC ITB 2014, yaitu sebagai berikut (tidak ditulis berdasar urutan) :</p>
				<div><strong>Problem A – Desain Game RPG</strong></div>
				<div style="margin:0 0 10px 3px;">...</div>
			<a href="<?php echo Yii::app()->createUrl('/berita#news-17'); ?>"><button type="button" class="btn btn-warning btn-xs">Baca Selengkapnya</button></a>
		</div>
	</div>
</div>
</div>
<div id="slideshow">
	<?php for($i=1;$i<3;++$i) echo '<img src="'.Yii::app()->request->baseUrl.'/img/slideshow/slideshow'.$i.'.png" border="0">'; ?>
</div>

<?php
$sponsor = array('IOM.jpg');
$media   = array('olimpiade.org.jpg', '8eh.jpg', 'boulevard.jpg', 'promobdg.png', 'Radio Kampus.jpg', 'paseban.png', 'rol.png', 'seputarkampuscom.png');
?>

<div id="sponsor">
	<h1 class="linecon-diamond" style="margin: 20px 0;">Sponsor</h1>
	<div id="sponsor-carousel">
		<?php for($i=0;$i<sizeof($sponsor);++$i): ?>
		<div class="sponsor-carousel-inner">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/sponsor_media_partner/<?php echo $sponsor[$i];?>" border="0">
		</div>
		<div class="sponsor-carousel-inner">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/sponsor_media_partner/<?php echo $sponsor[$i];?>" border="0">
		</div>
		<?php endfor; ?>
	</div>
</div>

<div id="media">
	<h1 class="linecon-tv" style="margin: 20px 0;">Media Partner</h1>
	<div id="media-carousel">
		<?php for($i=0;$i<sizeof($media);++$i): ?>
		<div class="media-carousel-inner">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/sponsor_media_partner/<?php echo $media[$i];?>" border="0">
		</div>
		<?php endfor; ?>
	</div>
</div>

<script type="text/javascript">
var i = 0;
jQuery('document').ready( function() {
	jQuery('#slideshow img').eq(0).fadeIn();
	function doSomething(){
		var current_img = jQuery('#slideshow img').eq(i);
		var next_img = jQuery('#slideshow img').eq((i+1)%2);
		current_img.fadeOut(2000, function(){
			next_img.fadeIn(2000, function(){
				i = (i+1)%2;
			});
		});
	}

	(function loop() {
		var rand = Math.floor((Math.random()*4)+4)*1000;
		setTimeout(function() {
			doSomething();
			loop();
		}, rand);
	}())

	var seen = 0;
	(function changeCountdown() {
		setTimeout(function() {
			seen = (seen+1)%2;
			if(seen == 0){
				jQuery('.countdown1').fadeOut(1000, function(){
					jQuery('.countdown0').fadeIn(1000);
				});
			}
			else{
				jQuery('.countdown0').fadeOut(1000, function(){
					jQuery('.countdown1').fadeIn(1000);
				});
			}
			changeCountdown();
		}, 10000);
	}())

	jQuery('#sponsor-carousel').show();
	jQuery('#sponsor-carousel').carouFredSel({
        items               : 1,
		circular            : true,
        direction           : "left",
        scroll : {
            items           : 1,
            easing          : "linear",
            duration        : 1000,
        }                   
    });

	jQuery('#media-carousel').show();
	jQuery('#media-carousel').carouFredSel({
        items               : 2,
		circular            : true,
        direction           : "left",
        scroll : {
            items           : 1,
            easing          : "linear",
            duration        : 1000,
        }                   
    });

	/*jQuery('#slideshow img').click(function(){
		window.location = $(this).attr("rel");
	});
	*/
});
</script>