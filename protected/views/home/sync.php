<?php
$result = array();
foreach($model as $mdl){
	$result[$mdl->contestant_id] = array(
		'contestant_id' => $mdl->contestant_id,
		'contestant_username' => $mdl->contestant_username,
		'contestant_password' => $mdl->contestant_password,
		'contestant_team_name' => School::model()->findByPk($mdl->contestant_id)->contestant_team_name,
	);
}
echo json_encode($result);