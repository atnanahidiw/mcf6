<?php
/* @var $this HomeController */
/* @var $topic_university Topic + amount */
?>
<div style="position: relative; bottom: 30px;">
	<div style="margin-left:75px;">
		<h3 style="margin-bottom: 20px;">Ubah Topik Pemodelan Siswa</h3>
		<div style="width: 100%; height: 40px;">
			<span class="col-md-5"><h4>Judul Topik</h4></span>
			<span class="col-md-4"><h4>ID Google Docs</h4></span>
		</div>
		<?php foreach($topic_school as $mdl){ ?>
		<form class="form-horizontal" action="<?php echo Yii::app()->createUrl('topic/edit').'?id='.$mdl->topic_id; ?>" method="post" role="form">
			<div class="form-group">
				<span class="col-md-5">
					<input size="100" maxlength="100" name="Topic[topic_title]" id="Topic_topic_title" class="form-control" type="text" value="<?php echo $mdl->topic_title; ?>">
				</span>
				<span class="col-md-4">
					<input name="Topic[topic_source]" id="Topic_topic_source" class="form-control" type="text" value="<?php echo $mdl->topic_source; ?>">
				</span>
				<span class="col-md-1">
					<input class="btn btn-default" type="submit" value="Ubah">
				</span>
			</div>
		</form>
		<?php } ?>
	</div>
	<div style="margin: 50px 0 0 75px;">
		<h3 style="margin-bottom: 20px;">Daftar Peserta Pemodelan Siswa yang Sudah Memilih Topik</h3>
		<table class="table table-hover table-condensed choose-topic-list" style="width: 1020px;">
			<thead>
				<tr>
					<th>#</th>
					<th>Nama Tim</th>
					<th>Topik Pilihan</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$i=0;
					foreach($choosens_school as $choosen){
						$elmt = '<td>'.(++$i).'</td>';
						$elmt .= '<td>'.$choosen['contestant'].'</td>';
						$menu = '<a title="Hapus" href="'.Yii::app()->createUrl('topic/delete/'.$choosen['id']).'"><i class="glyphicon glyphicon-remove"></i></a>';
						$elmt .= '<td>'.$choosen['topic'].$menu.'</td>';
						echo '<tr>'.$elmt.'</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
	<hr style="width: 1020px; margin: 40px 0 3px 75px; border: 2px solid #000; ">
	<hr style="width: 1020px; margin: 0 0 20px 75px; border: 2px solid #000; ">
	<div style="margin: 50px 0 0 75px;">
		<h3 style="margin-bottom: 20px;">Ubah Topik Pemodelan Mahasiswa</h3>
		<div style="width: 100%; height: 40px;">
			<span class="col-md-5"><h4>Judul Topik</h4></span>
			<span class="col-md-1"><h4>Kuota</h4></span>
			<span class="col-md-4"><h4>ID Google Docs</h4></span>
		</div>
		<?php foreach($topic_university as $mdl){ ?>
		<form class="form-horizontal" action="<?php echo Yii::app()->createUrl('topic/edit').'?id='.$mdl['topic']->topic_id; ?>" method="post" role="form">
			<div class="form-group">
				<span class="col-md-5">
					<input size="100" maxlength="100" name="Topic[topic_title]" id="Topic_topic_title" class="form-control" type="text" value="<?php echo $mdl['topic']->topic_title; ?>">
				</span>
				<span class="col-md-1">
					<input name="Topic[topic_quota]" id="Topic_topic_quota" class="form-control" type="text" value="<?php echo $mdl['topic']->topic_quota; ?>">
				</span>
				<span class="col-md-4">
					<input name="Topic[topic_source]" id="Topic_topic_source" class="form-control" type="text" value="<?php echo $mdl['topic']->topic_source; ?>">
				</span>
				<span class="col-md-1">
					<input class="btn btn-default" type="submit" value="Ubah">
				</span>
			</div>
		</form>
		<?php } ?>
	</div>
	<div style="margin: 50px 0 0 75px;">
		<h3 style="margin-bottom: 20px;">Pilihkan Topik Pemodelan Mahasiswa</h3>
		<?php if(isset($_GET['choosen'])): ?>
			<div class="alert alert-warning">Peserta Sudah Memilih Topik</div>
		<?php endif; ?>
		<form class="form-horizontal" action="<?php echo Yii::app()->createUrl('topic/choose'); ?>" method="post" role="form">
			<div class="form-group">
				<span class="col-md-2">
					<input name="username" id="username" class="form-control" type="text" placeholder="Username">
				</span>
				<span class="col-md-6">
					<select class="form-control" name="choice">
						<?php
						foreach($topic_university as $mdl){
							$remainder = $mdl['topic']->topic_quota - $mdl['amount'];
							echo '<option value="'.$mdl['topic']->topic_id.'">'.$mdl['topic']->topic_title.' ('.$remainder.')</option>';
						}
						?>
					<select>
				</span>
				<input class="btn btn-default" type="submit" value="Pilihkan">
			</div>
		</form>
	</div>
	<div style="margin: 50px 0 0 75px;">
		<h3 style="margin-bottom: 20px;">Daftar Peserta Pemodelan Mahasiswa yang Sudah Memilih Topik</h3>
		<table class="table table-hover table-condensed choose-topic-list" style="width: 1020px;">
			<thead>
				<tr>
					<th>#</th>
					<th>Nama Tim</th>
					<th>Topik Pilihan</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$i=0;
					foreach($choosens_university as $choosen){
						$elmt = '<td>'.(++$i).'</td>';
						$elmt .= '<td>'.$choosen['contestant'].'</td>';
						$menu = '<a title="Hapus" href="'.Yii::app()->createUrl('topic/delete/'.$choosen['id']).'"><i class="glyphicon glyphicon-remove"></i></a>';
						$elmt .= '<td>'.$choosen['topic'].$menu.'</td>';
						echo '<tr>'.$elmt.'</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<script type="application/javascript">
jQuery(function() {
    jQuery('a[title=Hapus]').click(function() {
        return confirm('Apakah Anda yakin ingin meghapus pilihan peserta?');
    });
});
</script>