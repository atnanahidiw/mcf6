<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2 style="margin: 50px 0 20px 20px;">Error <?php echo $code; ?></h2>

<div class="error" style="margin-left: 20px;">
<?php echo CHtml::encode($message); ?>
</div>