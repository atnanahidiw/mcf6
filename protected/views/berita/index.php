<?php
$half_size_univ = ceil(sizeof($university_contestant)/2);
?>
<div id="news-wrapper">
    <div class="news" id="news-17">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Pengunguman Finalis Kompetisi Pemodelan Matematika Siswa</i></div>
                <time class="news-time">
                    <div class="day">30</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo peserta Kompetisi Pemodelan Matematika Siswa!</p>
				<p>Melalui berita ini, kami sampaikan daftar tim yang lolos ke babak final KPMS MCF – MMC ITB 2014, yaitu sebagai berikut (tidak ditulis berdasar urutan) :</p>
				<div><strong>Problem A – Desain Game RPG</strong></div>
				<ul style="margin-left: 20px;">
					<li>Tim Wiskundig Genie (SMAN 1 Padang Panjang)</li>
					<li>Tim THE STAR (SMA Negeri 3 Bandung)</li>
					<li>Tim MERDEKA-91 (SMA Negeri 1 Garut)</li>
				</ul>
				<div><strong>Problem B – Spesifikasi Smartphone</strong></div>
				<ul style="margin-left: 20px;">
					<li>Tim Mustaghfirin (MAN Insan Cendikia Gorontalo)</li>
					<li>Tim GIRLS ON FIRE! (SMA Negeri 5 Surabaya)</li>
					<li>Tim SMA N 2 TANGERANG (SMA Negeri 2 Tangerang)</li>
				</ul>
				<p>Kami selaku panitia mengucapkan selamat pada setiap finalis yang lolos ke babak final! Keenam finalis ini akan kembali berkompetisi untuk mempresentasikan hasil penelitiannya dan juga memamerkan poster hasil penelitiannya di Kampus Ganesha Institut Teknologi Bandung pada tanggal 14-15 Februari 2014. Keenam finalis tersebut akan memperebutkan gelar juara pertama hingga ketiga “Mathematical Challenge Festival ITB 2014”.</p>
				<p>Kepada setiap tim yang belum lolos ke babak final, jangan berkecil hati! Predikat juara bukanlah segala-galanya, pengalaman bermatematika dalam periode modeling pastilah menjadi kenangan tersendiri bagi setiap tim. Lebih jauh dari itu, kami berharap pengalaman ini dapat menularkan semangat bermatematika yang dibawa oleh panitia MCF – MMC ITB kepada setiap peserta dari berbagai penjuru daerah di Indonesia, untuk nantinya juga kembali ditularkan pada rekan-rekan lainnya. Dan dengan demikian, bersama-sama kita wujudkan Indonesia Bermatematika!</p>
				<p>Akhir kata, panitia mengucapkan terima kasih atas partisipasi semua peserta dan juga mohon maaf atas kesalahan-kesalahan yang telah panitia lakukan selama ini.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-16">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Pengunguman Tiga Besar Kontes Menulis Esai Siswa</i></div>
                <time class="news-time">
                    <div class="day">27</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo peserta Kontes Menulis Esai Siswa!</p>
				<div>Melalui berita ini, kami sampaikan daftar tiga peserta dengan esai terbaik dalam Kontes Menulis Esai Siswa MCF – MMC ITB 2014, yaitu sebagai berikut (tidak ditulis berdasar urutan) :</div>
				<ul style="margin-left: 20px;">
					<li>Nudy Istifa Nugroho (SMA Negeri 1 Banjar)</li>
					<li>Mutia Ayu Cahyaningtyas (SMA Negeri 1 Medan)</li>
					<li>Gianina Dinda Pamungkas (SMA Negeri 4 Semarang)</li>
				</ul>
				<p>Kepada ketiga peserta di atas, kami ucapkan selamat atas keberhasilannya dalam merangkai kata dan gagasan mengenai bagaimana matematika berperan penting dalam berbagai penyelesaian masalah di dunia nyata. Untuk itu, ketiga peserta tersebut diundang mengikuti Seminar Nasional Matematika dan acara Penutupan MCF – MMC ITB 2014 di Kampus ITB, Ganesha, untuk menerima penghargaan dari panitia MCF – MMC ITB 2014.</p>
				<p>Kepada setiap peserta yang belum berhasil mencapai peringkat 3 besar, jangan berkecil hati! Penghargaan dan juga titel juara bukanlah segala-galanya, pengalaman bermatematika yang dituangkan dalam esai itulah hal yang paling berharga yang dirasakan oleh setiap peserta. Lebih lanjut lagi, kami berharap melalui event ini, semangat berkarya setiap peserta menjadi semakin membara dalam membangun #IndonesiaBermatematika !</p>
				<p>Akhir kata, panitia mengucapkan terima kasih atas partisipasi semua peserta dan juga mohon maaf atas kesalahan-kesalahan yang telah panitia lakukan selama ini.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-15">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Pengunguman Finalis Kompetisi Pemodelan Matematika Mahasiswa</i></div>
                <time class="news-time">
                    <div class="day">27</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo peserta Kompetisi Pemodelan Matematika Mahasiswa!</p>
				<p>Melalui berita ini, kami sampaikan daftar tim yang lolos ke babak final KPMM MCF – MMC ITB 2014, yaitu sebagai berikut (tidak ditulis berdasar urutan) :</p>
				<div><strong>Problem A – Desain Angklung</strong></div>
				<ul style="margin-left: 20px;">
					<li>Tim Sekala Math (Universitas Sumatera Utara)</li>
					<li>Tim Ochuuuuuu (Universitas Indonesia)</li>
				</ul>
				<div><strong>Problem B – Masalah Optimasi Portofolio Investasi</strong></div>
				<ul style="margin-left: 20px;">
					<li>Tim UPH A (Universitas Pelita Harapan)</li>
					<li>Tim Pedeef (Universitas Padjajaran)</li>
				</ul>
				<div><strong>Problem C – Masalah Optimasi Portofolio Investasi</strong></div>
				<ul style="margin-left: 20px;">
					<li>Tim Shadow (Universitas Indonesia)</li>
					<li>Tim Tolkim (Nanyang Technological University)</li>
				</ul>
				<p>Kami selaku panitia mengucapkan selamat pada setiap finalis yang lolos ke babak final! Setiap finalis dari tiap kategori akan kembali berkompetisi untuk mempresentasikan hasil penelitiannya dan juga memerkan poster hasil penelitiannya di Kampus Ganesha Institut Teknologi Bandung pada tanggal 14-15 Februari 2014. Untuk setiap kategori, kedua finalis tersebut akan memperebutkan gelar juara pertama “Mathematics Modeling Competition ITB 2014”.</p>
				<div>Setiap peserta akan mendapatkan sertifikat penghargaan dari panitia MCF – MMC ITB 2014 dengan beberapa predikat (sesuai urutan turun), yaitu:</div>
				<ul style="margin-left: 20px;">
					<li><strong>Finalis</strong></li>
					<li><strong>Honorable Mention</strong></li>
					<li><strong>Peserta</strong></li>
				</ul>
				<p>Predikat <strong>Finalis</strong> diberikan pada dua peserta terbaik dari setiap kategori.<br>
				Predikat <strong>Honorable Mention</strong> diberikan pada setiap peserta non finalis yang telah berhasil menyelesaikan pemodelan dengan langkah-langkah yang tepat dan hasil yang cukup memuaskan.<br>
				Predikat <strong>Peserta</strong> diberikan pada setiap peserta non finalis yang telah berhasil menyelesaikan seluruh proses pemodelan dengan baik.</p>
				<p>Kepada setiap tim yang belum lolos ke babak final, jangan berkecil hati! Predikat juara bukanlah segala-galanya, pengalaman bermatematika dalam periode modeling pastilah menjadi kenangan tersendiri bagi setiap tim. Lebih jauh dari itu, kami berharap pengalaman ini dapat menularkan semangat bermatematika yang dibawa oleh panitia MCF – MMC ITB kepada setiap peserta dari berbagai penjuru daerah di Indonesia, untuk nantinya juga kembali ditularkan pada rekan-rekan lainnya. Dan dengan demikian, bersama-sama kita wujudkan Indonesia Bermatematika!</p>
				<p>Akhir kata, panitia mengucapkan terima kasih atas partisipasi semua peserta dan juga mohon maaf atas kesalahan-kesalahan yang telah panitia lakukan selama ini.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-14">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Pendaftaran Seminar Nasional Matematika ITB 2014 telah dibuka!</i></div>
                <time class="news-time">
                    <div class="day">21</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Pendaftaran Seminar Nasional Matematika ITB <i>"Exploring Mathematical Modeling in Industry and Research"</i> telah dibuka!</p>
				<p>Seminar ini akan membahas bagaimana seorang matematikawan, dengan pemodelan matematika sebagai senjatanya, dapat berperan sangat esensial dalam dunia industri dan penelitian. Dalam seminar ini, seorang akademisi berpengalaman dan tiga praktisi alumni matematika yang bekerja di dunia industri dan penelitian non matematika akan berkolaborasi membagikan ilmu dan pengalaman mereka dalam mereka mewarnai Indonesia.</p>
				<p>Seminar ini dibuka untuk seluruh 'penggemar' matematika, baik dari kalangan mahasiswa, siswa SMA, dosen matematika, guru matematika, atau pun dari segala jenis profesi!</p>
				<p>Pendaftaran dapat dilakukan melalui tautan <a target="_blank" href="http://www.math.itb.ac.id/mcf-mmc/seminar_nasional/pendaftaran"><span class="label label-info">Pendaftaran Seminar Nasional Matematika ITB</span></a></p>
				<p>Jangan lewatkan kesempatan berharga ini!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-13">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Berakhirnya Periode Pemodelan Matematika untuk tingkat Siswa dan Mahasiswa</i></div>
                <time class="news-time">
                    <div class="day">20</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Ratusan siswa dan mahasiswa dari berbagai penjuru nusantara telah berkompetisi dan menyelesaikan mini penelitian mereka mengenai beberapa permasalahan dari dunia nyata. Baik dari tingkat siswa maupun mahasiswa, tim-tim bersaing memperebutkan 6 posisi terbaik, di mana keenam tim tersebut nantinya akan diundang ke ITB untuk mengikuti babak final. Dalam babak final, setiap finalis harus mempersiapkan sebuah presentasi mengenai hasil penelitiannya untuk dinilai para dewan juri, dan juga sebuah poster yang juga berisi hasil penelitiannya untuk dipamerkan di pameran karya matematika ITB 2014.</p>
				<div>Adapun dewan juri kompetisi pemodelan MCF - MMC ITB 2014 adalah sebagai berikut:</div>
				<ul style="margin-left: 20px;">
                    <li>Dr. Agus Yodi Gunawan (ITB)</li>
					<li>Dr. Sri Redjeki Pudjaprasetya F. (ITB)</li>
					<li>Dr. Mochamad Apri (ITB)</li>
					<li>Dr. Nuning Nuraini (ITB)</li>
					<li>Dr. Ardhasena Sopaheluwakan (BMKG)</li>
					<li>Dr. Novriana Sumarti (ITB)</li>
					<li>Dr. Kuntjoro Adji Sidharto (ITB)</li>
					<li>Dr. Riyanto Ahmadi (PAI)</li>
					<li>Dila Puspita, MSi.</li>
                </ul>
				<p>Daftar finalis Kompetisi Pemodelan Matematika Mahasiswa ITB akan dipublikasikan di web MCF-MMC ITB 2014 paling lambat tanggal 27 Januari 2014. Daftar finalis Kompetisi Pemodelan Matematika Siswa dan Kontes Menulis Esai Siswa ITB akan dipublish di web MCF-MMC ITB 2014 paling lambat tanggal 30 Januari 2014. Sementara sebelum tanggal-tanggal tsb, setiap finalis akan dihubungi oleh panitia MCF-MMC ITB 2014 sebelumnya. Pada hari di mana daftar finalis dipublikasikan di web MCF-MMC ITB 2014, peserta juga dapat mengunduh sertifikat mereka di laman login mereka masing-masing.</p>
				<p>Demikian pemberitauan. Kami berharap melalui setiap detik pengalaman bermatematika peserta kompetisi MCF-MMC ITB 2014, setiap peserta dapat menjadi agen perubahan di daerah masing-masing untuk mewujudkan Indonesia yang lebih baik melalui matematika.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-12">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Periode Semifinal Kompetisi Pemodelan Matematika Siswa Telah Dimulai!</i></div>
                <time class="news-time">
                    <div class="day">18</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo semifinalis Kompetisi Pemodelan Siswa MCF ITB 2014!</p>
				<p>Periode semifinal, yaitu periode proses pemodelan matematika telah dimulai! Waktu yang diberikan untuk pengerjaan pemodelan, termasuk pembuatan mini laporan pemodelan, adalah selama 2 hari, terhitung tanggal 18-19 Januari 2014.</p>
				<p>Kembali kami ingatkan setiap peserta untuk benar-benar memahami setiap teknis komopetisi babak semifinal ini dengan sebaik mungkin, sehingga tidak ada peserta yang hasilnya menjadi tidak maksimal hanya karena kegagalan peserta dalam memahami teknis kompetisi.</p>
				<p>Manfaatkan setiap waktu yang ada untuk memperoleh pengalaman bermatematika semaksimal mungkin! Selamat menikmati matematika!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-11">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Teknis Semifinal Kompetisi Pemodelan Matematika Siswa</i></div>
                <time class="news-time">
                    <div class="day">14</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo Peserta Kompetisi Pemodelan Siswa MCF ITB 2014!</p>
				<p>Tahap penyisihan sudah berlangsung pada tanggal 12 Januari yang lalu. Saat ini kami tengah melalukan penilaian hasil penyisihan. Setiap semifinalis akan dihubungi oleh panitia maksimal Rabu, 15 Januari 2014.</p>
				<p>Diingatkan kembali bahwa semifinal akan berlangsung pada tanggal 18-19 Januari 2014, secara online juga. Pada tahap ini, peserta akan diberikan suatu permasalahan yang ada di dunia nyata, dan kemudian peserta diminta untuk membuat suatu model matematikanya. Pada tahapan ini, akan diberikan dua buah permasalahan, dan peserta cukup mengerjakan salah satu saja. Kedua masalah memiliki bobot penilaian yang sama. Di akhir periode, peserta diminta untuk mengumpulkan berkas mini laporan penelitiannya ke website MCF ITB 2014. Mengenai spesifikasi format laporan, dapat dilihat di <link teknis lomba>. Peserta dapat mengakses soal dan mengumpulkan jawaban mulai tanggal 18 Januari 2014 pk 00.00 WIB s.d 19 Januari 2014 pk 23.59 WIB. Di luar itu, panitia tidak menerima pengumpulan berkas laporan lagi.</p>
				<p>Teknis pengerjaan pemodelan matematika adalah sebagai berikut. Peserta login di website www.math.itb.ac.id/mcf-mmc dan kemudian mengunduh soal pemodelan. Setelah itu, peserta dapat memilih salah satu dari kedua masalah untuk dikerjakan pemodelannya. Jika peserta merasa membutuhkan informasi tambahan untuk mengerjakan penelitian, peserta dipersilakan untuk meriset sendiri informasi tersebut melalui media apapun. Panitia tidak akan menerima pertanyaan mengenai materi masalah yang ada dalam soal pemodelan. Jika diperlukan, peserta dipersilakan untuk membuat asumsi sendiri untuk dapat menyelesaikan masalah pemodelan (jangan lupa untuk mencantumkan asumsi tersebut di laporan penelitian).</p>
				<p>Demikian penjelasan mengenai semifinal Kompetisi Pemodelan Matematika Siswa MCF ITB 2014. Semoga sukses!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-10">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Teknis Penyisihan Kompetisi Pemodelan Matematika Siswa</i></div>
                <time class="news-time">
                    <div class="day">10</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo peserta Kompetisi Pemodelan Matematika Siswa!</p>
				<p>Berikut kami sampaikan beberapa peraturan teknis mengenai penyisihan lomba. Penyisihan KPMS akan diadakan secara online pada tanggal 12 Januari 2014. Penyisihan <strong>TIDAK</strong> dilakukan pada website ini, namun dilakukan di alamat website <a target="_blank" href="http://penyisihan.mcf-mmc.web.id"><span class="label label-info">penyisihan.mcf-mmc.web.id</span></a>. Penyisihan berlangsung selama paling lama 2 jam. Peserta hanya dapat login ke laman penyisihan pada tanggal <strong>12 Januari 2014 pukul 09.00 s.d. 20.00 WIB</strong>. Waktu yang diberikan dari pukul 09.00 s.d 20.00 WIB tersebut merupakan periode peserta dapat memulai kompetisi, tidak termasuk waktu pengerjaan soal, jadi pukul 19.59 WIB adalah waktu terakhir peserta masuk ke laman penyisihan, <strong>BUKAN</strong> waktu terakhir peserta mengumpulkan jawaban.</p>
				<p>Saat peserta login ke laman penyisihan, peserta akan menghadapi 4 paket soal matematika pilihan ganda, dengan masing-masing paketnya berisi 10 soal. Waktu pengerjaan setiap paket soal paling lama 30 menit. Jika dalam 30 menit peserta belum mengumpulkan semua jawaban dari soal-soal yang ada, maka sistem akan secara otomatis mengumpulkan jawaban yang sudah diinputkan peserta dan peserta akan dialihkan ke paket soal berikutnya. Setiap paket soal harus dikerjakan secara berturut-turut. Setelah peserta menyelesaikan suatu paket soal dan mengumpulkan ke sistem, maka jawaban yang sudah diinputkan tidak dapat diubah-ubah kembali. Dengan demikian, pengerjaan semua soal dalam babak penyisihan paling lama 2 jam.</p>
				<p>Saat pengerjaan soal, peserta dapat keluar dari halaman web secara sementara, namun jawaban yang belum disimpan akan secara otomatis terbuang dari sistem. Oleh sebab itu, kami menyarankan peserta untuk selalu menyimpan jawaban terlebih dahulu melalui menu <strong>Simpan</strong> yang tersedia.</p>
				<p>Jika peserta menekan tombol <strong>Kumpulkan</strong>, maka sesi pengerjaan paket yang sedang dikerjakan akan berakhir saat itu juga, dan setiap jawaban yang sudah diinput tidak dapat diubah lagi.</p>
				<p>Saat peserta keluar dari laman web, <i>countdown timer</i> tetap akan berjalan.</p>
				<p>Suatu akun tidak dapat diakses oleh lebih dari 1 komputer.</p>
				<p>Setiap jawaban benar akan bernilai 4 poin, jawaban salah bernilai -1 poin, dan jawaban kosong bernilai 0 poin.</p>
				<p>Untuk mempersiapkan diri dalam babak penyisihan, kami menyarankan peserta untuk menyediakan komputer dan koneksi internet yang sangat terpercaya. Panitia tidak bertanggung jawab atas hasil yang tidak optimal atas terjadinya kesalahan-kelasahan teknis seperti: salah klik, mati listrik, internet putus, dsb. Kami menyarankan peserta untuk tidak terburu-buru login dan masuk ke laman penyisihan, tapi terlebih dahulu berunding dan menentukan jam yang paling tepat untuk setiap peserta dapat mengerjakan soal-soal secara bersama-sama dari antara pukul 09.00 s.d. 20.00 WIB.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-9">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Surat Pernyataan Kejujuran Akademik</i></div>
                <time class="news-time">
                    <div class="day">10</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
				<p>Halo peserta Kompetisi Pemodelan Matematika Siswa!</p>
				<p>Melalui berita ini, kami mengingatkan bahwa setelah penyisihan berakhir, setiap peserta harus mengumpulkan hasil scan atau foto surat pernyataan kejujuran akademik yang sudah ditandatangani setiap anggota kelompok dan juga supervisor. Format surat pernyataan kejujuran akademik dapat diunduh di laman login di web <a href="http://www.math.itb.ac.id/mcf-mmc"><span class="label label-info">www.math.itb.ac.id/mcf-mmc</span></a> peserta masing-masing.</p>
				<p>Surat pernyataan kejujuran akademik ini dikumpulkan ke panitia melalui email ke alamat <a href="mailto:mcf.mmc.itb@gmail.com"><span class="label label-info">mcf.mmc.itb@gmail.com</span></a> melalui <i>attachment</i>. File yang di<i>attach</i> tidak boleh lebih besar dari 5 MB. Format <i>Subject</i> email yaitu <strong>Surat Pernyataan Kejujuran Akademik &ltnama tim&gt</strong>. Sebagai contoh, jika nama tim Anda <strong>Tim Hore-Hore</strong>, maka <i>subject</i> email Anda adalah <strong>Surat Pernyataan Kejujuran Akademik Tim Hore-Hore</strong>. Penulisan <i>subject</i> harus tepat seperti yang dicontohkan. Kesalahan penulisan subject dapat mengakibatkan tidak diprosesnya surat pernyataan kejujuran akademik tim yang bersangkutan.</p>
				<p>Pengumpulan surat paling lambat <strong>Senin, 13 Januari 2014 pukul 18.00</strong>. Tim yang tidak atau terlambat mengumpulkan surat pernyataan kejujuran akademik dapat didiskualifikasi dari kompetisi.</p>
				<p>Demikian pemberitahuan mengenai surat pernyataan kejujuran akademik. Mohon untuk menjadi perhatian. Terima kasih! Selamat berkompetisi!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-8">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Perpanjangan Pendaftaran Kompetisi Pemodelan Siswa</i></div>
                <time class="news-time">
                    <div class="day">5</div>
                    <div class="month">Jan</div>
                </time>
            </header>
            <div class="news-content">
                <p>Berdasarkan permohonan berbagai pihak untuk memperpanjang waktu pendaftaran. Kami selaku panitia MCF-MMC ITB 2014 memutuskan untuk <strong>memperpanjang waktu pendaftaran Kompetisi Pemodelan Matematika Siswa  sampai dengan 10 Januari 2014.</strong></p>
                <p>
                    Jadwal untuk Kompetisi Pemodelan Matematika Siswa MCF-MMC ITB 2014 :<br>
                    <strong>
                    <div class="desc-list">
                        <span class="col-md-2">Pendaftaran</span>
                        <span>: <?php echo Yii::app()->params['school']['registration']['begin'].' - '.Yii::app()->params['school']['registration']['end']; ?></span>
                    </div>
                    </strong>
                    <div class="desc-list">
                        <span class="col-md-2">Penyisihan</span>
                        <span>: <?php echo Yii::app()->params['school']['preliminary']; ?> (secara <i>online</i>)</span>
                    </div>
                    <div class="desc-list">
                        <span class="col-md-2">Semifinal</span>
                        <span>: <?php echo Yii::app()->params['school']['semifinal']['begin'].' - '.Yii::app()->params['school']['semifinal']['end']; ?> (secara <i>online</i>)</span>
                    </div>
                    <div class="desc-list">
                        <span class="col-md-2">Final</span>
                        <span>: <?php echo Yii::app()->params['school']['final']; ?></span>
                    </div>
                </p>
                <p>Dengan adanya keputusan tersebut , kami selaku panitia berharap tidak ada pihak manapun yang merasa dirugikan.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-7">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Pendaftaran Kompetisi Pemodelan dan Kontes Esai bagi Siswa SMA telah Dibuka!</i></div>
                <time class="news-time">
                    <div class="day">12</div>
                    <div class="month">Dec</div>
                </time>
            </header>
            <div class="news-content">
                <p>Halo semua! Sebelumnya panitia memohon maaf atas keterlambatan pembukaan pendaftaran yang seharusnya dijadwalkan pada tanggal 9 Desember 2013, dikarenakan alasan teknis. Pendaftaran Kompetisi Pemodelan Matematika Siswa dibuka sampai dengan tanggal 5 Januari 2014, sementara pendaftaran dan pengumpulan naskah esai matematika dibuka sampai dengan tanggal 12 Januari 2014.</p>
                <p>Perlu menjadi catatan penting bahwa <strong>tidak diperkenankan seorang siswa SMA mengikuti kompetisi pemodelan dan kontes menulis esai sekaligus</strong>.</p>
                <p>Jangan lewatkan kesempatan emas ini! Pendaftaran dapat dilakukan melalui link berikut:</p>
                    <p><a href="<?php echo Yii::app()->createUrl('pemodelan_siswa/pendaftaran'); ?>"><span class="label label-info">Pendaftaran Kompetisi Pemodelan Matematika Siswa SMA</span></a></p>
                    <p><a href="<?php echo Yii::app()->createUrl('esai/pendaftaran'); ?>"><span class="label label-info">Pendaftaran Kontes Esai Matematika Siswa SMA</span></a></p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-6">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>59 Tim Mahasiswa Indonesia telah Berhasil Menyelesaikan KPMM ITB 2014!</i></div>
                <time class="news-time">
                    <div class="day">11</div>
                    <div class="month">Dec</div>
                </time>
            </header>
            <div class="news-content">
                <p>Halo Semua! Periode pemodelan mahasiswa telah usai! Sebanyak 59 tim mahasiswa Indonesia dari berbagai penjuru tempat telah berhasil menyelesaikan proses pemodelan matematika selama 5 hari berturut-turut, terhitung dari tanggal 5 Desember 2013 sampai dengan 9 Desember 2013. Angka 59 telah menembus target awal panitia MCF – MMC ITB 2014, yang semula hanya ditargetkan sebanyak 50 tim saja. Perubahan target peserta ini disesuaikan dengan tingginya antusiasme mahasiswa sarjana berbagai universitas dari seluruh wilayah Indonesia untuk dapat berpartisipasi dalam pengalaman kompetisi pemodelan matematika pertama di Indonesia. 58 tim yang terdaftar berasal dari berbagai PTN dan juga PTS dari berbagai wilayah di Indonesia, mulai dari Medan sampai Jayapura; sementara 1 tim sisanya berasal dari NTU (Nanyang Technological University), yang notabene semua pesertanya juga merupakan warga negara Indonesia yang tengah menyelesaikan studi di negara Singapura. Antusiasme ini tidak hanya dimiliki oleh mahasiswa dari jurusan matematika dan statistika saja, namun juga dimiliki oleh teman-teman jurusan lain, seperti dari teknik komputasi Telkom University, teknik kelautan ITB, teknik elektro NTU, dan lain-lain.</p>
                <p>Dalam proses pemodelan, setiap peserta diberi kesempatan untuk memilih satu dari tiga buah kasus pemodelan. Kasus pertama adalah mengenai bagaimana model matematika untuk mendesain sebuah angklung untuk memaksimumkan keindahan suaranya. Kasus kedua mengenai bagaimana membuat perencanaan investasi jangka panjang supaya return yang didapat sebesar mungkin dengan resiko yang sekecil mungkin. Kasus ketiga adalah mengenai bagaimana membuat model matematika proses evaporasi air dalam siklus hidrologi dengan menggunakan data-data meteorologi. Detail ketiga buah kasus tersebut dapat dilihat melalui tautan berikut:</p>
                <ul style="margin-left: 20px;">
                    <li><a target="_blank" href="https://docs.google.com/file/d/0By_WcqkJZeCgRjBZLTV4ejhqeUE/preview">Problem A - Desain Angklung</a></li>
                    <li><a target="_blank" href="https://docs.google.com/file/d/0By_WcqkJZeCgS0xWeVFjbF9MZUU/preview">Problem B - Masalah Optimasi Portofolio Investasi</a></li>
                    <li><a target="_blank" href="https://docs.google.com/file/d/0By_WcqkJZeCgWnlCNW5zTERHUkk/preview">Problem C - Menentukan Model Evaporasi Menggunakan Data Meteorologi</a></li>
                </ul>
                <p>Terdata dalam database panitia, terdapat sebanyak 17 tim yang memilih kasus angklung, 23 tim yang memilih kasus perencanaan investasi, dan 19 tim yang memilih kasus evaporasi. Dari ketiga kelompok tim ini kemudian akan dipilih dua tim dengan hasil penelitian terbaik, untuk nantinya diundang ke ITB untuk menyampaikan poster dan presentasi hasil penelitian mereka. Pengunguman finalis dan pengunduhan sertifikat dapat diakses melalui website MCF – MMC ITB 2014 paling lambat tanggal 27 Januari 2014.</p>
                <p>Akhir kata, kami selaku panitia MCF – MMC ITB 2014 mengucapkan banyak terima kasih atas partisipasi setiap peserta dan juga memohon maaf atas berbagai kekurangan dari setiap panitia selama proses pendaftaran dan proses pemodelan berlangsung!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-5">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Kompetisi Pemodelan Matematika Mahasiswa telah dimulai!</i></div>
                <time class="news-time">
                    <div class="day">5</div>
                    <div class="month">Dec</div>
                </time>
            </header>
            <div class="news-content">
                <p>Halo semua!</p>
                <p>Periode pemodelan mahasiswa telah dimulai! Ini merupakan momen pertama sebuah kompetisi pemodelan matematika untuk tingkat mahasiswa sarjana di Indonesia!</p>
                <p>Diingatkan kembali bagi setiap peserta bahwa pemilihan topik permasalahan hanya dapat dilakukan secara online melalui laman peserta masing-masing yang dapat diakses setelah <i>login</i>. Pemilihan topik permasalahan hanya dapat dilakukan pada periode 5 Desember 2013 pukul 00.00 WIB sampai dengan pukul 22.00 WIB. Jangan sampai kehabisan kuota permasalahan!</p>
                <p>Selamat berkompetisi dan semoga berhasil!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-4">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Daftar Peserta Kompetisi Pemodelan Matematika Mahasiswa</i></div>
                <time class="news-time">
                    <div class="day">4</div>
                    <div class="month">Dec</div>
                </time>
            </header>
            <div class="news-content">
                <p>Berikut adalah daftar peserta Kompetisi Pemodelan Matematika Mahasiswa:</p>
                <table style="width: 49.5%; position: relative; float: left;">
                    <thead style="background-color: #bf002d;">
                        <tr>
                            <th style="color: #fff;">No</th>
                            <th style="color: #fff; width: 39.8%;">Nama Tim</th>
                            <th style="color: #fff; width: 60%;">Asal Universitas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for($i=0; $i<$half_size_univ; ++$i){
                            $uni = $university_contestant[$i]
                        ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $uni->contestant_team_name; ?></td>
                            <td><?php echo ucwords($uni->contestant_university_name); ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <table style="width: 49.5%; position: relative; float: right;">
                    <thead style="background-color: #bf002d;">
                        <tr>
                            <th style="color: #fff;">No</th>
                            <th style="color: #fff; width: 39.8%;">Nama Tim</th>
                            <th style="color: #fff; width: 60%;">Asal Universitas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for($i=$half_size_univ; $i<sizeof($university_contestant); ++$i){
                            $uni = $university_contestant[$i]
                        ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $uni->contestant_team_name; ?></td> 
                            <td><?php echo ucwords($uni->contestant_university_name); ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-3">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Yang perlu dipersiapkan peserta Kompetisi Pemodelan Matematika Mahasiswa</i></div>
                <time class="news-time">
                    <div class="day">3</div>
                    <div class="month">Dec</div>
                </time>
            </header>
            <div class="news-content">
                <p>Halo peserta Kompetisi Pemodelan Matematika Mahasiswa</p>
                <p>Sebentar lagi periode kompetisi pemodelan akan dimulai! Sudahkah teman-teman mempersiapkan diri? Untuk itu, kami selaku panitia MCF – MMC ITB 2014 menyarankan setiap peserta untuk menyiapkan diri dalam hal-hal berikut:</p>
                <ul>
                <li><p><strong>Basecamp tim</strong>, yaitu suatu tempat (bisa sebuah ruangan di kampus, rumah kosan, rumah pribadi, ruang kantor dosen, dll) di mana teman-teman akan memfokuskan diri untuk melakukan penelitian secara terfokus dalam periode 5-9 Desember 2014. Sangat tidak disarankan untuk teman-teman dalam satu tim mengerjakan pemodelan dalam dua lokasi berbeda. Tidak disarankan juga teman-teman memiliki lokasi basecamp yang tidak tetap selama periode 5-9 Desember. Oleh sebab itu kami sarankan teman-teman dapat mengurus setiap perijinan ruangan ataupun rumah yang akan teman-teman gunakan sebagai basecamp sebelum proses pemodelan berlangsung. (termasuk ijin penggunaan ruangan di kampus saat akhir pekan)</p></li>
                <li><p><strong>Fasilitas internet yang memadai.</strong> Dalam proses pemodelan, teman-teman akan sangat membutuhkan akses internet, baik sebagai referensi maupun sebagai sumber data pemodelan. Untuk itu, kami menyarankan supaya teman-teman sudah menyiapkan semua keperluan tersebut sebelum hari-H pelaksanaan (pulsa internet, pengadaan modem, setting router hotspot, ijin penggunaan hotspot kampus, dll) supaya teman-teman tidak kehilangan banyak waktu untuk mengurus hal-hal semacam ini saat proses pemodelan berlangsung</p></li>
                <li><p><strong>Buku Teks pendukung referensi.</strong> Kami menyarankan teman-teman untuk mulai dapat menyiapkan semua buku teks yang dirasa perlu dalam proses modeling. Mengingat kompetisi kita akan melewati masa akhir pekan, di mana kemungkinan perpustakaan kampus tidak beroperasi, kami menyarankan teman-teman dapat meminjam buku-buku dari perpustakaan kampus teman-teman sebelum perpustakaan tidak beroperasi saat masa akhir pekan.</p></li>
                <li><p><strong>Suplemen kesehatan dan Obat-obatan pribadi.</strong> Kami menyarankan teman-teman dalam satu tim bisa menghabiskan waktu setidaknya 10 jam dalam sehari untuk bersama-sama mengerjakan pemodelan. Ini merupakan kegiatan yang cukup menguras tenaga, pikiran, dan juga emosi. Oleh sebab itu, suplemen kesehatan dan obat-obatan pribadi sangat disarankan untuk selalu dibawa saat proses pemodelan berlangsung.</p></li>
                <li><p><strong>Catering makanan.</strong> Karena proses pemodelan harus dilakukan secara sangat intense dalam periode 5-9 Desember ini, kami tidak menyarankan peserta berleha-leha (bahkan untuk makan) dalam mengerjakan pemodelan. Oleh sebab itu, untuk membuat waktu pemodelan teman-teman sangat efektif, kami sarankan teman-teman mulai hari ini untuk memesan catering makanan ke basecamp teman-teman setiap harinya, sehingga teman-teman tidak kehabisan banyak waktu hanya untuk mencari makan.</p></li>
                <li><p><strong>Hati, pikiran, dan fisik yang kuat.</strong> Dalam proses pemodelan, akan terjadi banyak kendala dan kesulitan yang memiliki tingkat stress tersendiri. Selain itu, kegiatan ini juga membutuhkan energi yang tidak sedikit. Energi yang dimaksud tidak hanya energi secara fisik, namun juga secara pikiran. Untuk mempersiapkan hal itu, kami menyarankan teman-teman untuk dapat mempersolid ikatan antar anggota tim dan juga menjaga kesehatan fisik teman-teman.</p></li>
                <li><p><strong>Pemahaman teknis kompetisi.</strong> Sebelum kompetisi mulai, kami menyarankan teman-teman untuk benar-benar memahami teknis kompetisi, seperti bagaimana sistem pemilihan kasus, format laporan, pengumpulan laporan, dll. Terutama mengenai teknis pemilihan kasus, perlu kami ingatkan bahwa periode pemilihan kasus pemodelan hanya dibuka pada tanggal 5 Desember 2014 pk 00.00 s.d pk 22.00 WIB. Untuk itu, jangan kehilangan kesempatan untuk memilih kasus yang paling suitable bagi teman-teman.</p></li>
                <p>Demikian hal-hal yang perlu dipersiapkan sebelum kompetisi. Perlu kami tekankan bahwa kegagalan peserta dalam proses pemodelan karena kurangnya persiapan atas hal-hal di atas tidak menjadi tanggung jawab panitia. Panitia hanya akan menerima pertanyaan mengenai teknis kompetisi saja, mengenai akomodasi dan kebutuhan pribadi setiap peserta sepenuhnya menjadi tanggungan peserta.</p>
                <p>Perlu diinformasikan juga bahwa untuk setiap kasus, kami tidak menerima pertanyaan mengenai tambahan informasi yang dibutuhkan dalam proses pemodelan. Jika teman-teman merasa membutuhkan informasi yang lebih banyak, silakan teman-teman meriset sendiri informasi yang dibutuhkan dari berbagai sumber (internet, buku teks, dll).</p>
                </ul>
                <p>Selamat mempersiapkan diri, dan <i>Enjoy the Math!</i></p>
                <p><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-2">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Perpanjangan Pendaftaran Kompetisi Pemodelan Mahasiswa</i></div>
                <time class="news-time">
                    <div class="day">29</div>
                    <div class="month">Nov</div>
                </time>
            </header>
            <div class="news-content">
                <p>Berdasarkan permohonan berbagai pihak untuk memperpanjang waktu pendaftaran. Kami selaku panitia MCF-MMC ITB 2014 memutuskan untuk <strong>memperpanjang waktu pendaftaran Kompetisi Pemodelan Matematika Mahasiswa  sampai dengan 3 Desember 2014.</strong></p>
                <p>
                    Jadwal untuk Kompetisi Pemodelan Matematika Mahasiswa MCF-MMC ITB 2014 :<br>
                    <strong>
                    <div class="desc-list">
                        <span class="col-md-2">Pendaftaran</span>
                        <span>: <?php echo Yii::app()->params['university']['registration']['begin'].' - '.Yii::app()->params['university']['registration']['end']; ?></span>
                    </div>
                    </strong>
                    <div class="desc-list">
                        <span class="col-md-2">Pelaksanaan Pemodelan</span>
                        <span>: <?php echo Yii::app()->params['university']['semifinal']['begin'].' - '.Yii::app()->params['university']['semifinal']['end']; ?></span>
                    </div>
                    <div class="desc-list">
                        <span class="col-md-2">Pengumuman Finalis</span>
                        <span>: <?php echo Yii::app()->params['university']['announcement']; ?></span>
                    </div>
                    <div class="desc-list">
                        <span class="col-md-2">Final</span>
                        <span>: <?php echo Yii::app()->params['university']['final']; ?></span>
                    </div>
                </p>
                <p>Dengan adanya keputusan tersebut , kami selaku panitia berharap tidak ada pihak manapun yang merasa dirugikan.</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
    <hr class="news-separator">
    <div class="news" id="news-1">
        <div class="clearfif cl">
            <header>
                <div class="news-header"><i>Pendaftaran Kompetisi Pemodelan Matematika Mahasiswa Telah Dibuka!</i></div>
                <time class="news-time">
                    <div class="day">11</div>
                    <div class="month">Nov</div>
                </time>
            </header>
            <div class="news-content">
                <p>Pendaftaran Kompetisi Pemodelan Matematika Mahasiswa dibuka sampai dengan Jum'at, 29 November 2013 pukul 23.59 WIB.</p>
                <p>Kompetisi ini merupakan Kompetisi Pemodelan <strong>Pertama di Indonesia</strong> yang ditujukan untuk tingkat mahasiswa.</p>
                <p>Pendaftaran dapat dilakukan melalui tautan <a href="<?php echo Yii::app()->createUrl('pemodelan_mahasiswa/pendaftaran'); ?>"><span class="label label-info">Pendaftaran Pemodelan Matematika Mahasiswa</span></a></p>
                <p>Hanya 50 Tim dengan persyaratan lengkap yang dapat mengikutinya.</p>
                <p>Segera daftarkan Tim-mu pada Kompetisi Pemodelan Matematika Mahasiswa MCF-MMC ITB 2014!</p>
                <p style="clear: both;"><br>Salam #IndonesiaBermatematika !</p>
                <p>Panitia MCF – MMC ITB 2014</p>
            </div>
        </div>
    </div>
</div>
