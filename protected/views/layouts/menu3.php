									<li <?php echo (Yii::app()->controller->id=='pemodelan_siswa') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-pencil">
											<a title="Pemodelan Siswa" href="<?php echo Yii::app()->createUrl('pemodelan_siswa/admin'); ?>">
												<span class="link-text">Pemodelan Siswa</span>
												<span class="link-desc"></span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->id=='pemodelan_mahasiswa') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-graduation-cap">
											<a title="Pemodelan Mahasiswa" href="<?php echo Yii::app()->createUrl('pemodelan_mahasiswa/admin'); ?>">
												<span class="link-text">Pemodelan Mahasiswa</span>
												<span class="link-desc"></span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->id=='esai') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-attach">
											<a title="Esai" href="<?php echo Yii::app()->createUrl('esai/admin'); ?>">
												<span class="link-text">Esai</span>
												<span class="link-desc"></span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->id=='seminar_nasional') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-globe">
											<a title="Seminar Nasional" href="<?php echo Yii::app()->createUrl('seminar_nasional/admin'); ?>">
												<span class="link-text">Seminar Nasional</span>
												<span class="link-desc"></span>
											</a>
										</span>
									</li>