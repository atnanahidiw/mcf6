									<li <?php echo (Yii::app()->controller->id=='tentang') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-note">
											<a title="Apa itu MCF?" href="<?php echo Yii::app()->createUrl("/tentang"); ?>">
												<span class="link-text">Tentang</span>
												<span class="link-desc">Apa itu MCF?</span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->id=='berita') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-doc">
											<a title="Info Terkait Acara" href="<?php echo Yii::app()->createUrl("/berita"); ?>">
												<span class="link-text">Berita</span>
												<span class="link-desc">Info Terkait Acara</span>
											</a>
										</span>
									</li>
									<?php 
										$acara = false;
										if(Yii::app()->controller->id=='pemodelan_siswa') $acara = true;
										if(Yii::app()->controller->id=='pemodelan_mahasiswa') $acara = true;
										if(Yii::app()->controller->id=='esai') $acara = true;
										if(Yii::app()->controller->id=='pameran_karya') $acara = true;
										if(Yii::app()->controller->id=='seminar_nasional') $acara = true;
									?>
									<li class="has-submenu<?php echo ($acara) ? " current-menu-item" : ""; ?>">
										<span class="menu-item-wrap linecon-lightbulb">
											<a title="Penjelasan Detil Acara">
												<span class="link-text">Acara</span>
												<span class="link-desc">Penjelasan Detil Acara</span>
											</a>
										</span>
										<ul>
											<li>
												<span class="menu-item-wrap">
													<a href="<?php echo Yii::app()->createUrl("/pemodelan_siswa"); ?>"><span class="link-text">Kompetisi Pemodelan Matematika Siswa</span></a>
												</span>
											</li>
											<li>
												<span class="menu-item-wrap">
													<a href="<?php echo Yii::app()->createUrl("/pemodelan_mahasiswa"); ?>"><span class="link-text">Kompetisi Pemodelan Matematika Mahasiswa</span></a>
												</span>
											</li>
											<li>
												<span class="menu-item-wrap">
													<a href="<?php echo Yii::app()->createUrl("/esai"); ?>"><span class="link-text">Kontes Menulis Esai Matematika </span></a>
												</span>
											</li>
											<li>
												<span class="menu-item-wrap">
													<a href="<?php echo Yii::app()->createUrl("/pameran_karya"); ?>"><span class="link-text">Pameran Karya Matematika ITB</span></a>
												</span>
											</li>
											<li>
												<span class="menu-item-wrap">
													<a href="<?php echo Yii::app()->createUrl("/seminar_nasional"); ?>"><span class="link-text">Seminar Nasional Matematika ITB</span></a>
												</span>
											</li>
										</ul>
									</li>
									<li <?php echo (Yii::app()->controller->id=='kontak') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-mail">
											<a title="Silahkan hubungi kami" href="<?php echo Yii::app()->createUrl("/kontak"); ?>">
												<span class="link-text">Kontak</span>
												<span class="link-desc">Silahkan hubungi kami</span>
											</a>
										</span>
									</li>