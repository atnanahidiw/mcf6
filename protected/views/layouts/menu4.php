									<li <?php echo (Yii::app()->controller->action->id=='index') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-sound">
											<a title="Pengumuman Lomba" href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id); ?>">
												<span class="link-text">Pengumuman</span>
												<span class="link-desc">Pengumuman Lomba</span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->action->id=='masalah') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-search">
											<a title="Soal & Pengumpulan Jawaban" href="<?php echo Yii::app()->getController()->createUrl("masalah"); ?>">
												<span class="link-text">Masalah</span>
												<span class="link-desc">Pilihan Topik Masalah</span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->action->id=='topik') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-tag">
											<a title="Topik" href="<?php echo Yii::app()->createUrl('tes/topik'); ?>">
												<span class="link-text">Topik</span>
												<span class="link-desc">Pilih Topik Pemodelan</span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->action->id=='pengumpulan') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-attach">
											<a title="Unggah Motivation Letter" href="<?php echo Yii::app()->getController()->createUrl("pengumpulan"); ?>">
												<span class="link-text">Pengumpulan</span>
												<span class="link-desc">Unggah Berkas Laporan</span>
											</a>
										</span>
									</li>