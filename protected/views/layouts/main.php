<?php
/* @var $this Controller */
$home = Yii::app()->createUrl("/");
switch(Yii::app()->controller->id){
    case 'tentang'                : $page = ' - Tentang';  break;
    case 'berita'                 : $page = ' - Berita';   break;
    case 'pemodelan_siswa'        : $page = ' - Acara';    break;
    case 'pemodelan_mahasiswa'    : $page = ' - Acara';    break;
    case 'esai'                   : $page = ' - Acara';    break;
    case 'pameran_karya'          : $page = ' - Acara';    break;
    case 'seminar_nasional'       : $page = ' - Acara';    break;
    case 'kontak'                 : $page = ' - Kontak';   break;
    default                       : $page = '';
}
if((Yii::app()->controller->id=='home')&&(Yii::app()->controller->action->id=='error')) $page = ' - Error';
?>
<!DOCTYPE html>
<html class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en-US" style="overflow: hidden;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Mathematical Challenge Festival (MCF) Institut Teknologi Bandung merupakan acara dwitahunan yang diselenggarakan oleh Himpunan Mahasiswa Matematika (HIMATIKA) ITB sejak tahun 2002. Dalam perkembangannya, MCF ITB telah diadakan sebanyak lima kali.">
        <meta name="keywords" content="MCF, MMC, MCF-MMC, MCF - MMC, MCF6, MCF 6, MCF 6 ITB, MCF ITB, MMC ITB, MCF-MMC ITB, MCF - MMC ITB, MCF 2014, MMC 2014, MCF-MMC 2014, MCF - MMC 2014, MCF ITB 2014, MMC ITB 2014, MCF-MMC ITB 2014, MCF - MMC ITB 2014, Mathematical Challenge Festival, Mathematical Challenge Festival ITB">
        <meta property="og:title" content="MCF-MMC ITB 2014">
        <meta property="og:url" content="http://www.math.itb.ac.id/mcf-mmc/">
        <meta property="og:image" content="http://www.math.itb.ac.id/mcf-mmc/img/logo.jpg">
        <title>MCF-MMC ITB 2014<?php echo $page; ?></title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl.'/img/'.(rand(0,1)==1?'mcf':'mmc').'.ico'; ?>">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C700italic%2C400%2C700&subset=latin%2Clatin-ext&ver=3.6.1">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C600%2C700%2C300italic%2C400italic%2C600italic%2C700italic&subset=latin%2Clatin-ext&ver=3.6.1">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icon-font-style.css?ver=3.6.1">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/app.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/options.css">
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jsapi"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js?ver=1.10.2"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.tipsy.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.carouFredSel-6.2.1.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    </head>
    <body class="page page-template page-template-tmp-archive-left-img-php siteorigin-panels wpb-js-composer js-comp-ver-3.6.7 vc_responsive" style="">
        <div id="change_wrap_div">
            <section id="header" class="horizontal">
                <div class="row">
                    <div class="twelve columns">
                        <div id="logo"><a href="<?php echo $home; ?>" title="Halaman Utama"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Untitled-21.png" alt="MCF-MMC 2014"></a></div>
                        <div id="header-login-form">
                            <?php if(Yii::app()->user->isGuest): ?>
                            <form class="form-inline" action="<?php echo Yii::app()->createUrl("/"); ?>/" method="post" role="form">
                                <div class="form-group">
                                    <label class="sr-only" for="inputUsername">Username</label>
                                    <input type="text" class="form-control" id="LoginForm_username" name="LoginForm[username]" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="inputPassword">Password</label>
                                    <input type="password" class="form-control" id="LoginForm_password" name="LoginForm[password]" placeholder="Password">
                                </div>
                                <input class="btn btn-default btn-sm" type="submit" value="Masuk">
                            </form>

                            <?php else: ?>
                            <a id="logout" href="<?php echo Yii::app()->createUrl("/home/logout"); ?>">Keluar</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="header-navi-wrap crum_start_animation">
                    <div class="header-navi">
                        <div class="header-navi-inner">
                            <nav id="top-menu" class="fake">
                                <ul id="menu-top" class="menu">
                                <?php require $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseUrl.'/protected/views/layouts/menu.php'; ?>
                                </ul>
                            </nav>
                            <div class="cl"></div>
                        </div>
                    </div>
                </div>
                <div class="droped-navi" style="top: -60px; opacity: 0;">
                    <div class="droped-navi-inner">
                        <div class="small-logo">
                            <a href="<?php echo $home; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Untitled-21.png" alt="MCF-MMC 2014" title="Halaman Utama"></a>
                        </div>
                        <nav id="drop-top-menu" class="fake">
                            <ul id="menu-top-1" class="menu">
                                <?php require $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseUrl.'/protected/views/layouts/menu.php'; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section>

            <?php if(Yii::app()->controller->id!='home') : ?>
            <div id="stuning-header">
                <div class="row">
                    <div class="twelve columns">
                        <div id="page-title">
                            <a href="javascript:history.back()" class="back"></a>
                            <div class="page-title-inner">
                                <h1 class="page-title">
                                <?php
                                    $title = (Yii::app()->controller->action->id=="pendaftaran") ? "Pendaftaran" : Yii::app()->controller->id;
                                    echo ucwords(str_replace("_", " ", $title));
                                ?>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <section id="layout">
                <div class="row"><?php echo $content; ?></div>
            </section>

            <div class="push"></div>
        </div>

        <section id="sub-footer">
            <div class="row">
                <div class="six mobile-two columns">&copy Panitia MCF - MMC ITB 2014</div>
                <div class="six mobile-two columns right">#IndonesiaBermatematika</div>
            </div>
            <a href="#" id="linkTop" class="backtotop hidden"></a>
        </section>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.nicescroll.plus.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
        <div id="ascrail2000" class="nicescroll-rails" style="width: 10px; z-index: 9999; cursor: default; position: fixed; top: 0px; height: 100%; right: 0px; opacity: 0.8; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;">
            <div style="position: relative; top: 0px; float: right; width: 4px; height: 296px; background-color: rgb(97, 107, 116); border: 0px; background-clip: padding-box; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;"></div>
        </div>
        <div id="ascrail2000-hr" class="nicescroll-rails" style="height: 10px; z-index: 9999; position: fixed; left: 0px; width: 100%; bottom: 0px; opacity: 0.8; cursor: default; display: none;">
            <div style="position: relative; top: 0px; height: 10px; width: 1366px; background-color: rgb(97, 107, 116); border: 0px; background-clip: padding-box; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;"></div>
        </div>
    </body>
</html>