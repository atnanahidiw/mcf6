<?php
/* @var $this Controller */
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/app.css">
	</head>
	<body class="page page-template page-template-tmp-archive-left-img-php siteorigin-panels wpb-js-composer js-comp-ver-3.6.7 vc_responsive" style="">
		<div id="change_wrap_div">
			<section id="layout">
				<div class="row"><?php echo $content; ?></div>
			</section>
		</div>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
	</body>
</html>