									<li <?php echo (Yii::app()->controller->action->id=='index') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-sound">
											<a title="Pengumuman Lomba" href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id); ?>">
												<span class="link-text">Pengumuman</span>
												<span class="link-desc">Pengumuman Lomba</span>
											</a>
										</span>
									</li>
									<?php if(Yii::app()->user->status < 2): ?>
									<li <?php echo (Yii::app()->controller->action->id=='profil') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-user">
											<a title="Ubah Data Diri" href="<?php echo Yii::app()->getController()->createUrl("profil"); ?>">
												<span class="link-text">Profil</span>
												<span class="link-desc">Ubah Data Diri</span>
											</a>
										</span>
									</li>
									<?php endif; ?>
									<!--
									<li <?php echo (Yii::app()->controller->action->id=='password') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-lock">
											<a title="Ganti Password" href="<?php echo Yii::app()->getController()->createUrl("password"); ?>">
												<span class="link-text">Password</span>
												<span class="link-desc">Ganti Password</span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->action->id=='motivation_letter') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-attach">
											<a title="Unggah Motivation Letter" href="<?php echo Yii::app()->getController()->createUrl("motivation_letter"); ?>">
												<span class="link-text">Motivation Letter</span>
												<span class="link-desc">Unggah Motivation Letter</span>
											</a>
										</span>
									</li>
									-->
									<?php //if(date('Y-m-d', strtotime('now')) == '2013-12-05'): ?>
								<?php if(Yii::app()->user->type == 1): ?>
									<?php
									$have_choose = TopicChoice::model()->findByAttributes(array('contestant_id'=>Yii::app()->user->id));
									if($have_choose == null):
									?>
									
									<li <?php echo (Yii::app()->controller->action->id=='masalah') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-search">
											<a title="Soal & Pengumpulan Jawaban" href="<?php echo Yii::app()->getController()->createUrl("masalah"); ?>">
												<span class="link-text">Masalah</span>
												<span class="link-desc">Pilihan Topik Masalah</span>
											</a>
										</span>
									</li>
									<li <?php echo (Yii::app()->controller->action->id=='topik') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-tag">
											<a title="Topik" href="<?php echo Yii::app()->getController()->createUrl("topik"); ?>">
												<span class="link-text">Topik</span>
												<span class="link-desc">Pilih Topik Pemodelan</span>
											</a>
										</span>
									</li>
									<?php else: ?>
									<li <?php echo (Yii::app()->controller->action->id=='pengumpulan') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-attach">
											<a title="Unggah Motivation Letter" href="<?php echo Yii::app()->getController()->createUrl("pengumpulan"); ?>">
												<span class="link-text">Pengumpulan</span>
												<span class="link-desc">Unggah Berkas Laporan Pemodelan</span>
											</a>
										</span>
									</li>
									<?php endif; ?>
								<?php endif; ?>
									<?php if(Yii::app()->user->type == 3): ?>
									<li <?php echo (Yii::app()->controller->action->id=='pengumpulan') ? "class=\"current-menu-item\"" : ""; ?>>
										<span class="menu-item-wrap linecon-attach">
											<a title="Unggah Motivation Letter" href="<?php echo Yii::app()->getController()->createUrl("pengumpulan"); ?>">
												<span class="link-text">Pengumpulan</span>
												<span class="link-desc">Unggah Berkas Esai</span>
											</a>
										</span>
									</li>
									<?php endif; ?>