<?php
/* @var $this SchoolController */
$name = (Yii::app()->user->type > 2) ? $model->contestant_name : 'Tim '.$model->contestant_team_name;
?>
	<ul class="contestant-announcement">
		<h4>Selamat Datang <?php echo $name; ?></h4>
		<?php if(Yii::app()->user->status == 2): ?>
		<li>
			Sertifikat lomba dapat diunduh <a href="<?php echo Yii::app()->createUrl('contestant/certificate');?>"><span class="label label-info">di sini</span></a>
		</li>
			<?php if(time() >= strtotime('18 January 2014 00:00:01 GMT+7')): ?>
			<li>
				<p>Setiap Laporan Pemodelan yang dikumpulkan harus mencantumkan <i>Statement of Originality</i>. Contoh <i>Statement of Originality</i> dapat diunduh di <a href="<?php echo Yii::app()->baseUrl.'/berkas/STATEMENT OF ORIGINALITY.doc'; ?>" target="_blank"><span class="label label-info">tautan ini</span></a></p>
			</li>
			<?php endif; ?>
		<li>
			<p><strong>Selamat! Tim Anda lolos ke semifinal!</strong></p>
			<p>Silahkan mempersiapkan diri untuk babak semifinal yang akan dilaksanakan pada tanggal <?php echo Yii::app()->params['school']['semifinal']['begin'].' - '.Yii::app()->params['school']['semifinal']['end']; ?>! </p>
		</li>
		<li>
			<p>Penyisihan <strong>TIDAK</strong> dilakukan pada website ini, namun dilakukan di alamat website <a target="_blank" href="http://penyisihan.mcf-mmc.web.id"><span class="label label-info">penyisihan.mcf-mmc.web.id</span></a>.</p>
		</li>
		<li>
			<p>Setiap peserta harus mengumpulkan hasil scan atau foto surat pernyataan kejujuran akademik yang sudah ditandatangani setiap anggota kelompok dan juga supervisor.</p>
			<p>Contoh surat pernyataan kejujuran akademik dapat diunduh di <a href="<?php echo Yii::app()->baseUrl.'/berkas/Surat Pernyataan Kejujuran Akademik.docx'; ?>" target="_blank"><span class="label label-info">tautan ini</span></a> 
		</li>
		<li>
			Selamat! Tim Anda sudah resmi terdaftar sebagai peserta Kompetisi Pemodelan Matematika tingkat Siswa MCF - MMC ITB 2014!
			Terima kasih atas partisipasinya <!--, sampai berjumpa di periode pemodelan pada tanggal <?php //echo Yii::app()->params['university']['semifinal']['begin'].' - '.Yii::app()->params['university']['semifinal']['end']; ?>! -->
		</li>
		<?php else: ?>
		<li>
			Silahkan melengkapi <a href="<?php echo Yii::app()->controller->createUrl('profil'); ?>">Data Profil</a> dengan menekan menu yang telah disediakan.
		</li>
		<?php endif; ?>
	</ul>
