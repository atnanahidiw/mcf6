<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="desc-content">
	<div style="width: 100%; display: inline-block; margin-bottom: 40px;">
		<div class="reg-btn-wrapper">
			<a href="<?php echo Yii::app()->createUrl('/pemodelan_siswa/pendaftaran'); ?>" class="btn btn-warning btn-lg btn-block<?php echo Configuration::isContestStarted($model_type)?'':' disabled'; ?>">DAFTAR</a>
		</div>
	</div>

	<h3 class="desc-header" style="margin-top: 0px !important;">Deskripsi</h3>
	<p>Kompetisi Pemodelan Matematika tingkat Siswa merupakan kompetisi pemodelan matematika yang ditujukan bagi pelajar SMA/sederajat dari seluruh wilayah Indonesia. Kompetisi ini bertujuan untuk mengenalkan matematika sebagai ilmu dasar yang dapat diaplikasikan langsung terhadap permasalahan di kehidupan sehari-hari.  Pada kompetisi ini, peserta akan dihadapkan pada suatu permasalahan yang akan dipecahkan dengan menggunakan konsep Matematika.</p>
	<p>Kompetisi ini dilakukan secara berkelelompok dengan beranggotakan maksimal 3 (tiga) siswa per tim. Pada babak penyisihan, setiap peserta akan diuji kecakapan matematikanya secara umum dengan mengerjakan soal – soal pilihan ganda secara berkelompok. Soal – soal yang diberikan dibagi atas beberapa kelompok dalam disiplin ilmu matematika seperti Aljabar, Kombinatorika, Geometri, Pertaksamaan, Kalkulus (Limit, Turunan, dan Integral), Optimasi (Modeling Sederhana), dan Statistika.</p>
	<p>Lima puluh tim terbaik dari babak penyisihan akan masuk ke dalam babak semi final. Pada tahap ini, setiap peserta akan diajak untuk membuat suatu model matematika dalam penyelesaian suatu masalah nyata. Tahapan ini akan dilaksanakan secara online melalui web ini.</p>
	<p>6 tim dengan laporan penelitian terbaik akan diundang ke kampus ITB Ganesha untuk mengikuti babak final dan juga serangkaian acara utama MCF – MMC ITB 2014. Di babak ini, setiap peserta akan mempresentasikan hasil penelitian mereka di hadapan para juri.</p>
	<p>Dengan lomba ini, kami berharap para pelajar SMA dapat melihat matematika dengan sudut pandang yang berbeda, yaitu bahwa matematika tidaklah seabstrak yang kerap dipikirkan banyak orang dan ternyata begitu dekat dengan kehidupan sehari – hari.</p>
	<p>Detail peraturan kompetisi dapat diunduh melalui link berikut <a href="<?php echo Yii::app()->baseUrl.'/berkas/KPMS 2014.pdf'; ?>"><span class="label label-info">KPMS 2014 (diperbarui: 12 Desember 2013)</span></a></p>

	<h3 class="desc-header">Jadwal</h3>
	<div class="desc-list">
		<span class="col-md-2">Pendaftaran</span>
		<span>: <?php echo Yii::app()->params['school']['registration']['begin'].' - '.Yii::app()->params['school']['registration']['end']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Penyisihan</span>
		<span>: <?php echo Yii::app()->params['school']['preliminary']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Semifinal</span>
		<span>: <?php echo Yii::app()->params['school']['semifinal']['begin'].' - '.Yii::app()->params['school']['semifinal']['end']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Pengumuman Finalis</span>
		<span>: <?php echo Yii::app()->params['school']['announcement']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Final</span>
		<span>: <?php echo Yii::app()->params['school']['final']; ?></span>
	</div>

	<h3 class="desc-header">Hadiah</h3>
	<div class="desc-list">
		<span class="col-md-2">Juara 1</span>
		<span>: Rp 7.500.000,00</span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Juara 2</span>
		<span>: Rp 6.000.000,00</span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Juara 3</span>
		<span>: Rp 4.500.000,00</span>
	</div>
</div>
<script language="javascript" type="text/javascript">

var createTimer = function(options) {

  options = jQuery.extend({
    element : 'body',
    secondLeft : jQuery(options.element).text(),
    interval : 1000,
	getFormattedTime : function(){
		time = options.secondLeft;
		second = time % 60;
		time = (time - second) / 60;
		minute = time % 60;
		time = (time - minute) / 60;
		hour = time % 24;
		time = (time - hour) / 24;
		day = time % 365;
		return '(' + day + ' hari ' + hour + ' jam ' + minute + ' menit ' + second + ' detik lagi)';
	},
  }, options);
  
  
  setInterval(function() {
	if (options.secondLeft > 0) --options.secondLeft;
	else clearInterval(this);
    jQuery(options.element).text(options.getFormattedTime());  
  }, options.interval);
};

createTimer({element: '#pendaftaran'});
createTimer({element: '#penyisihan'});
createTimer({element: '#semifinal'});
createTimer({element: '#final'});
</script>