<?php
/* @var $this UniversityController */
$id = Yii::app()->user->id;
switch($id){
case 7   : $name = 'James Bond'; break;
case 8   : $name = 'Saras 008';  break;
case 21  : $name = 'Jakarta';    break;
case 22  : $name = 'Bandung';    break;
default  : $name = Contestant::model()->findByPk($id)->contestant_username;
}
?>
	<ul class="contestant-announcement">
		<h4>Selamat Datang <?php echo $name; ?></h4>
		<?php if(Yii::app()->user->status == 2): ?>
		<li>
			Setiap Laporan Pemodelan yang dikumpulkan harus mencantumkan <i>Statement of Originality</i>. Contoh <i>Statement of Originality</i> dapat diunduh di <a href="<?php echo Yii::app()->baseUrl.'/berkas/STATEMENT OF ORIGINALITY.doc'; ?>" target="_blank">tautan ini</a> 
		</li>
		<li>
			Selamat! Tim Anda sudah resmi terdaftar sebagai peserta Kompetisi Pemodelan Matematika tingkat Mahasiswa MCF - MMC ITB 2014!
			Terima kasih atas partisipasinya, sampai berjumpa di periode pemodelan pada tanggal <?php echo Yii::app()->params['university']['semifinal']['begin'].' - '.Yii::app()->params['university']['semifinal']['end']; ?>!
		</li>
		<?php endif; ?>
		<li>
			Silahkan melengkapi <a href="">Data Profil</a> dan mengunggah <a href="<?php echo Yii::app()->controller->createUrl('motivation_letter'); ?>" style="font-style: italic;">Motivation Letter</a> dengan menekan menu yang telah disediakan.<br>
			Jika sampai tanggal <?php echo date("d M Y", strtotime(Contestant::model()->findByPk($id)->contestant_expired_registration_date)) ?> persyaratan masih belum lengkap, Tim Anda akan dianggap  mengundurkan diri dan uang pendaftaran tidak dapat dikembalikan. 
		</li>
	</ul>
