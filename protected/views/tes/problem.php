<?php
/* @var $this UniversityController */
/* @var $model Topic*/
?>

<div style="margin-left: 75px;">
	<div>Berikut adalah pilihan topik masalah yang akan dilombakan</div>
	<?php
	$i = 1;
	foreach($model as $mdl){
	?>
		<div class="desc-list">
			<span class="col-md-1">Topik <?php echo $i; ?></span>
			<span>: <a class="label label-info" href="<?php echo $mdl->topic_source; ?>" target="_blank"><?php echo $mdl->topic_title; ?></a></span>
		</div>
	<?php
	++$i;
	}
	?>
	<div style="margin-top:50px;">Silahkan memilihnya di menu <a href="<?php echo Yii::app()->createUrl('tes/topik'); ?>">Topik</a></div>
</div>