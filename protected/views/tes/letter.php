<?php
/* @var $this UniversityController */
/* @var $model University */

$new   = true;
$admin = (!$new && Yii::app()->user->type==0);
?>

<div class="form-error-message alert alert-danger"></div>


<?php if (Yii::app()->user->hasFlash('warning')) : ?>
<div class="alert alert-warning letter-success"><?php echo Yii::app()->user->getFlash('warning'); ?></div>
<form enctype="multipart/form-data" class="form-horizontal" action="<?php echo $this->action->id; ?>" method="post" role="form" style="margin: 0 0 50px 75px;">
<?php else: ?>
<form enctype="multipart/form-data" class="form-horizontal" action="<?php echo $this->action->id; ?>" method="post" role="form" style="margin: 52px 0 50px 75px;">
<?php endif; ?>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_motivation_letter">Kumpulkan <i>Motivation Letter</i></label>
		<div class="col-md-5">
			<?php $file = ((!$new) && ($model->contestant_motivation_letter != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="input-group">
					<div class="form-control uneditable-input span3" data-trigger="fileinput" style="width: 360px;">
						<i class="glyphicon glyphicon-file fileinput-exists"></i>
						<span class="fileinput-filename"><?php echo ($file=='fileinput-exists') ? $model->contestant_motivation_letter : ''; ?></span>
					</div>
					<span class="input-group-addon btn btn-default btn-file">
						<span class="fileinput-new">Pilih Berkas</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file">
					</span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_motivation_letter"></label>
		<div class="col-md-6">
			<input class="btn btn-success" type="submit" value="Kumpulkan">
		</div>
	</div>
</form>
<script language="javascript" type="text/javascript">
var size = 10 * 1024 * 1024;
jQuery('.form-error-message').hide();
jQuery('input[type=submit]').click(function(){
	var error = '';
	jQuery('form').css('margin-top', '52px');
	jQuery('.has-error').removeClass('has-error');
	jQuery('.form-error-message').hide();
	if((file = jQuery('input[name=file]')[0].files[0]) != null){
		if(file.size > size){
			error = 'Ukuran Berkas terlalu besar (maksimal 10MB)';
		}
		else if(file.type != 'application/pdf'){
			error = 'Berkas yang diunggah harus berformat pdf';
		}
	}
	else return false;
	if(error != ''){
		jQuery('form').css('margin-top', '0');
		jQuery('.form-error-message').html(error);
		jQuery('.form-error-message').css('display','inline-block');
		window.scrollTo(0,0);
		return false;
	}
});
</script>