<?php
/* @var $this UniversityController */
/* @var $model University */
$is_started = Configuration::isContestStarted($model_type);
?>

<div class="quota-alert">
	<div>
	Sudah terdapat <strong><?php echo Contestant::model()->countByAttributes(array('contestant_type' => '2', 'contestant_status' => '2')); ?></strong> pendaftar yang telah melengkapi data dari <strong><?php echo Yii::app()->params['universityMaxContestantNumber'];?></strong> kuota yang tersedia
	</div>
</div>

<div style="width: 300px; margin: 0 auto 40px;">
		<a href="<?php echo Yii::app()->createUrl('/pemodelan_mahasiswa/'.($is_started?'stop':'start')); ?>" class="btn btn-lg btn-block <?php echo $is_started?'btn-danger':'btn-success'; ?>"><?php echo $is_started?'Tutup':'Buka';?> Pendaftaran</a>
</div>


<table class="table table-hover table-condensed admin-table">
	<thead>
		<tr>
			<th>#</th>
			<th>Username</th>
			<th>Nama Tim</th>
			<th>Email</th>
			<th>Status</th>
            <th>Batas Waktu Melengkapi Data</th>
			<th>Menu</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i=0;
			foreach($model as $mdl){
				$contestant = Contestant::model()->findByPk($mdl->contestant_id);
				$problem = Problem::model()->findByAttributes(array('contestant_id' => $mdl->contestant_id));
				$verify = $contestant->contestant_status;
				
				switch($verify){
					case 1  : $color='#fc0';  $message='Pembayaran sudah diverifikasi';    break;
					case 2  : $color='#0f0';  $message='Data Lengkap sudah diverifikasi';  break;
					default : $color='#999';  $message='Sama sekali belum diverifikasi';
				}
                if(($verify == 1) && ($contestant->contestant_expired_registration_date < date('Y-m-d', strtotime('now')))){
                    $color='#f00';
                    $message='Batas waktu terlewati';
                }
				$title      = ($verify == 0) ? 'Verifikasi Pembayaran' : 'Verifikasi Data Lengkap';
				$verify_act = ($verify == 0) ? 'paid' : 'complete';

				if($problem != null) $act = '<a style="margin: 0 5px;" title="Unduh Laporan Pemodelan" target="_blank" href="'.Yii::app()->baseUrl.'/backup/'.$problem->file_name.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
				else $act = '<a style="margin: 0 10px;">&nbsp</a>';
				$act .= '<a style="margin: 0 5px;" title="Unduh Sertifikat" href="'.Yii::app()->createUrl('contestant/certificate', array('type' => 2, 'id' => $mdl->contestant_id)).'"><i class="glyphicon glyphicon-file"></i></a>';
				$act .= '<a style="margin: 0 5px;" title="Lihat" href="'.Yii::app()->createUrl($this->id.'/view/'.$mdl->contestant_id).'"><i class="glyphicon glyphicon-user"></i></a>';
				$act .= '<a style="margin: 0 5px;" title="Hapus" href="'.Yii::app()->createUrl($this->id.'/delete/'.$mdl->contestant_id).'"><i class="glyphicon glyphicon-remove"></i></a>';
				$act .= $contestant->contestant_status == 2 ? '' : '<a style="margin: 0 5px;" title="'.$title.'" href="'.Yii::app()->createUrl($this->id.'/'.$verify_act.'/'.$mdl->contestant_id).'"><i class="glyphicon glyphicon-ok"></i></a>';

				$elmt = '<td>'.(++$i).'</td>';
				$elmt .= '<td>'.$contestant->contestant_username.'</td>';
				$elmt .= '<td>Tim '.$mdl->contestant_team_name.'</td>';
				$elmt .= '<td>'.$mdl->contestant_email.'</td>';
				$elmt .= '<td style="color: '.$color.';">'.$message.'</td>';
                $elmt .= '<td>'.(($contestant->contestant_expired_registration_date == '0000-00-00') ? '-' : date("d M Y", strtotime($contestant->contestant_expired_registration_date))).'</td>';
				$elmt .= '<td>'.$act.'</td>';
				echo '<tr>'.$elmt.'</tr>';
			}
		?>
	</tbody>
</table>

<script type="application/javascript">
jQuery(function() {
    jQuery('a[title=Hapus]').click(function() {
        return confirm('Apakah Anda yakin ingin meghapus peserta?');
    });
});
</script>