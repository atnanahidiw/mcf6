<?php
/* @var $this UniversityController */
/* @var $model University */

$new   = $model->isNewRecord;
$admin = false;
?>

<?php if($admin) :
    if($model->contestant_motivation_letter==''){
		$text = 'belum';
		$color = '#f00';
	}
	else{
		$text = 'sudah';
		$color = '#0f0';	
	}
?>
<div class="quota-alert" style="color: <?php echo $color; ?>;"><i>Motivation Letter</i> <?php echo $text; ?> diunggah</div>
<?php else: ?>
<div class="quota-alert">
	Masih terdapat <strong><?php echo Yii::app()->params['universityMaxContestantNumber'] - Contestant::model()->countByAttributes(array('contestant_type' => '2', 'contestant_status' => '2'));?></strong> kuota peserta tersedia
</div>
<?php endif ?>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id' => 'register-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array(
        	'class'=>'form-horizontal',
	        'role'=>'form',
	        'style'=>'margin: 0 0 50px 75px',
	        'enctype'=>'multipart/form-data',
        )
    )); 
?>

<div class="form-error-message alert alert-danger"></div>
<?php echo $form->errorSummary($model); ?>

<!-- <form enctype="multipart/form-data" class="form-horizontal" action="<?php echo $this->action->id; ?>" method="post" role="form" style="margin: 0 0 50px 75px;"> -->
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_team_name">Nama Tim*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_team_name]" id="University_contestant_team_name" class="form-control" type="text" value="<?php echo $model->contestant_team_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_team_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_email">Email*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_email]" id="University_contestant_email" class="form-control" type="email" value="<?php echo $model->contestant_email; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_email'); ?>
	</div>
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Peserta #1</h3> <h3 style="padding-top: 7px;">(Ketua)</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_first_photo">Pasfoto Berwarna</label>
		<div class="col-md-4">
			<?php $file = ((!$new) && ($model->contestant_first_photo != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 130px; height: 190px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_first_photo : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-0">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_first_photo'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_first_id">Kartu Identitas</label>
		<div class="col-md-4">
			<div>Tampak Depan</div>
			<?php $file = ((!$new) && ($model->contestant_first_id_f != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_first_id_f : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-1">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_first_id_f'); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div>Tampak Belakang</div>
			<?php $file = ((!$new) && ($model->contestant_first_id_b != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_first_id_b : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-2">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_first_id_b'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_first_name">Nama Lengkap*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_first_name]" id="University_contestant_first_name" class="form-control" type="text" value="<?php echo $model->contestant_first_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_first_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_first_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_first_address]" id="University_contestant_first_address" class="form-control"><?php echo $model->contestant_first_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_first_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_first_phone">Telepon / HP*</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="University[contestant_first_phone]" id="University_contestant_first_phone" class="form-control" type="text" value="<?php echo $model->contestant_first_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_first_phone'); ?>
	</div>
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Peserta #2</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_second_photo">Pasfoto Berwarna</label>
		<div class="col-md-4">
			<?php $file = ((!$new) && ($model->contestant_second_photo != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 130px; height: 190px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_second_photo : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-3">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_second_photo'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_second_id">Kartu Identitas</label>
		<div class="col-md-4">
			<div>Tampak Depan</div>
			<?php $file = ((!$new) && ($model->contestant_second_id_f != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_second_id_f : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-4">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_second_id_f'); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div>Tampak Belakang</div>
			<?php $file = ((!$new) && ($model->contestant_second_id_b != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_second_id_b : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-5">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_second_id_b'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_second_name">Nama Lengkap</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_second_name]" id="University_contestant_second_name" class="form-control" type="text" value="<?php echo $model->contestant_second_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_second_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_second_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_second_address]" id="University_contestant_second_address" class="form-control"><?php echo $model->contestant_second_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_second_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_second_phone">Telepon / HP</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="University[contestant_second_phone]" id="University_contestant_second_phone" class="form-control" type="text" value="<?php echo $model->contestant_second_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_second_phone'); ?>
	</div>	<div class="form-group">
		<h3 class="control-label col-md-2">Data Peserta #3</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_third_photo">Pasfoto Berwarna</label>
		<div class="col-md-4">
			<?php $file = ((!$new) && ($model->contestant_third_photo != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 130px; height: 190px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_third_photo : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-6">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_third_photo'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_third_id">Kartu Identitas</label>
		<div class="col-md-4">
			<div>Tampak Depan</div>
			<?php $file = ((!$new) && ($model->contestant_third_id_f != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_third_id_f : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-7">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_third_id_f'); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div>Tampak Belakang</div>
			<?php $file = ((!$new) && ($model->contestant_third_id_b != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_third_id_b : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-8">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_third_id_b'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_third_name">Nama Lengkap</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_third_name]" id="University_contestant_third_name" class="form-control" type="text" value="<?php echo $model->contestant_third_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_third_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_third_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_third_address]" id="University_contestant_third_address" class="form-control"><?php echo $model->contestant_third_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_third_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_third_phone">Telepon / HP</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="University[contestant_third_phone]" id="University_contestant_third_phone" class="form-control" type="text" value="<?php echo $model->contestant_third_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_third_phone'); ?>
	</div>
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Supervisor</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_supervisor_name">Nama Lengkap</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_supervisor_name]" id="University_contestant_supervisor_name" class="form-control" type="text" value="<?php echo $model->contestant_supervisor_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_supervisor_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_supervisor_nik">NIK</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_supervisor_nik]" id="University_contestant_supervisor_nik" class="form-control" type="text" value="<?php echo $model->contestant_supervisor_nik; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_supervisor_nik'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_supervisor_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_supervisor_address]" id="University_contestant_supervisor_address" class="form-control"><?php echo $model->contestant_supervisor_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_supervisor_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_supervisor_email">Email</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_supervisor_email]" id="University_contestant_supervisor_email" class="form-control" type="email" value="<?php echo $model->contestant_supervisor_email; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_supervisor_email'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_supervisor_phone">Telepon / HP</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="University[contestant_supervisor_phone]" id="University_contestant_supervisor_phone" class="form-control" type="text" value="<?php echo $model->contestant_supervisor_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_supervisor_phone'); ?>
	</div>
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Universitas</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_university_name">Nama Universitas*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_university_name]" id="University_contestant_university_name" class="form-control" type="text" value="<?php echo $model->contestant_university_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_university_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_university_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="University[contestant_university_address]" id="University_contestant_university_address" class="form-control"><?php echo $model->contestant_university_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_university_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="University_contestant_university_phone">Telepon / Faksimile*</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="University[contestant_university_phone]" id="University_contestant_university_phone" class="form-control" type="text" value="<?php echo $model->contestant_university_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_university_phone'); ?>
	</div>
	<?php if(!$admin): ?>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			<button type="submit" class="btn btn-default"><?php echo $new ? 'Daftar' : 'Simpan'; ?></button>
		</div>
	</div>
	<?php endif; ?>
</form>

<?php $this->endWidget(); ?>

<script language="javascript" type="text/javascript">
var photo_size = 1024 * 1024;
var number =new RegExp('^\[0-9]{9,15}$');
jQuery('button[type=submit]').click(function(){
	var error = '';
	jQuery('.has-error').removeClass('has-error');
	jQuery('.form-error-message').hide();
	if((file = jQuery('input[name=file-0]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Pasfoto Peserta #1 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Pasfoto Peserta #1 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-1]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Depan Peserta #1 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Depan Peserta #1 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-2]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Belakang Peserta #1 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Belakang Peserta #1 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-3]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Pasfoto Peserta #2 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Pasfoto Peserta #2 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-4]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Depan Peserta #2 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Depan Peserta #2 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-5]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Belakang Peserta #2 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Belakang Peserta #2 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-6]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Pasfoto Peserta #3 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Pasfoto Peserta #3 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-7]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Depan Peserta #3 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Depan Peserta #3 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-8]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Belakang Peserta #3 terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Belakang Peserta #3 yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if(jQuery('#University_contestant_team_name').val() == '' ){
		error = 'Nama Tim perlu diisi';
		jQuery('#University_contestant_team_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#University_contestant_email').val() == '' ){
		error = 'Email perlu diisi';
		jQuery('#University_contestant_email').parent().parent().addClass('has-error');
	}
	else if(jQuery('#University_contestant_first_name').val() == '' ){
		error = 'Nama Peserta #1 perlu diisi';
		jQuery('#University_contestant_first_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#University_contestant_first_phone').val() == '' ){
		error = 'Telepon / HP Peserta #1 perlu diisi';
		jQuery('#University_contestant_first_phone').parent().parent().addClass('has-error');
	}
	else if(!number.test(jQuery('#University_contestant_first_phone').val())){
		error = 'Format Telepon / HP Peserta #1 tidak benar';
		jQuery('#University_contestant_first_phone').parent().parent().addClass('has-error');
	}
	else if(jQuery('#University_contestant_university_name').val() == '' ){
		error = 'Nama Universitas perlu diisi';
		jQuery('#University_contestant_university_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#University_contestant_university_phone').val() == '' ){
		error = 'Telepon / Faksimile Universitas perlu diisi';
		jQuery('#University_contestant_university_phone').parent().parent().addClass('has-error');
	}
	else if(!number.test(jQuery('#University_contestant_university_phone').val())){
		error = 'Format Telepon / Faksimile Universitas tidak benar';
		jQuery('#University_contestant_university_phone').parent().parent().addClass('has-error');
	}
	if(error != ''){
		jQuery('.form-error-message').html(error);
		jQuery('.form-error-message').css('display','inline-block');
		window.scrollTo(0,0);
		return false;
	}
});
</script>