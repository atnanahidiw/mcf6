<?php
/* @var $this HomeController */
/* @var $model Topic + amount */
?>
<div style="margin-left: 75px;">
	<form class="form-horizontal" action="<?php echo Yii::app()->createUrl('topic/choose'); ?>" method="post" role="form">
		<input type="hidden" name="returnUrl" value="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/topik'); ?>">
		<input type="hidden" name="id" value="<?php echo Yii::app()->user->id; ?>">
		<div class="form-group">
			<label class="control-label col-md-2">Pilihan Topik</label>
			<span class="col-md-6">
				<select class="form-control" <?php echo ($choice==null)?'':'disabled '; ?>name="choice">
					<?php
					foreach($model as $mdl){
						$choosen   = (($choice!=null)&&($mdl['topic']->topic_id==$choice->topic_id))?' selected':''; 
						$remainder = $mdl['topic']->topic_quota - $mdl['amount'];
						if($remainder>0) echo '<option value="'.$mdl['topic']->topic_id.'"'.$choosen.'>'.$mdl['topic']->topic_title.' ('.$remainder.')</option>';
					}
					?>
				<select>
			</span>
			<?php if($choice==null) {?><input id="choice-button" class="btn btn-default col-md-1" type="submit" value="Pilih"> <?php } ?>
		</div>
	</form>
</div>

<script type="application/javascript">
jQuery(function() {
    jQuery('#choice-button').click(function() {
        return confirm('Apakah Anda yakin? Pilihan tidak dapat diganti');
    });
});
</script>