<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="desc-content">
	<div style="width: 100%; display: inline-block; margin-bottom: 40px;">
		<div class="reg-btn-wrapper">
			<a href="<?php echo Yii::app()->createUrl('/pemodelan_mahasiswa/pendaftaran'); ?>" class="btn btn-warning btn-lg btn-block<?php echo Configuration::isContestStarted($model_type)?'':' disabled'; ?>">DAFTAR</a>
		</div>
	</div>

	<h3 class="desc-header" style="margin-top: 0px !important;">Deskripsi</h3>
	<p>Kompetisi Pemodelan Matematika tingkat Mahasiswa MCF – MMC ITB 2014 merupakan kompetisi pemodelan matematika untuk Mahasiswa S1 dari seluruh perguruan tinggi di Indonesia. Peserta merupakan tim yang beranggotakan tiga mahasiswa sarjana dari satu perguruan tinggi yang sama. Kompetisi ini dilakukan secara online melalui web ini.</p>
	<p>Di awal kompetisi, setiap tim dapat memilih 1 (satu) dari 3 (tiga) kategori kasus yang diberikan oleh panitia. Kasus yang diberikan adalah permasalahan yang benar-benar terjadi di dunia nyata. Selanjutnya, dalam waktu 5 hari (5 x 24 jam), setiap peserta akan diminta untuk membuat model matematika dan penyelesaian atas permasalahan dalam kasus yang telah dipilih. Di akhir kompetisi setiap peserta akan diminta untuk mengumpulkan hasil penelitian singkat mereka untuk dinilai oleh para juri. Melalui pemilihan kasus nyata yang dekat dengan kehidupan sehari – hari, kami berharap kompetisi ini dapat memberikan wawasan dan pengalaman berharga kepada para mahasiswa mengenai pemodelan matematika, selain tentunya dapat mengembangkan kecakapan bermatematika bagi para mahasiswa sendiri.</p>
	<p>Detail peraturan kompetisi dapat diunduh melalui link berikut <a href="<?php echo Yii::app()->baseUrl.'/berkas/KPMM 2014.pdf'; ?>"><span class="label label-info">KPMM 2014</span></a></p>

	<h3 class="desc-header">Jadwal</h3>
	<div class="desc-list">
		<span class="col-md-2">Pendaftaran</span>
		<span>: <?php echo Yii::app()->params['university']['registration']['begin'].' - '.Yii::app()->params['university']['registration']['end']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Pelaksanaan Pemodelan</span>
		<span>: <?php echo Yii::app()->params['university']['semifinal']['begin'].' - '.Yii::app()->params['university']['semifinal']['end']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Pengumuman Finalis</span>
		<span>: <?php echo Yii::app()->params['university']['announcement']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Final</span>
		<span>: <?php echo Yii::app()->params['university']['final']; ?></span>
	</div>

	<h3 class="desc-header">Hadiah</h3>
	<div class="desc-list">
		<span class="col-md-2">Juara Per Kategori</span>
		<span>: Rp &nbsp&nbsp7.500.000,00</span>
	</div>
</div>