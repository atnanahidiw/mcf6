<?php
/* @var $this UniversityController */
/* @var $model University */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'contestant_id'); ?>
		<?php echo $form->textField($model,'contestant_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_team_name'); ?>
		<?php echo $form->textField($model,'contestant_team_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_phone'); ?>
		<?php echo $form->textField($model,'contestant_phone',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_email'); ?>
		<?php echo $form->textField($model,'contestant_email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_first_name'); ?>
		<?php echo $form->textField($model,'contestant_first_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_second_name'); ?>
		<?php echo $form->textField($model,'contestant_second_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_third_name'); ?>
		<?php echo $form->textField($model,'contestant_third_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_university_name'); ?>
		<?php echo $form->textField($model,'contestant_university_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->