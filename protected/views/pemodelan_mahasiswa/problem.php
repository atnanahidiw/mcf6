<?php
/* @var $this UniversityController */
/* @var $model Topic*/
?>

<div style="margin-left: 75px;">
	<div>Berikut adalah pilihan topik masalah yang akan dilombakan</div>
	<?php
	$i = 0;
	foreach($model as $mdl){
	?>
		<div class="desc-list">
			<span class="col-md-1">Topik <?php echo ++$i; ?></span>
			<span>
				<span class="col-md-6">: <?php echo $mdl->topic_title; ?></span>
				<span><a class="label label-info" target="_blank" href="https://docs.google.com/file/d/<?php echo $mdl->topic_source; ?>/preview">Lihat</a></span>
				<span><a class="label label-success" target="_blank" href="https://docs.google.com/uc?export=download&id=<?php echo $mdl->topic_source; ?>">Unduh</a></span>
				<?php if($mdl->topic_id == 3) : ?>
					<span><a class="label label-warning" target="_blank" href="https://docs.google.com/uc?export=download&id=0By_WcqkJZeCgSThpQVI5a0VLY1k">Unduh Data Set</a></span>
				<?php endif; ?>
			</span>
		</div>
	<?php } ?>
	<div style="margin-top:50px;">Silahkan memilihnya di menu <a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/topik'); ?>">Topik</a></div>
</div>