<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<script type="text/javascript">
$('button[name=yt0]').hide();
$('input[type=text], textarea').keyup(function(){
	$('button[name=yt0]').show();
});
$('input.error').blur(function(){
	$(this).css('border','0');
	$(this).removeClass('error');
	$(this).parent().children().last().hide();
	$(this).parent().parent().children().first().removeClass('error');
});
</script>