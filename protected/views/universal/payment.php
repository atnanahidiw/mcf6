<form class="form-horizontal" action="payment" method="post" enctype="multipart/form-data">
	<div class="control-group">
		<label class="control-label">Bukti Pembayaran</label>
		<div class="fileupload fileupload-new controls" data-provides="fileupload">
			<div class="input-append">
				<div class="uneditable-input span3">
					<i class="icon-file fileupload-exists"></i>
					<span class="fileupload-preview"></span>
				</div>
				<span class="btn btn-file">
					<span class="fileupload-new">Pilih Berkas</span>
					<span class="fileupload-exists">Ubah</span>
					<input type="file" name="file" id="file">
				</span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Batal</a>
			</div>
			<?php
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'success',
				'label'=>'Unggah',
			));
			?>
		</div>
	</div>
</form>

<?php
if (Yii::app()->user->hasFlash('success')||Yii::app()->user->hasFlash('error')){
	$this->widget('bootstrap.widgets.TbAlert', array(
		'block'=>true,
		'fade'=>true,
		'closeText'=>false,
		'alerts'=>array(
			'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>false),
			'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>false),
		),
	));
}
$source = (is_null($file)) ? '' : Yii::app()->request->baseUrl.'/payment/'.$file->file_name;
?>
<img style="-webkit-user-select: none; cursor: -webkit-zoom-in;" src="<?php echo $source; ?>" width="660px">