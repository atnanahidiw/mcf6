<?php if (Yii::app()->user->hasFlash('success')) : ?>
	<div class="alert alert-success reg-success"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php elseif (Yii::app()->user->hasFlash('error')) : ?>
	<div class="alert alert-warning reg-success"><?php echo Yii::app()->user->getFlash('error'); ?></div>
<?php else : 
	echo $this->renderPartial('_form', array('model'=>$model));
	endif;
?>