<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/google.maps.js"></script>
<sdiv class="row">
	<div>
		<div class="contact-info">
			<div class="page-block-title"><h2>Sekretariat</h2></div>
			<div id="map-canvas"/></div>
			<div class="contact-desc">
				<div>Labtek III Matematika</div>
				<div>Program Studi Matematika</div>
				<div>Institut Teknologi Bandung</div>
				<div>Jalan Ganesha 10, Bandung 40132</div>
				<div class="contact-social contact-mail"><a href="mailto:mcf.mmc.itb@gmail.com">mcf.mmc.itb@gmail.com</a></div>
				<div class="contact-social contact-fb"><a href="http://www.facebook.com/mcf.mmc" target="_blank">www.facebook.com/mcf.mmc</a></div>
				<div class="contact-social contact-twitter"><a href="http://www.twitter.com/mcf_mmc_itb" target="_blank">@mcf_mmc_itb</a></div>
			</div>
		</div>
	</div>

	<div>
		<div class="contact-info">
			<div class="page-block-title"><h2><span style="line-height: 1.3;">Narahubung</span></h2></div>
			<div class="contact-desc contact-cp">
				<div class="contact-desc-right">
					<div><strong>Dita Ully Manurung</strong></div>
					<div class="contact-social contact-phone">+62 8996911778</div>
					<div class="contact-social contact-mail"><a href="mailto:dita.manurung@gmail.com">dita.manurung@gmail.com</a></div>
				</div>
				<div class="contact-desc-left">
					<div><strong>Yozef Giovanni Tjandra</strong></div>
					<div class="contact-social contact-phone">+62 81804597377</div>
					<div class="contact-social contact-mail"><a href="mailto:yozef.g.tjandra@live.com">yozef.g.tjandra@live.com</a></div>
				</div>
			</div>
		</div>
	</div>
</div>