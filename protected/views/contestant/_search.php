<?php
/* @var $this ContestantController */
/* @var $model Contestant */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'contestant_id'); ?>
		<?php echo $form->textField($model,'contestant_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_username'); ?>
		<?php echo $form->textField($model,'contestant_username',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_type'); ?>
		<?php echo $form->textField($model,'contestant_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contestant_paid'); ?>
		<?php echo $form->textField($model,'contestant_paid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->