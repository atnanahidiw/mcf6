<?php
/* @var $this ContestantController */
/* @var $model Contestant */

$this->breadcrumbs=array(
	'Contestants'=>array('index'),
	$model->contestant_id=>array('view','id'=>$model->contestant_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Contestant', 'url'=>array('index')),
	array('label'=>'Create Contestant', 'url'=>array('create')),
	array('label'=>'View Contestant', 'url'=>array('view', 'id'=>$model->contestant_id)),
	array('label'=>'Manage Contestant', 'url'=>array('admin')),
);
?>

<h1>Update Contestant <?php echo $model->contestant_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>