<?php
/* @var $this ContestantController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contestants',
);

$this->menu=array(
	array('label'=>'Create Contestant', 'url'=>array('create')),
	array('label'=>'Manage Contestant', 'url'=>array('admin')),
);
?>

<h1>Contestants</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
