<?php
/* @var $this ContestantController */
/* @var $model Contestant */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contestant-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_username'); ?>
		<?php echo $form->textField($model,'contestant_username',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'contestant_username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_password'); ?>
		<?php echo $form->textField($model,'contestant_password',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'contestant_password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_type'); ?>
		<?php echo $form->textField($model,'contestant_type'); ?>
		<?php echo $form->error($model,'contestant_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_paid'); ?>
		<?php echo $form->textField($model,'contestant_paid'); ?>
		<?php echo $form->error($model,'contestant_paid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->