<?php
/* @var $this ContestantController */
/* @var $data Contestant */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->contestant_id), array('view', 'id'=>$data->contestant_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_username')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_password')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_type')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_paid')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_paid); ?>
	<br />


</div>