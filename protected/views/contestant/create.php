<?php
/* @var $this ContestantController */
/* @var $model Contestant */

$this->breadcrumbs=array(
	'Contestants'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Contestant', 'url'=>array('index')),
	array('label'=>'Manage Contestant', 'url'=>array('admin')),
);
?>

<h1>Create Contestant</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>