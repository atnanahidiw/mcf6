<?php
/* @var $this ContestantController */
/* @var $model Contestant */

$this->breadcrumbs=array(
	'Contestants'=>array('index'),
	$model->contestant_id,
);

$this->menu=array(
	array('label'=>'List Contestant', 'url'=>array('index')),
	array('label'=>'Create Contestant', 'url'=>array('create')),
	array('label'=>'Update Contestant', 'url'=>array('update', 'id'=>$model->contestant_id)),
	array('label'=>'Delete Contestant', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->contestant_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Contestant', 'url'=>array('admin')),
);
?>

<h1>View Contestant #<?php echo $model->contestant_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'contestant_id',
		'contestant_username',
		'contestant_password',
		'contestant_type',
		'contestant_paid',
	),
)); ?>
