<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="desc-content">
	<h3 class="desc-header" style="margin-top: 0px !important;">Deskripsi</h3>
	<p>Pameran Karya MCF – MMC ITB 2014, merupakan pameran hasil karya mahasiswa Program Studi S1 Matematika ITB yang dilaksanakan pada tanggal 15-16 Februari 2014, bertepatan dengan acara puncak MCF – MMC ITB 2014. Pada kesempatan ini aka ditampilkan karya-karya berupa hasil Tugas Akhir mahasiswa, hasil pemodelan matematika, tugas besar mata kuliah, hasil penelitian mahasiswa dan banyak lagi yang lainnya. Hal ini bersinergis dengan tujuan MCF yang ingin menunjukkan bahwa Matematika tidak hanya berperan dalam sebatas dunia pendidikan, tetapi juga berperan dalam signifikansi pembangunan bangsa yang serius.</p>

	<h3 class="desc-header">Waktu Pelaksanaan</h3>
	<p>14 Februari 2014 - 15 Februari 2014</p>
</div>