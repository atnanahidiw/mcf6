<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="desc-content">
	<div style="width: 100%; display: inline-block; margin-bottom: 40px;">
		<div class="reg-btn-wrapper">
			<a href="<?php echo Yii::app()->createUrl('/esai/pendaftaran'); ?>" class="btn btn-warning btn-lg btn-block<?php echo Configuration::isContestStarted($model_type)?'':' disabled'; ?>">DAFTAR</a>
		</div>
	</div>

	<h3 class="desc-header" style="margin-top: 0px !important;">Deskripsi</h3>
	<p>Matematika merupakan salah satu ilmu dasar yang memegang peranan penting dalam pengembangan berbagai disiplin ilmu lain. Matematika hadir secara alami dalam setiap aspek kehidupan manusia. Begitu luasnya peran matematika dalam kehidupan, namun hanya sebagian kecil masyarakat Indonesia yang mengetahui dan menyadari hal tersebut. Sebagai negara dengan penduduk terbanyak keempat di dunia, terlalu sedikit SDM Indonesia yang dibekali kemampuan berpikir matematis yang baik.</p>
	<p>Melalui Kontes Menulis Esai Matematika ITB 2014, MCF – MMC ITB 2014 akan menantang siswa-siswi SMA Indonesia untuk menetaskan buah pikiran terbaik mereka melalui kompetisi esai. Tuliskan esaimu. Jangan berangkat dari teori yang muluk-muluk. Mulailah dengan mengamati, observasi, kondisi di sekitarmu. Gambarkan permasalahan paling menarik atau paling penting di sekelilingmu, di wilayahmu, di “area kekuasaanmu”.</p>
	<p>Ini bukan kompetisi membuat makalah dengan basis teori yang rigid, tapi tentang pendapat subjektif. Tulisan bisa berupa refleksi, observasi mendalam, atau gagasan konkret atas sebuah persoalan nyata di sekitarmu.</p>
	<p>Pengumpulan naskah esai dilakukan secara online melalui web ini. Tiga peserta dengan naskah esai terbaik akan diundang ke kampus ITB untuk mengikuti serangkaian acara puncak MCF – MMC ITB 2014 dan menerima penghargaan dari panitia MCF – MMC ITB 2014.</p>
	<p>Detail peraturan kompetisi dapat diunduh melalui link berikut <a href="<?php echo Yii::app()->baseUrl.'/berkas/KMES 2014.pdf'; ?>"><span class="label label-info">KMES 2014</span></a>.</p>

	<h3 class="desc-header">Subtema</h3>
	<div class="desc-list">
		<ol>
			<li>Peranan Matematika dalam Teknologi.</li>
			<li>Peranan Matematika dalam Ekonomi.</li>
			<li>Dunia Pendidikan Matematika.</li>
		</ol>
	</div>

	<h3 class="desc-header">Jadwal</h3>
	<div class="desc-list">
		<span class="col-md-2">Pendaftaran</span>
		<span>: <?php echo Yii::app()->params['paper']['registration']['begin'].' - '.Yii::app()->params['paper']['registration']['end']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Penerimaan Berkas</span>
		<span>: <?php echo Yii::app()->params['paper']['semifinal']['begin'].' - '.Yii::app()->params['paper']['semifinal']['end']; ?></span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Pengumuman Finalis</span>
		<span>: <?php echo Yii::app()->params['paper']['announcement']; ?></span>
	</div>

	<h3 class="desc-header">Hadiah</h3>
	<div class="desc-list">
		<span class="col-md-2">Juara 1</span>
		<span>: Rp 3.000.000,00</span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Juara 2</span>
		<span>: Rp 2.000.000,00</span>
	</div>
	<div class="desc-list">
		<span class="col-md-2">Juara 3</span>
		<span>: Rp 1.000.000,00</span>
	</div>
</div>