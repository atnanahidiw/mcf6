<?php
/* @var $this PaperController */
/* @var $model Paper */

$new   = $model->isNewRecord;
$admin = false;
$form=$this->beginWidget('CActiveForm', array(
        'id' => 'register-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array(
        	'class'=>'form-horizontal',
	        'role'=>'form',
	        'style'=>'margin: 0 0 50px 75px',
	        'enctype'=>'multipart/form-data',
        )
    )); 
?>

<div class="form-error-message alert alert-danger"></div>
<?php echo $form->errorSummary($model); ?>


<!-- <form enctype="multipart/form-data" class="form-horizontal" action="<?php echo $this->action->id; ?>" method="post" role="form" style="margin: 0 0 50px 75px;"> -->
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Pribadi</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_photo">Pasfoto Berwarna</label>
		<div class="col-md-4">
			<?php $file = ((!$new) && ($model->contestant_photo != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 130px; height: 190px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_photo : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-0">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_photo'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_id">Kartu Identitas</label>
		<div class="col-md-4">
			<div>Tampak Depan</div>
			<?php $file = ((!$new) && ($model->contestant_id_f != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_id_f : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-1">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_id_f'); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div>Tampak Belakang</div>
			<?php $file = ((!$new) && ($model->contestant_id_b != '')) ? 'fileinput-exists' : 'fileinput-new'; ?>
			<div class="fileinput <?php echo $file; ?>" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" style="width: 310px; height: 200px;">
					<img src="<?php echo ($file=='fileinput-exists') ? Yii::app()->baseUrl.'/upload/'.$model->contestant_id_b : ''; ?>">
				</div>
				<?php if(!$admin): ?>
				<div>
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">Pilih Foto</span>
						<span class="fileinput-exists">Ubah</span>
						<input type="file" name="file-2">
					</span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
				</div>
				<?php endif; ?>
				<?php echo $form->error($model, 'contestant_id_b'); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_name">Nama Lengkap*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Paper[contestant_name]" id="Paper_contestant_name" class="form-control" type="text" value="<?php echo $model->contestant_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Paper[contestant_address]" id="Paper_contestant_address" class="form-control"><?php echo $model->contestant_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_email">Email*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Paper[contestant_email]" id="Paper_contestant_email" class="form-control" type="email" value="<?php echo $model->contestant_email; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_email'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_phone">Telepon / HP*</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="Paper[contestant_phone]" id="Paper_contestant_phone" class="form-control" type="text" value="<?php echo $model->contestant_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_phone'); ?>
	</div>
	<div class="form-group">
		<h3 class="control-label col-md-2">Data Sekolah</h3>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_school_name">Nama Sekolah*</label>
		<div class="col-md-4">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Paper[contestant_school_name]" id="Paper_contestant_school_name" class="form-control" type="text" value="<?php echo $model->contestant_school_name; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_school_name'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_school_address">Alamat</label>
		<div class="col-md-4">
			<textarea <?php echo $admin ? 'disabled' : ''; ?> size="50" maxlength="50" name="Paper[contestant_school_address]" id="Paper_contestant_school_address" class="form-control"><?php echo $model->contestant_school_address; ?></textarea>
		</div>
		<?php echo $form->error($model, 'contestant_school_address'); ?>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="Paper_contestant_school_phone">Telepon / Faksimile*</label>
		<div class="col-md-2">
			<input <?php echo $admin ? 'disabled' : ''; ?> size="15" maxlength="15" name="Paper[contestant_school_phone]" id="Paper_contestant_school_phone" class="form-control" type="text" value="<?php echo $model->contestant_school_phone; ?>">
		</div>
		<?php echo $form->error($model, 'contestant_school_phone'); ?>
	</div>
	<?php if(!$admin): ?>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			<button type="submit" class="btn btn-default"><?php echo $new ? 'Daftar' : 'Simpan'; ?></button>
		</div>
	</div>
	<?php endif; ?>
</form>

<?php $this->endWidget(); ?>

<script language="javascript" type="text/javascript">
var photo_size = 1024 * 1024;
var number =new RegExp('^\[0-9]{9,15}$');
jQuery('button[type=submit]').click(function(){
	var error = '';
	jQuery('.has-error').removeClass('has-error');
	jQuery('.form-error-message').hide();
	if((file = jQuery('input[name=file-0]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Pasfoto terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Pasfoto yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-1]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Depan terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Depan yang diunggah harus berformat png/jpg/jpeg';
		}
	}
	if((file = jQuery('input[name=file-2]')[0].files[0]) != null){
		if(file.size > photo_size){
			error = 'Ukuran Kartu Identitas Belakang terlalu besar (maksimal 1MB)';
		}
		else if((file.type != 'image/png') && (file.type != 'image/jpg') && (file.type != 'image/jpeg')){
			error = 'Kartu Identitas Belakang yang diunggah harus berformat png/jpg/jpeg';
		}
	}
    if(jQuery('#Paper_contestant_name').val() == '' ){
		error = 'Nama Lengkap perlu diisi';
		jQuery('#Paper_contestant_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Paper_contestant_phone').val() == '' ){
		error = 'Telepon / HP perlu diisi';
		jQuery('#Paper_contestant_phone').parent().parent().addClass('has-error');
	}
	else if(!number.test(jQuery('#Paper_contestant_phone').val())){
		error = 'Format Telepon / HP tidak benar';
		jQuery('#Paper_contestant_phone').parent().parent().addClass('has-error');
	}
    else if(jQuery('#Paper_contestant_email').val() == '' ){
		error = 'Email perlu diisi';
		jQuery('#Paper_contestant_email').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Paper_contestant_school_name').val() == '' ){
		error = 'Nama Sekolah perlu diisi';
		jQuery('#Paper_contestant_school_name').parent().parent().addClass('has-error');
	}
	else if(jQuery('#Paper_contestant_school_phone').val() == '' ){
		error = 'Telepon / Faksimile Sekolah perlu diisi';
		jQuery('#Paper_contestant_school_phone').parent().parent().addClass('has-error');
	}
	else if(!number.test(jQuery('#Paper_contestant_school_phone').val())){
		error = 'Format Telepon / Faksimile Sekolah tidak benar';
		jQuery('#Paper_contestant_school_phone').parent().parent().addClass('has-error');
	}
	if(error != ''){
		jQuery('.form-error-message').html(error);
		jQuery('.form-error-message').css('display','inline-block');
		window.scrollTo(0,0);
		return false;
	}
});
</script>