<?php
/* @var $this SchoolController */
/* @var $data School */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->contestant_id), array('view', 'id'=>$data->contestant_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_team_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_team_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_phone')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_email')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_second_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_second_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_third_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_third_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('contestant_school_name')); ?>:</b>
	<?php echo CHtml::encode($data->contestant_school_name); ?>
	<br />

	*/ ?>

</div>