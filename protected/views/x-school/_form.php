<?php
/* @var $this SchoolController */
/* @var $model School */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'school-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_team_name'); ?>
		<?php echo $form->textField($model,'contestant_team_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contestant_team_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_phone'); ?>
		<?php echo $form->textField($model,'contestant_phone',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'contestant_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_email'); ?>
		<?php echo $form->textField($model,'contestant_email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contestant_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_first_name'); ?>
		<?php echo $form->textField($model,'contestant_first_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contestant_first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_second_name'); ?>
		<?php echo $form->textField($model,'contestant_second_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contestant_second_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_third_name'); ?>
		<?php echo $form->textField($model,'contestant_third_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contestant_third_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contestant_school_name'); ?>
		<?php echo $form->textField($model,'contestant_school_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contestant_school_name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->